﻿using AssemblyCSharp;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.IO;
using System.Xml.Serialization;

public class ShopScreen : MonoBehaviour {
	private FileStream fileStream;

	private void Start() {
		loadPlayerCoins();
	}

	private void loadPlayerCoins() {
		const string NAME = "Coins/Value";
		var child = transform.Find(NAME);
		var t = child.GetComponent<Text>();
		var balance = getBalance();
		t.text = balance;
	}

	private string getBalance() {
		try {
			return getBalanceFromPlayerProgress();
		} catch (Exception e) {
			return getBalanceInCaseOf(e);
		}
	}

	private string getBalanceFromPlayerProgress() {
		var playerProgress = getPlayerProgress();
		var balance = playerProgress.balanceInCoins;
		return balance.ToString();
	}

	private PlayerProgress getPlayerProgress() {
		var serializer = getSerializerForPlayerProgress();
		var stream = getStream();
		var playerProgress = serializer.Deserialize(stream) as PlayerProgress;
		stream.Close();
		return playerProgress;
	}

	private XmlSerializer getSerializerForPlayerProgress() {
		var type = typeof(PlayerProgress);
		return new XmlSerializer(type);
	}

	private FileStream getStream() {
		resetFileStream();
		return getFileStream();
	}

	private void resetFileStream() {
		const string KEY = "Path To Player Progress";
		var path = PlayerPrefs.GetString(KEY);
		var mode = FileMode.OpenOrCreate;
		var value = new FileStream(path, mode);
		setFileStream(value);
	}

	private void setFileStream(FileStream value) {
		fileStream = value;
	}

	private FileStream getFileStream() {
		return fileStream;
	}

	private string getBalanceInCaseOf(Exception e) {
		handle(e);
		return getBalanceFromNewlyCreatedPlayerProgress();
	}

	private void handle(Exception e) {
		e.GetType();
	}

	private string getBalanceFromNewlyCreatedPlayerProgress() {
		const int BALANCE = 2000;
		var serializer = getSerializerForPlayerProgress();
		var stream = getFileStream();
		var o = new PlayerProgress();
		o.balanceInCoins = BALANCE;
		serializer.Serialize(stream, o);
		stream.Close();
		return BALANCE.ToString();
	}

	public void updateBalanceByAdding(int coinsQuantity) {
		updateBalanceInPlayerProgressByAdding(coinsQuantity);
		loadPlayerCoins();
	}

	private void updateBalanceInPlayerProgressByAdding(int coinsQuantity) {
		var serializer = getSerializerForPlayerProgress();
		var o = getPlayerProgress();
		var stream = getStream();
		o.balanceInCoins += coinsQuantity;
		serializer.Serialize(stream, o);
		stream.Close();
	}

	public void goToHomeScreen() {
		const string SCENE_NAME = "Home Screen";
		SceneManager.LoadScene(SCENE_NAME);
	}
}