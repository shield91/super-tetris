﻿using UnityEngine;
using AssemblyCSharp;
using System;
using System.IO;
using System.Xml.Serialization;

public class StartScreen : MonoBehaviour {
	private FileStream fileStream;

	private void Start() {
		deleteDataAboutChosenStartMode();
		resetFileStream();
		loadStartModes();
	}

	private void deleteDataAboutChosenStartMode() {
		const string KEY = "Chosen Start Mode";
		PlayerPrefs.DeleteKey(KEY);
	}

	private void resetFileStream() {
		var path = getPathToPlayerProgress();
		var mode = FileMode.OpenOrCreate;
		var value = new FileStream(path, mode);
		setFileStream(value);
	}

	private string getPathToPlayerProgress() {
		const string KEY = "Path To Player Progress";
		return PlayerPrefs.GetString(KEY);
	}

	private void setFileStream(FileStream value) {
		fileStream = value;
	}

	private void loadStartModes() {
		try {
			loadAllAvailableModes();
		} catch (Exception e) {
			loadOnlyEasyModeBecauseOf(e);
		}
	}

	private void loadAllAvailableModes() {
		if (allModesAreClosed()) {
			loadOnlyEasyMode();
		} else {
			loadAllOpenedModes();
		}
	}

	private bool allModesAreClosed() {
		var playerProgress = getPlayerProgress();
		var startModes = playerProgress.startModes;
		var startModesCount = startModes.Count;
		return startModesCount == 0;
	}

	private PlayerProgress getPlayerProgress() {
		try {
			return getAlreadyCreatedPlayerProgress();
		} catch (Exception e) {
			return getNewlyCreatedPlayerProgressBecauseOf(e);
		}
	}

	private PlayerProgress getAlreadyCreatedPlayerProgress() {
		var serializer = getSerializerForPlayerProgress();
		var stream = getFileStream();
		var playerProgress = serializer.Deserialize(stream) as PlayerProgress;
		stream.Close();
		return playerProgress;
	}

	private XmlSerializer getSerializerForPlayerProgress() {
		var type = typeof(PlayerProgress);
		return new XmlSerializer(type);
	}

	private FileStream getFileStream() {
		return fileStream;
	}

	private PlayerProgress getNewlyCreatedPlayerProgressBecauseOf(Exception e) {
		handle(e);
		return getNewlyCreatedPlayerProgress();
	}

	private void handle(Exception e) {
		e.GetType();
	}

	private PlayerProgress getNewlyCreatedPlayerProgress() {
		const int BALANCE = 2000;
		var serializer = getSerializerForPlayerProgress();
		var stream = getFileStream();
		var o = new PlayerProgress();
		o.balanceInCoins = BALANCE;
		serializer.Serialize(stream, o);
		stream.Close();
		return o;
	}

	private void loadOnlyEasyMode() {
		resetFileStream();
		openEasyMode();
		loadAllOpenedModes();
	}

	private void openEasyMode() {
		var serializer = getSerializerForPlayerProgress();
		var o = getPlayerProgress();
		var stream = getStream();
		var startModes = o.startModes;
		var item = new StartMode();
		item.name = "Easy";
		item.speed = 1;
		item.coinsToOpenNextMode = 100;
		startModes.Add(item);
		serializer.Serialize(stream, o);
		stream.Close();
	}

	private FileStream getStream() {
		resetFileStream();
		return getFileStream();
	}

	private void loadAllOpenedModes() {
		var suppliant = this;
		var loader = new OpenedStartModesLoader(suppliant);
		loader.loadOpenedStartModes();
	}

	private void loadOnlyEasyModeBecauseOf(Exception e) {
		handle(e);
		loadOnlyEasyMode();
	}

	public void goToPlayScreenInEasyMode() {
		setPlayScreenForEasyStartMode();
		loadPlayScreen();
	}

	private void setPlayScreenForEasyStartMode() {
		const string NAME = "Easy";
		setPlayScreenForStartMode(NAME);
	}

	private void setPlayScreenForStartMode(string name) {
		const string KEY = "Chosen Start Mode";
		string value = name;
		PlayerPrefs.SetString(KEY, value);
	}

	private void loadPlayScreen() {
		const string NAME = "Play Screen";
		Application.LoadLevel(NAME);
	}
	
	public void goToPlayScreenInMediumMode() {
		setPlayScreenForMediumStartMode();
		loadPlayScreen();
	}

	private void setPlayScreenForMediumStartMode() {
		const string NAME = "Medium";
		setPlayScreenForStartMode(NAME);
	}

	public void goToPlayScreenInHardMode() {
		setPlayScreenForHardStartMode();
		loadPlayScreen();
	}

	private void setPlayScreenForHardStartMode() {
		const string NAME = "Hard";
		setPlayScreenForStartMode(NAME);
	}

	public void goToPlayScreenInFreeMode() {
		setPlayScreenForFreeStartMode();
		loadPlayScreen();
	}

	private void setPlayScreenForFreeStartMode() {
		const string NAME = "Free";
		setPlayScreenForStartMode(NAME);
	}

	public void goToHomeScreen() {
		const string NAME = "Home Screen";
		Application.LoadLevel(NAME);
	}
}