using UnityEngine;
using System.IO;
using System.Xml.Serialization;

namespace AssemblyCSharp {
	public class OpenedStartModesLoader	{
		private StartScreen requester;
		private int currentOpenedStartModeIndex;

		public OpenedStartModesLoader(StartScreen suppliant) {
			setRequester(suppliant);
			resetCurrentOpenedStartModeIndex();
		}

		private void setRequester(StartScreen value) {
			requester = value;
		}

		private void resetCurrentOpenedStartModeIndex() {
			const int VALUE = 0;
			setCurrentOpenedStartModeIndex(VALUE);
		}

		private void setCurrentOpenedStartModeIndex(int value) {
			currentOpenedStartModeIndex = value;
		}

		public void loadOpenedStartModes() {
			resetCurrentOpenedStartModeIndex();
			loadOpenedModes();
		}

		private void loadOpenedModes() {
			while (thereAreOpenedStartModesNeedToBeLoaded()) {
				loadCurrentOpenedStartMode();
			}
		}

		private bool thereAreOpenedStartModesNeedToBeLoaded() {
			var playerProgress = getPlayerProgress();
			var startModes = playerProgress.startModes;
			var n = startModes.Count;
			var i = getCurrentOpenedStartModeIndex();
			return i < n;
		}

		private PlayerProgress getPlayerProgress() {
			var serializer = getSerializerForPlayerProgress();								
			var stream = getStreamForPlayerProgress();
			var playerProgress = serializer.Deserialize(stream) as PlayerProgress;
			stream.Close();
			return playerProgress;
		}

		private XmlSerializer getSerializerForPlayerProgress() {
			var type = typeof(PlayerProgress);
			return new XmlSerializer(type);
		}

		private FileStream getStreamForPlayerProgress() {
			var path = getPathToPlayerProgress();
			var mode = FileMode.OpenOrCreate;
			return new FileStream(path, mode);
		}
		
		private string getPathToPlayerProgress() {
			const string KEY = "Path To Player Progress";
			return PlayerPrefs.GetString(KEY);
		}

		private int getCurrentOpenedStartModeIndex() {
			return currentOpenedStartModeIndex;
		}

		private void loadCurrentOpenedStartMode() {
			loadCurrentOpenedMode();
			moveOnToTheNextOpenedStartMode();
		}

		private void loadCurrentOpenedMode() {
			const string NAME = "Scroll Panel/Buttons";
			const bool VALUE = true;
			var suppliant = getRequester();
			var transform = suppliant.transform;
			var buttons = transform.Find(NAME);
			var index = getCurrentOpenedStartModeIndex();
			var currentButton = buttons.GetChild(index);
			var go = currentButton.gameObject;
			go.SetActive(VALUE);
		}

		private StartScreen getRequester() {
			return requester;
		}

		private void moveOnToTheNextOpenedStartMode() {
			int index = getCurrentOpenedStartModeIndex();
			int value = index + 1;
			setCurrentOpenedStartModeIndex(value);
		}
	}
}