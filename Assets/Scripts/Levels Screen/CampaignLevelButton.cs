﻿using UnityEngine;
using UnityEngine.UI;
using AssemblyCSharp;

public class CampaignLevelButton : MonoBehaviour {
	public void processClickOnButton() {
		if (levelEditorModeIsEnabled()) {
			showPopUpWindowForChosenLevel();
		} else {
			processClick();
		}
	}

	private bool levelEditorModeIsEnabled() {
		const string KEY = "Level Editor Mode Is Enabled";
		const string VALUE = "";
		string value = PlayerPrefs.GetString(KEY);
		return !value.Equals(VALUE);
	}

	private void showPopUpWindowForChosenLevel() {
		const string NAME = "Pop-Up Window";
		var suppliant = this;
		var ls = GetComponentInParent<LevelsScreen>();
		var canvas = ls.transform;
		var window = canvas.Find(NAME);
		var lspuw = window.GetComponent<LevelsScreenPopUpWindow>();
		lspuw.showPopUpWindowFor(suppliant);
	}

	protected virtual void processClick() {
		goToCampaignChosenLevel();
	}

	private void goToCampaignChosenLevel() {
		packDataForChosenLevel();
		loadChosenLevel();
	}

	public void packDataForChosenLevel() {
		const string KEY = "Chosen Level";
		var engine = new CampaignLevelsSearchEngine();
		var name = getLevelNumber();
		var value = engine.get(name);
		PlayerPrefs.SetString(KEY, value);
	}

	private string getLevelNumber() {
		const string NAME = "Number";
		var number = transform.Find(NAME);
		var t = number.GetComponent<Text>();
		return t.text;
	}
	
	public void loadChosenLevel() {
		const string NAME = "Play Screen";
		Application.LoadLevel(NAME);
	}
}