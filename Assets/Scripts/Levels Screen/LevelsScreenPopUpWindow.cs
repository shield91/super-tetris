﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Xml;

public class LevelsScreenPopUpWindow : MonoBehaviour {
	private CampaignLevelButton requester;

	private void Awake() {
		resetRequester();
	}

	private void resetRequester() {
		CampaignLevelButton value = null;
		setRequester(value);
	}

	private void setRequester(CampaignLevelButton value) {
		requester = value;
	}

	public void showPopUpWindowFor(CampaignLevelButton suppliant) {
		showPopUpWindow();
		setRequester(suppliant);
	}

	private void showPopUpWindow() {
		const bool VALUE = true;
		gameObject.SetActive(VALUE);
	}

	public void processGoingToCampaignChosenLevel() {
		try {
			goToCampaignChosenLevelIfItIsPossible();
		} catch (Exception e) {
			doNothingBecauseOf(e);
		}
	}

	private void goToCampaignChosenLevelIfItIsPossible() {
		if (inputDataIsValid()) {
			goToCampaignChosenLevel();
		}
	}

	private bool inputDataIsValid() {
		if (inputFieldHasSomeData()) {
			return positiveIntegerHasBeenEntered();
		} else {
			return inputFieldIsEmpty();
		}
	}

	private bool inputFieldHasSomeData() {
		return !inputFieldIsEmpty();
	}

	private bool inputFieldIsEmpty() {
		var s = getTextFromInputField();
		var n = s.Length;
		return n == 0;
	}
	
	private string getTextFromInputField() {
		var inputField = GetComponentInChildren<InputField>();
		return inputField.text;
	}

	private bool positiveIntegerHasBeenEntered() {
		var s = getTextFromInputField();
		var value = int.Parse(s);
		return value > 0;
	}

	private void goToCampaignChosenLevel() {
		packDataForChosenLevel();
		saveDataAboutCost();
		prepareChosenLevelForLoading();
		loadChosenLevel();
	}

	private void packDataForChosenLevel() {
		var suppliant = getRequester();
		suppliant.packDataForChosenLevel();
	}

	private CampaignLevelButton getRequester() {
		return requester;
	}

	private void saveDataAboutCost() {
		const string KEY = "Modified Chosen Level";
		string value = getXmlWithChosenLevelModifiedCost();
		PlayerPrefs.SetString(KEY, value);
	}

	private string getXmlWithChosenLevelModifiedCost() {
		const string NAME = "Cost";
		var document = getDocumentForChosenLevel();
		var node = document.CreateAttribute(NAME);
		var item = document.DocumentElement;
		var attributes = item.Attributes;
		node.Value = getTextFromInputField();
		attributes.Append(node);
		return document.OuterXml;
	}

	private XmlDocument getDocumentForChosenLevel() {
		const string NAME = "Chosen Level";
		return getDocumentFor(NAME);
	}

	private XmlDocument getDocumentFor(string name) {
		var key = name;
		var xml = PlayerPrefs.GetString(key);
		var document = new XmlDocument();
		document.LoadXml(xml);
		return document;
	}

	private void prepareChosenLevelForLoading() {
		addPlayFieldsToLevelIfItIsNecessary();
		updateChosenCampaign();
		updateChosenLevel();
	}

	private void addPlayFieldsToLevelIfItIsNecessary() {
		if (chosenLevelIsNew()) {
			addPlayFieldsToLevel();
		}
	}

	private bool chosenLevelIsNew() {
		var document = getDocumentForModifiedChosenLevel();
		var documentElement = document.DocumentElement;
		return documentElement.IsEmpty;
	}

	private XmlDocument getDocumentForModifiedChosenLevel() {
		const string NAME = "Modified Chosen Level";
		return getDocumentFor(NAME);
	}

	private void addPlayFieldsToLevel() {
		addLeftPlayFieldToLevel();
		addRightPlayFieldToLevel();
	}

	private void addLeftPlayFieldToLevel() {
		const string NAME = "Left Play Field";
		updateModifiedChosenLevelByAdding(NAME);
	}

	private void updateModifiedChosenLevelByAdding(string name) {
		const string KEY = "Modified Chosen Level";
		string value = getXmlForModifiedChosenLevelWith(name);
		PlayerPrefs.SetString(KEY, value);
	}

	private string getXmlForModifiedChosenLevelWith(string name) {
		const string NAME = "Name";
		var document = getDocumentWithAddedPlayField();
		var documentElement = document.DocumentElement;
		var playField = documentElement.LastChild;
		var playFieldAttributes = playField.Attributes;
		var node = document.CreateAttribute(NAME);
		node.Value = name;
		playFieldAttributes.Append(node);
		return document.OuterXml;
	}

	private XmlDocument getDocumentWithAddedPlayField() {
		const string NAME = "PlayField";
		var document = getDocumentForModifiedChosenLevel();
		var documentElement = document.DocumentElement;
		var newChild = document.CreateElement(NAME);
		documentElement.AppendChild(newChild);
		return document;
	}

	private void addRightPlayFieldToLevel() {
		const string NAME = "Right Play Field";
		updateModifiedChosenLevelByAdding(NAME);
	}

	private void updateChosenCampaign() {
		const string KEY = "Modified Chosen Campaign";
		var value = getModifiedChosenCampaign();
		PlayerPrefs.SetString(KEY, value);
	}

	private string getModifiedChosenCampaign() {
		string campaign = getChosenCampaign();
		string oldValue = getOriginalChosenLevel();
		string newValue = getModifiedChosenLevel();
		return campaign.Replace(oldValue, newValue);
	}

	private string getChosenCampaign() {
		const string KEY = "Modified Chosen Campaign";
		string defaultValue = getOriginalChosenCampaign();
		return PlayerPrefs.GetString(KEY, defaultValue);
	}

	private string getOriginalChosenCampaign() {
		const string KEY = "Chosen Campaign";
		return PlayerPrefs.GetString(KEY);
	}

	private string getOriginalChosenLevel() {
		const string KEY = "Chosen Level";
		return PlayerPrefs.GetString(KEY);
	}

	private string getModifiedChosenLevel() {
		const string KEY = "Modified Chosen Level";
		return PlayerPrefs.GetString(KEY);
	}

	private void updateChosenLevel() {
		const string KEY = "Chosen Level";
		string value = getModifiedChosenLevel();
		PlayerPrefs.SetString(KEY, value);
	}

	private void loadChosenLevel() {
		var suppliant = getRequester();
		suppliant.loadChosenLevel();
	}

	private void doNothingBecauseOf(Exception e) {
		e.GetType();
	}
}