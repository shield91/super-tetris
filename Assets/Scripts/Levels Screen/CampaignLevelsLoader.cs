using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Xml.Serialization;

namespace AssemblyCSharp
{
	public class CampaignLevelsLoader : ButtonsLoader {
		protected override string getXpathForItems() {
			return "Campaign/Level";
		}

		protected override string getKeyForModifiedDocument() {
			return "Modified Chosen Campaign";
		}
		
		protected override string getKeyForOriginalDocument() {
			return "Chosen Campaign";
		}

		protected override string getPathForCurrentItem() {
			if (levelIsPaid()) {
				return getPathForPaidLevelButton();
			} else {
				return getPathForUnpaidLevelButton();
			}
		}

		private bool levelIsPaid() {
			bool ba = levelIsFree();
			bool bb = levelIsAvailable();
			return ba || bb;
		}

		private bool levelIsFree() {
			const string VALUE = "";
			string value = getValueForCost();
			return value.Equals(VALUE);
		}

		private string getValueForCost() {
			const string NAME = "Cost";
			return getValueFor(NAME);
		}

		private bool levelIsAvailable() {
			var chosenCampaignIndex = getChosenCampaignIndex();
			var chosenLevelIndex = getChosenLevelIndex();
			var playerProgress = getPlayerProgress();
			var campaigns = playerProgress.campaigns;

			//TODO testing
			var chosenCampaign = campaigns[chosenCampaignIndex];
			var chosenCampaignLevels = chosenCampaign.levels;
			var levelsQuantity = chosenCampaignLevels.Count;
			return chosenLevelIndex < levelsQuantity;
//			if (chosenCampaignIndex == campaigns.Count) {
//				return false;
//			} else {
//				var chosenCampaign = campaigns[chosenCampaignIndex];
//				var chosenCampaignLevels = chosenCampaign.levels;
//				var levelsQuantity = chosenCampaignLevels.Count;
//				return chosenLevelIndex < levelsQuantity;
//			}
		}

		private int getChosenCampaignIndex() {
			var engine = new PlayerProgressCampaignsSearchEngine();
			return engine.getChosenCampaignIndex();
		}
		
		private int getChosenLevelIndex() {
			var item = getCurrentItem();
			var xmlForSelectedLevel = item.OuterXml;
			var engine = new PlayerProgressChosenCampaignLevelsSearchEngine(xmlForSelectedLevel);
			return engine.getChosenLevelIndex();
		}


		public PlayerProgress getPlayerProgress() {
			var serializer = getSerializerForPlayerProgress();
			var stream = getStreamForPlayerProgress();
			var playerProgress = serializer.Deserialize(stream) as PlayerProgress;

			//stream.Close();
			stream.Dispose();

			return playerProgress;
		}
		
		public XmlSerializer getSerializerForPlayerProgress() {
			var type = typeof(PlayerProgress);
			return new XmlSerializer(type);
		}
		
		public FileStream getStreamForPlayerProgress() {
			var path = getPathToPlayerProgress();
			var mode = FileMode.OpenOrCreate;
			return new FileStream(path, mode);
		}
		
		private string getPathToPlayerProgress() {
			const string KEY = "Path To Player Progress";
			return PlayerPrefs.GetString(KEY);
		}

		private string getPathForPaidLevelButton() {
			return "Campaign Level Button";
		}

		private string getPathForUnpaidLevelButton() {
			return "Campaign Unpaid Level Button";
		}

		protected override string getNameForCurrentItem() {
			const string HEADER = "Level ";
			string value = getValueForLevelNumber();
			return HEADER + value;
		}

		private string getValueForLevelNumber() {
			const string NAME = "Number";
			return getValueFor(NAME);
		}

		protected override void setUpSpecificParametersForCurrentItem() {
			setUpNumberForCurrentButton();
			defineButtonType();
		}

		private void setUpNumberForCurrentButton() {
			var t = getTextComponentForLevelNumber();
			var value = getValueForLevelNumber();
			t.text = value;
		}

		private Text getTextComponentForLevelNumber() {
			const string NAME = "Number";
			return getTextComponentFor(NAME);
		}

		private void defineButtonType() {
			if (levelIsPaid()) {
				setUpPaidLevelButton();
			} else {
				setUpUnpaidLevelButton();
			}
		}

		private void setUpPaidLevelButton() {
			setUpHighScore();
			setUpPaidLevelButtonAvailability();
			adjustLevelNumber();
		}

		private void setUpHighScore() {
			if (levelIsAvailable()) {
				setUpHighScoreForAvailableLevel();
			}
		}

		private void setUpHighScoreForAvailableLevel() {
			var chosenCampaignIndex = getChosenCampaignIndex();
			var chosenLevelIndex = getChosenLevelIndex();
			var playerProgress = getPlayerProgress();
			var campaigns = playerProgress.campaigns;
			var chosenCampaign = campaigns[chosenCampaignIndex];
			var chosenCampaignLevels = chosenCampaign.levels;
			var chosenLevel = chosenCampaignLevels[chosenLevelIndex];
			var t = getTextComponentForHighScore();
			var value = chosenLevel.highScoreInCoins;
			var b = value > 0;
			var sa = value.ToString();
			var sb = "";
			t.text = b ? sa : sb;
		}

		private Text getTextComponentForHighScore() {
			const string NAME = "High Score";
			return getTextComponentFor(NAME);
		}	

		private void setUpPaidLevelButtonAvailability() {
			if (levelEditorModeIsEnabled()) {
				setUpPaidLevelButtonAvailabilityInLevelEditorMode();
			} else {
				setUpPaidLevelButtonAvailabilityInPlayMode();
			}
		}

		private bool levelEditorModeIsEnabled() {
			const string KEY = "Level Editor Mode Is Enabled";
			const string VALUE = "";
			string value = PlayerPrefs.GetString(KEY);
			return !value.Equals(VALUE);
		}

		private void setUpPaidLevelButtonAvailabilityInLevelEditorMode() {
			var button = getButtonComponentFromCurrentButton();
			button.interactable = true;
		}

		private Button getButtonComponentFromCurrentButton() {
			var button = getInstantiatedItem();
			return button.GetComponent<Button>();
		}

		private void setUpPaidLevelButtonAvailabilityInPlayMode() {
			var button = getButtonComponentFromCurrentButton();
			button.interactable = levelIsAvailable();
		}

		private void adjustLevelNumber() {
			if (levelIsOpened()) {
				setUpNumberForOpenedLevel();
			}
		}

		private bool levelIsOpened() {
			var b = getButtonComponentFromCurrentButton();
			return b.interactable;
		}
		
		private void setUpNumberForOpenedLevel() {
			setUpPositionForOpenedLevelNumber();
			setUpFontSizeForOpenedLevelNumber();
		}
		
		private void setUpPositionForOpenedLevelNumber() {
			const float X = -2f;
			const float Y = 30f;
			var number = getTextComponentForLevelNumber();
			var rt = number.rectTransform;
			rt.anchoredPosition = new Vector2(X, Y);
		}
		
		private void setUpFontSizeForOpenedLevelNumber() {
			const int FONT_SIZE = 40;
			var number = getTextComponentForLevelNumber();
			number.fontSize = FONT_SIZE;
		}

		private void setUpUnpaidLevelButton() {
			var t = getTextComponentForCost();
			var value = getValueForCost();
			t.text = value;
		}
		
		private Text getTextComponentForCost() {
			const string NAME = "Cost";
			return getTextComponentFor(NAME);
		}

		protected override string getKeyForDocumentWithNewItem() {
			return "Modified Chosen Campaign";
		}

		protected override string getNewItemName() {
			return "Level";
		}

		protected override string getNameForMainAttributeOfNewItem() {
			return "Number";
		}

		protected override string getValueForMainAttributeOfNewItem() {
			var document = getDocumentWithNewItem();
			var documentElement = document.DocumentElement;
			var childNodes = documentElement.ChildNodes;
			var childNodesCount = childNodes.Count;
			return childNodesCount.ToString();
		}
	}
}