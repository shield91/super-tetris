namespace AssemblyCSharp
{
	public class CampaignLevelsSearchEngine : ItemsSearchEngine	{
		protected override string getXpathForItems() {
			return "Campaign/Level";
		}

		protected override ButtonsLoader getItemsLoader() {
			return new CampaignLevelsLoader();
		}

		protected override string getNameForCurrentItemMainAttribute() {
			return "Number";
		}
	}
}