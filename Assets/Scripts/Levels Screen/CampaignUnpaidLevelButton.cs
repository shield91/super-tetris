﻿using UnityEngine;
using UnityEngine.UI;
using AssemblyCSharp;
using System.IO;
using System.Xml.Serialization;

public class CampaignUnpaidLevelButton : CampaignLevelButton {
	protected override void processClick() {
		if (playerIsAbleToPurchaseTheLevel()) {
			purchaseLevel();
		}
	}

	private bool playerIsAbleToPurchaseTheLevel() {
		int coins = getCoins();
		int cost = getCost();
		return cost <= coins;
	}

	private int getCoins() {
		var t = getTextComponentForBalance();
		var s = t.text;
		return int.Parse(s);
	}

	private Text getTextComponentForBalance() {
		const string NAME = "Coins/Value";
		var levelsScreen = GetComponentInParent<LevelsScreen>();
		var canvas = levelsScreen.transform;
		var child = canvas.Find(NAME);
		return child.GetComponent<Text>();
	}

	private int getCost() {
		const string NAME = "Cost";
		var child = transform.Find(NAME);
		var t = child.GetComponent<Text>();
		var s = t.text;
		return int.Parse(s);
	}

	private void purchaseLevel() {
		updateBalance();
		instantiateCampaignLevelButton();
		setUpNumberForCampaignLevelButton();
		setUpCampaignLevelButtonAvailability();
		adjustLevelNumberForCampaignLevelButton();
		setUpScaleForCampaignLevelButton();
		setUpIndexForCampaignLevelButton();
		updatePlayerProgress();
		destroyThisButton();
	}

	private void updateBalance() {
		var balance = getUpdatedBalance();
		var t = getTextComponentForBalance();
		t.text = balance.ToString();
	}

	private int getUpdatedBalance() {
		int coins = getCoins();
		int cost = getCost();
		return coins - cost;
	}

	private void instantiateCampaignLevelButton() {
		const string PATH = "Campaign Level Button";
		var original = Resources.Load(PATH);
		var parent = transform.parent;
		var go = GameObject.Instantiate(original, parent);
		go.name = name;
	}

	private void setUpNumberForCampaignLevelButton() {
		var t = getTextComponentForLevelNumber();
		var index = transform.GetSiblingIndex();
		var value = index + 1;
		t.text = value.ToString();
	}

	private Text getTextComponentForLevelNumber() {
		const string NAME = "Number";
		var button = getCampaignLevelButton();
		var buttonParameter = button.Find(NAME);
		return buttonParameter.GetComponent<Text>();
	}

	private Transform getCampaignLevelButton() {
		var panel = transform.parent;
		var childCount = panel.childCount;
		var index = childCount - 1;
		return panel.GetChild(index);
	}

	private void setUpCampaignLevelButtonAvailability() {
		var button = getButtonComponentFromCampaignLevelButton();
		button.interactable = true;
	}
	
	private Button getButtonComponentFromCampaignLevelButton() {
		var button = getCampaignLevelButton();
		return button.GetComponent<Button>();
	}
	
	private void adjustLevelNumberForCampaignLevelButton() {
		if (levelIsOpened()) {
			setUpNumberForOpenedLevel();
		}
	}
	
	private bool levelIsOpened() {
		var b = getButtonComponentFromCampaignLevelButton();
		return b.interactable;
	}
	
	private void setUpNumberForOpenedLevel() {
		setUpPositionForOpenedLevelNumber();
		setUpFontSizeForOpenedLevelNumber();
	}
	
	private void setUpPositionForOpenedLevelNumber() {
		const float X = -2f;
		const float Y = 30f;
		var number = getTextComponentForLevelNumber();
		var rt = number.rectTransform;
		rt.anchoredPosition = new Vector2(X, Y);
	}
	
	private void setUpFontSizeForOpenedLevelNumber() {
		const int FONT_SIZE = 40;
		var number = getTextComponentForLevelNumber();
		number.fontSize = FONT_SIZE;
	}

	private void setUpScaleForCampaignLevelButton() {
		var button = getCampaignLevelButton();
		button.localScale = transform.localScale;
	}

	private void setUpIndexForCampaignLevelButton() {
		var button = getCampaignLevelButton();
		var index = transform.GetSiblingIndex();
		button.SetSiblingIndex(index);
	}		

	private void updatePlayerProgress() {
		var index = getChosenCampaignIndex();
		var o = getPlayerProgress();
		var campaigns = o.campaigns;
		var chosenCampaign = campaigns[index];
		var chosenCampaignLevels = chosenCampaign.levels;
		var serializer = getSerializerForPlayerProgress();
		var stream = getStreamForPlayerProgress();
		var item = getNewLevel();
		o.balanceInCoins = getCoins();
		chosenCampaignLevels.Add(item);
		chosenCampaignLevels.Sort();
		serializer.Serialize(stream, o);
		stream.Close();
	}

	private int getChosenCampaignIndex() {
		var engine = new PlayerProgressCampaignsSearchEngine();
		return engine.getChosenCampaignIndex();
	}

	private PlayerProgress getPlayerProgress() {
		var serializer = getSerializerForPlayerProgress();
		var stream = getStreamForPlayerProgress();
		var playerProgress = serializer.Deserialize(stream) as PlayerProgress;
		stream.Close();
		return playerProgress;
	}

	private XmlSerializer getSerializerForPlayerProgress() {
		var type = typeof(PlayerProgress);
		return new XmlSerializer(type);
	}

	private FileStream getStreamForPlayerProgress() {
		var path = getPathToPlayerProgress();
		var mode = FileMode.OpenOrCreate;
		return new FileStream(path, mode);
	}

	private string getPathToPlayerProgress() {
		const string KEY = "Path To Player Progress";
		return PlayerPrefs.GetString(KEY);
	}

	private Level getNewLevel() {
		const string NAME = "Number";
		var buttonNumber = transform.Find(NAME);
		var t = buttonNumber.GetComponent<Text>();
		var s = t.text;
		var levelNumber = int.Parse(s);
		var level = new Level();
		level.number = levelNumber;
		return level;
	}

	private void destroyThisButton() {
		var obj = gameObject;
		Destroy(obj);
	}
}