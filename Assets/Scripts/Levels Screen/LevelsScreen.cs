﻿using UnityEngine;
using UnityEngine.UI;
using AssemblyCSharp;
using System;
using System.IO;
using System.Xml.Serialization;

public class LevelsScreen : MonoBehaviour {
	private FileStream fileStream;

	private void Start() {
		setUpLevelsScreen();
		deleteDataAboutChosenLevel();
		loadLevelsForChosenCampaign();
	}

	private void setUpLevelsScreen() {
		if (levelEditorModeIsEnabled()) {
			setUpLevelsScreenInLevelEditorMode();
		} else {
			setUpLevelsScreenInPlayMode();
		}
	}

	private bool levelEditorModeIsEnabled() {
		const string KEY = "Level Editor Mode Is Enabled";
		const string VALUE = "";
		string value = PlayerPrefs.GetString(KEY);
		return !value.Equals(VALUE);
	}

	private void setUpLevelsScreenInLevelEditorMode() {
		hideSoundButton();
		hideCoinsPanel();
		showAddNewLevelButton();
	}

	private void hideSoundButton() {
		const string NAME = "Sound";
		hide(NAME);
	}

	private void hide(string name) {
		const bool VALUE = false;
		var go = getGameObjectFor(name);
		go.SetActive(VALUE);
	}

	private GameObject getGameObjectFor(string name) {
		var child = transform.Find(name);
		return child.gameObject;
	}

	private void hideCoinsPanel() {
		const string NAME = "Coins";
		hide(NAME);
	}

	private void showAddNewLevelButton() {
		const string NAME = "Add New Level";
		const bool VALUE = true;
		var go = getGameObjectFor(NAME);
		go.SetActive(VALUE);
	}

	private void setUpLevelsScreenInPlayMode() {
		resetFileStream();
		loadPlayerCoins();
	}

	private void resetFileStream() {
		const string KEY = "Path To Player Progress";
		var path = PlayerPrefs.GetString(KEY);
		var mode = FileMode.OpenOrCreate;
		var value = new FileStream(path, mode);
		setFileStream(value);
	}
	
	private void setFileStream(FileStream value) {
		fileStream = value;
	}

	private void loadPlayerCoins() {
		const string NAME = "Coins/Value";
		var child = transform.Find(NAME);
		var t = child.GetComponent<Text>();
		var balance = getBalance();
		t.text = balance;
	}
	
	private string getBalance() {
		try {
			return getBalanceFromPlayerProgress();
		} catch (Exception e) {
			return getBalanceInCaseOf(e);
		}
	}
	
	private string getBalanceFromPlayerProgress() {
		var serializer = getSerializerForPlayerProgress();
		var stream = getFileStream();
		var playerProgress = serializer.Deserialize(stream) as PlayerProgress;
		var balance = playerProgress.balanceInCoins;
		stream.Close();
		return balance.ToString();
	}
	
	private XmlSerializer getSerializerForPlayerProgress() {
		var type = typeof(PlayerProgress);
		return new XmlSerializer(type);
	}
	
	private FileStream getFileStream() {
		return fileStream;
	}
	
	private string getBalanceInCaseOf(Exception e) {
		handle(e);
		return getBalanceFromNewlyCreatedPlayerProgress();
	}
	
	private void handle(Exception e) {
		e.GetType();
	}
	
	private string getBalanceFromNewlyCreatedPlayerProgress() {
		const int BALANCE = 2000;
		var serializer = getSerializerForPlayerProgress();
		var stream = getFileStream();
		var o = new PlayerProgress();
		o.balanceInCoins = BALANCE;
		serializer.Serialize(stream, o);
		stream.Close();
		return BALANCE.ToString();
	}

	private void deleteDataAboutChosenLevel() {
		deleteDataAboutOriginalChosenLevel();
		deleteDataAboutModifiedChosenLevel();
	}

	private void deleteDataAboutOriginalChosenLevel() {
		const string NAME = "Chosen Level";
		deleteDataAbout(NAME);
	}

	private void deleteDataAbout(string name) {
		string key = name;
		PlayerPrefs.DeleteKey(key);
	}

	private void deleteDataAboutModifiedChosenLevel() {
		const string NAME = "Modified Chosen Level";
		deleteDataAbout(NAME);
	}

	private void loadLevelsForChosenCampaign() {
		ItemsLoader l = new CampaignLevelsLoader();
		l.load();
	}

	public void goToCampaignScreen() {
		const string NAME = "Campaign Screen";
		Application.LoadLevel(NAME);
	}

	public void addNewLevel() {
		var l = new CampaignLevelsLoader();
		l.addNewButton();
	}
}