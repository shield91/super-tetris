using System;
using System.Xml.Serialization;

namespace AssemblyCSharp
{
	public class Level : IComparable<Level> {
		[XmlAttribute("Number")]
		public int number {get; set;}
		[XmlAttribute("HighScoreInCoins")]
		public int highScoreInCoins {get; set;}
		[XmlAttribute("TimesPlayed")]
		public int timesPlayed {get; set;}

		public int CompareTo(Level that) {
			int value = that.number;
			return number.CompareTo(value);
		}
	}
}