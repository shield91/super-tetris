using UnityEngine;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace AssemblyCSharp
{
	public class PlayerProgressCampaignsSearchEngine {
		private bool chosenCampaignIsFound;
		private int currentCampaignIndex;

		public PlayerProgressCampaignsSearchEngine() {
			resetDataAboutChosenCampaign();
			resetCurrentCampaignIndex();
		}

		private void resetDataAboutChosenCampaign() {
			const bool VALUE = false;
			setDataAboutChosenCampaign(VALUE);
		}

		private void setDataAboutChosenCampaign(bool value) {
			chosenCampaignIsFound = value;
		}

		private void resetCurrentCampaignIndex() {
			const int VALUE = 0;
			setCurrentCampaignIndex(VALUE);
		}

		private void setCurrentCampaignIndex(int value) {
			currentCampaignIndex = value;
		}

		public int getChosenCampaignIndex() {
			resetCurrentCampaignIndex();
			searchForChosenCampaignIndex();
			return getResultOfSearch();
		}

		private void searchForChosenCampaignIndex() {
			while (needInSearchIsStillPresent()) {
				continueSearch();
			}
		}

		private bool needInSearchIsStillPresent() {
			bool ba = thereAreCampaignsNeedToBeAnalyzed();
			bool bb = chosenCampaignIsStillWanted();
			return ba && bb;
		}

		private bool thereAreCampaignsNeedToBeAnalyzed() {
			int n = getCampaignsNumber();
			int i = getCurrentCampaignIndex();
			return i < n;
		}

		private int getCampaignsNumber() {
			var campaigns = getCampaigns();
			return campaigns.Count;
		}

		private List<Campaign> getCampaigns() {
			var playerProgress = getPlayerProgress();
			return playerProgress.campaigns;
		}

		private PlayerProgress getPlayerProgress() {
			var serializer = getSerializerForPlayerProgress();
			var stream = getStreamForPlayerProgress();
			var playerProgress = serializer.Deserialize(stream) as PlayerProgress;
			stream.Close();
			return playerProgress;
		}
		
		private XmlSerializer getSerializerForPlayerProgress() {
			var type = typeof(PlayerProgress);
			return new XmlSerializer(type);
		}
		
		private FileStream getStreamForPlayerProgress() {
			var path = getPathToPlayerProgress();
			var mode = FileMode.OpenOrCreate;
			return new FileStream(path, mode);
		}
		
		private string getPathToPlayerProgress() {
			const string KEY = "Path To Player Progress";
			return PlayerPrefs.GetString(KEY);
		}

		private int getCurrentCampaignIndex() {
			return currentCampaignIndex;
		}

		private bool chosenCampaignIsStillWanted() {
			return !chosenCampaignIsDetected();
		}

		private bool chosenCampaignIsDetected() {
			return chosenCampaignIsFound;
		}

		private void continueSearch() {
			if (currentCampaignIsChosenOne()) {
				stopSearch();
			} else {
				moveOnToTheNextCampaign();
			}
		}

		private bool currentCampaignIsChosenOne() {
			var value = getChosenCampaignName();
			var currentCampaign = getCurrentCampaign();
			var currentCampaignName = currentCampaign.name;
			return currentCampaignName.Equals(value);
		}

		private string getChosenCampaignName() {
			const string NAME = "Name";
			var document = getDocumentForChosenCampaign();
			var documentElement = document.DocumentElement;
			var attributes = documentElement.Attributes;
			var attribute = attributes[NAME];
			return attribute.Value;
		}

		private XmlDocument getDocumentForChosenCampaign() {
			const string KEY = "Chosen Campaign";
			var document = new XmlDocument();
			var xml = PlayerPrefs.GetString(KEY);
			document.LoadXml(xml);
			return document;
		}

		private Campaign getCurrentCampaign() {
			var campaigns = getCampaigns();
			var index = getCurrentCampaignIndex();
			return campaigns[index];
		}

		private void stopSearch() {
			const bool VALUE = true;
			setDataAboutChosenCampaign(VALUE);
		}

		private void moveOnToTheNextCampaign() {
			int index = getCurrentCampaignIndex();
			int value = index + 1;
			setCurrentCampaignIndex(value);
		}

		private int getResultOfSearch() {
			if (chosenCampaignIsStillWanted()) {
				return getNewCampaignIndexAfterUpdatingPlayerProgress();
			} else {
				return getCurrentCampaignIndex();
			}
		}

		private int getNewCampaignIndexAfterUpdatingPlayerProgress() {
			addNewCampaignToPlayerProgress();
			setNewCampaignAsTheCurrentOne();
			return getCurrentCampaignIndex();
		}

		private void addNewCampaignToPlayerProgress() {
			var serializer = getSerializerForPlayerProgress();
			var o = getPlayerProgress();
			var stream = getStreamForPlayerProgress();
			var campaigns = o.campaigns;
			var item = new Campaign();
			item.name = getChosenCampaignName();
			campaigns.Add(item);
			serializer.Serialize(stream, o);
			stream.Close();
		}

		private void setNewCampaignAsTheCurrentOne() {
			int count = getCampaignsNumber();
			int value = count - 1;
			setCurrentCampaignIndex(value);
		}
	}
}