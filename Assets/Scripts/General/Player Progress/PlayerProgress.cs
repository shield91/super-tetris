using System.Collections.Generic;
using System.Xml.Serialization;

namespace AssemblyCSharp
{
	[XmlRoot("PlayerProgress")]
	public class PlayerProgress	{
		[XmlAttribute("BalanceInCoins")]
		public int balanceInCoins {get; set;}
		[XmlAttribute("GameTimeInSeconds")]
		public int gameTimeInSeconds {get; set;}
		[XmlAttribute("PointsInFreeMode")]
		public int pointsInFreeMode {get; set;}
		[XmlAttribute("PointsInCampaign")]
		public int pointsInCampaign {get; set;}
		[XmlArray("StartModes")]
		[XmlArrayItem("StartMode")]
		public List<StartMode> startModes = new List<StartMode>();
		[XmlArray("Campaigns")]
		[XmlArrayItem("Campaign")]
		public List<Campaign> campaigns = new List<Campaign>();
	}
}