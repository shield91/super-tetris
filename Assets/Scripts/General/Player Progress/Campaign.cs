using System.Collections.Generic;
using System.Xml.Serialization;

namespace AssemblyCSharp
{
	public class Campaign {
		[XmlAttribute("Name")]
		public string name;
		[XmlArray("Levels")]
		[XmlArrayItem("Level")]
		public List<Level> levels = new List<Level>();
	}
}