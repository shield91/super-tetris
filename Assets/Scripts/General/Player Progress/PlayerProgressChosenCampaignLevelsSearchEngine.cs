using UnityEngine;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace AssemblyCSharp
{
	public class PlayerProgressChosenCampaignLevelsSearchEngine {
		private string xmlForChosenLevel;
		private bool chosenLevelIsFound;
		private int currentChosenCampaignLevelIndex;

		public PlayerProgressChosenCampaignLevelsSearchEngine(string xmlForSelectedLevel) {
			setXmlForChosenLevel(xmlForSelectedLevel);
			resetSearchData();
		}

		private void setXmlForChosenLevel(string value) {
			xmlForChosenLevel = value;
		}

		private void resetSearchData() {
			resetDataAboutChosenLevel();
			resetCurrentChosenCampaignLevelIndex();
		}

		private void resetDataAboutChosenLevel() {
			const bool VALUE = false;
			setDataAboutChosenLevel(VALUE);
		}

		private void setDataAboutChosenLevel(bool value) {
			chosenLevelIsFound = value;
		}

		private void resetCurrentChosenCampaignLevelIndex() {
			const int VALUE = 0;
			setCurrentChosenCampaignLevelIndex(VALUE);
		}

		private void setCurrentChosenCampaignLevelIndex(int value) {
			currentChosenCampaignLevelIndex = value;
		}

		public int getChosenLevelIndex() {
			resetSearchData();
			searchForChosenLevelIndex();
			return getResultOfSearch();
		}

		private void searchForChosenLevelIndex() {
			while (needInSearchIsStillPresent()) {
				continueSearch();
			}
		}

		private bool needInSearchIsStillPresent() {
			bool ba = thereAreChosenCampaignLevelsNeedToBeAnalyzed();
			bool bb = chosenLevelIsStillWanted();
			return ba && bb;
		}

		private bool thereAreChosenCampaignLevelsNeedToBeAnalyzed() {
			var chosenCampaignLevels = getChosenCampaignLevels();
			var n = chosenCampaignLevels.Count;
			var i = getCurrentChosenCampaignLevelIndex();
			return i < n;
		}

		private List<Level> getChosenCampaignLevels() {
			var playerProgress = getPlayerProgress();
			var campaigns = playerProgress.campaigns;
			var index = getChosenCampaignIndex();
			var chosenCampaign = campaigns[index];
			return chosenCampaign.levels;
		}

		private PlayerProgress getPlayerProgress() {
			var serializer = getSerializerForPlayerProgress();
			var stream = getStreamForPlayerProgress();
			var playerProgress = serializer.Deserialize(stream) as PlayerProgress;
			stream.Close();
			return playerProgress;
		}
		
		private XmlSerializer getSerializerForPlayerProgress() {
			var type = typeof(PlayerProgress);
			return new XmlSerializer(type);
		}
		
		private FileStream getStreamForPlayerProgress() {
			var path = getPathToPlayerProgress();
			var mode = FileMode.OpenOrCreate;
			return new FileStream(path, mode);
		}
		
		private string getPathToPlayerProgress() {
			const string KEY = "Path To Player Progress";
			return PlayerPrefs.GetString(KEY);
		}

		private int getChosenCampaignIndex() {
			var engine = new PlayerProgressCampaignsSearchEngine();
			return engine.getChosenCampaignIndex();
		}

		private int getCurrentChosenCampaignLevelIndex() {
			return currentChosenCampaignLevelIndex;
		}

		private bool chosenLevelIsStillWanted() {
			return !chosenLevelIsDetected();
		}

		private bool chosenLevelIsDetected() {
			return chosenLevelIsFound;
		}

		private void continueSearch() {
			if (currentChosenCampaignLevelIsChosenOne()) {
				stopSearch();
			} else {
				moveOnToTheNextChosenCampaignLevel();
			}
		}

		private bool currentChosenCampaignLevelIsChosenOne() {
			var chosenLevelNumber = getChosenLevelNumber();
			var currentChosenCampaignLevel = getCurrentChosenCampaignLevel();
			var currentChosenCampaignLevelNumber = currentChosenCampaignLevel.number;
			return currentChosenCampaignLevelNumber == chosenLevelNumber;
		}

		private int getChosenLevelNumber() {
			const string NAME = "Number";
			var document = getDocumentForChosenLevel();
			var documentElement = document.DocumentElement;
			var attributes = documentElement.Attributes;
			var attribute = attributes[NAME];
			var s = attribute.Value;
			return int.Parse(s);
		}

		private XmlDocument getDocumentForChosenLevel() {
			var document = new XmlDocument();
			var xml = getXmlForChosenLevel();
			document.LoadXml(xml);
			return document;
		}

		private string getXmlForChosenLevel() {
			return xmlForChosenLevel;
		}

		private Level getCurrentChosenCampaignLevel() {
			var levels = getChosenCampaignLevels();
			var index = getCurrentChosenCampaignLevelIndex();
			return levels[index];
		}

		private void stopSearch() {
			const bool VALUE = true;
			setDataAboutChosenLevel(VALUE);
		}

		private void moveOnToTheNextChosenCampaignLevel() {
			int index = getCurrentChosenCampaignLevelIndex();
			int value = index + 1;
			setCurrentChosenCampaignLevelIndex(value);
		}

		private int getResultOfSearch() {
			return getCurrentChosenCampaignLevelIndex();
		}
	}
}