using System.Xml.Serialization;

namespace AssemblyCSharp
{
	public class StartMode {
		[XmlAttribute("Name")]
		public string name {get; set;}
		[XmlAttribute("Speed")]
		public int speed {get; set;}
		[XmlAttribute("CoinsToOpenNextMode")]
		public int coinsToOpenNextMode {get; set;}
		[XmlAttribute("TimesPlayed")]
		public int timesPlayed {get; set;}
	}
}