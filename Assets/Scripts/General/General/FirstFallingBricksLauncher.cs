﻿using UnityEngine;

namespace AssemblyCSharp
{
	public class FirstFallingBricksLauncher {
		private Transform gameWindow;
		private int currentSpawnerIndex;

		public FirstFallingBricksLauncher(Transform playWindow) {
			setGameWindow(playWindow);
			resetCurrentSpawnerIndex();
		}

		private void setGameWindow(Transform value) {
			gameWindow = value;
		}

		private void resetCurrentSpawnerIndex() {
			const int VALUE = 0;
			setCurrentSpawnerIndex(VALUE);
		}

		private void setCurrentSpawnerIndex(int value) {
			currentSpawnerIndex = value;
		}

		public void launchBricks() {
			resetCurrentSpawnerIndex();
			launchFallingBricks();
		}

		private void launchFallingBricks() {
			while (thereAreSpawnersNeedToBeProcessed()) {
				processCurrentSpawner();
			}
		}

		private bool thereAreSpawnersNeedToBeProcessed() {
			int n = getSpawnersNumber();
			int i = getCurrentSpawnerIndex();
			return i < n;
		}

		private int getSpawnersNumber() {
			var spawners = getSpawnersFromGameWindow();
			return spawners.Length;
		}

		private Spawner[] getSpawnersFromGameWindow() {
			var playWindow = getGameWindow();
			return playWindow.GetComponentsInChildren<Spawner>();
		}

		private Transform getGameWindow() {
			return gameWindow;
		}

		private int getCurrentSpawnerIndex() {
			return currentSpawnerIndex;
		}

		private void processCurrentSpawner() {
			launchCurrentFallingBrick();
			moveOnToTheNextSpawner();
		}

		private void launchCurrentFallingBrick() {
			const bool VALUE = true;
			var spawners = getSpawnersFromGameWindow();
			var index = getCurrentSpawnerIndex();
			var currentSpawner = spawners[index];
			currentSpawner.enabled = VALUE;
		}

		private void moveOnToTheNextSpawner() {
			const int STEP = 1;
			int index = getCurrentSpawnerIndex();
			int value = index + STEP;
			setCurrentSpawnerIndex(value);
		}
	}
}