using UnityEngine;
using System;
using System.Xml;

namespace AssemblyCSharp
{
	public abstract class ItemsLoader {
		private XmlNode currentItem;

		public ItemsLoader() {
			resetCurrentItem();
		}

		private void resetCurrentItem() {
			var xpath = getXpathForItems();
			var items = getItems();
			var value = items.SelectSingleNode(xpath);
			setCurrentItem(value);
		}

		protected abstract string getXpathForItems();

		public XmlDocument getItems() {
			var items = new XmlDocument();
			var xml = getXmlForItems();
			items.LoadXml(xml);
			return items;
		}

		protected abstract string getXmlForItems();

		protected void setCurrentItem(XmlNode value) {
			currentItem = value;
		}

		public void load() {
			resetCurrentItem();
			charge();
		}

		private void charge() {
			while (thereAreItemsNeedToBeLoaded()) {
				loadCurrentItem();
			}
		}

		private bool thereAreItemsNeedToBeLoaded() {
			var item = getCurrentItem();
			return item != null;
		}

		protected XmlNode getCurrentItem() {
			return currentItem;
		}

		private void loadCurrentItem() {
			loadItem();
			moveOnToTheNextItem();
		}

		protected void loadItem() {
			instantiateCurrentItem();
			setUpCurrentItem();
		}

		private void instantiateCurrentItem() {
			var original = getOriginalForCurrentItem();
			var parent = getParentForItems();
			var go = GameObject.Instantiate(original, parent);
			var obj = go.GetComponent<Collider2D>();
			go.name = getNameForCurrentItem();
			GameObject.Destroy(obj);
		}

		protected virtual GameObject getOriginalForCurrentItem() {
			var path = getPathForCurrentItem();
			var o = Resources.Load(path);
			return (GameObject) o;
		}

		protected abstract string getPathForCurrentItem();

		protected string getValueFor(string name) {
			try {
				return getValueForAttribute(name);
			} catch (Exception e) {
				return getEmptyStringBecauseOf(e);
			}
		}

		private string getValueForAttribute(string name) {
			var item = getCurrentItem();
			var xac = item.Attributes;
			var attribute = xac[name];
			return attribute.Value;
		}

		private string getEmptyStringBecauseOf(Exception e) {
			handle(e);
			return getEmptyString();
		}

		private void handle(Exception e) {
			e.GetType();
		}

		private string getEmptyString() {
			return "";
		}

		protected abstract Transform getParentForItems();

		protected abstract string getNameForCurrentItem();

		private void setUpCurrentItem() {
			setUpScaleForCurrentItem();
			setUpSpecificParametersForCurrentItem();
		}

		private void setUpScaleForCurrentItem() {
			var panel = getParentForItems();
			var button = getInstantiatedItem();
			var multiplier = getMultiplierForItemScale();
			button.localScale = panel.localScale * multiplier;
		}

		protected Transform getInstantiatedItem() {
			var panel = getParentForItems();
			var buttonsQuantity = panel.childCount;
			var index = buttonsQuantity - 1;
			return panel.GetChild(index);
		}

		protected virtual float getMultiplierForItemScale() {
			return 1f;
		}

		protected abstract void setUpSpecificParametersForCurrentItem();

		private void moveOnToTheNextItem() {
			var item = getCurrentItem();
			var value = item.NextSibling;
			setCurrentItem(value);
		}
	}
}