using UnityEngine;
using UnityEngine.UI;
using System.Xml;

namespace AssemblyCSharp
{
	public abstract class ButtonsLoader : ItemsLoader {
		protected override string getXmlForItems() {
			string key = getKeyForModifiedDocument();
			string defaultValue = getDefaultValueForXmlForItems();
			return PlayerPrefs.GetString(key, defaultValue);
		}

		protected abstract string getKeyForModifiedDocument();

		private string getDefaultValueForXmlForItems() {
			string key = getKeyForOriginalDocument();
			return PlayerPrefs.GetString(key);
		}

		protected abstract string getKeyForOriginalDocument();

		protected override Transform getParentForItems() {
			const string NAME = "Scroll Panel/Buttons";
			const string TAG = "Canvas";
			var go = GameObject.FindWithTag(TAG);
			var t = go.transform;
			return t.Find(NAME);
		}

		protected Text getTextComponentFor(string name) {
			var button = getInstantiatedItem();
			var buttonParameter = button.Find(name);
			return buttonParameter.GetComponent<Text>();
		}

		public void addNewButton() {
			addNewItemToXml();
			setLastItemToBeTheCurrentOne();
			loadItem();
		}

		private void addNewItemToXml() {
			addXmlElementForNewItemToDocument();
			adjustMainAttributeForNewItemXmlElement();
		}

		private void addXmlElementForNewItemToDocument() {
			var key = getKeyForDocumentWithNewItem();
			var document = getDocumentWithNewItemAdded();
			var value = document.OuterXml;
			PlayerPrefs.SetString(key, value);
		}

		protected abstract string getKeyForDocumentWithNewItem();

		private XmlDocument getDocumentWithNewItemAdded() {
			var name = getNewItemName();
			var document = getDocumentWithNewItem();
			var newChild = document.CreateElement(name);
			var documentElement = document.DocumentElement;
			documentElement.AppendChild(newChild);
			return document;
		}

		protected abstract string getNewItemName();

		public XmlDocument getDocumentWithNewItem() {
			var key = getKeyForDocumentWithNewItem();
			var items = getItems();
			var defaultValue = items.OuterXml;
			var xml = PlayerPrefs.GetString(key, defaultValue);
			var document = new XmlDocument();
			document.LoadXml(xml);
			return document;
		}

		private void adjustMainAttributeForNewItemXmlElement() {
			var key = getKeyForDocumentWithNewItem();
			var document = getDocumentWithAdjustedMainAttributeOfNewItem();
			var value = document.OuterXml;
			PlayerPrefs.SetString(key, value);
		}

		private XmlDocument getDocumentWithAdjustedMainAttributeOfNewItem() {
			var name = getNameForMainAttributeOfNewItem();
			var document = getDocumentWithNewItem();
			var documentElement = document.DocumentElement;
			var item = documentElement.LastChild;
			var node = document.CreateAttribute(name);
			var attributes = item.Attributes;
			node.Value = getValueForMainAttributeOfNewItem();
			attributes.Append(node);
			return document;
		}

		protected abstract string getNameForMainAttributeOfNewItem();

		protected abstract string getValueForMainAttributeOfNewItem();

		private void setLastItemToBeTheCurrentOne() {
			var document = getDocumentWithNewItem();
			var documentElement = document.DocumentElement;
			var value = documentElement.LastChild;
			setCurrentItem(value);
		}
	}
}