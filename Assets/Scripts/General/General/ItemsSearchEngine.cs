using System.Xml;

namespace AssemblyCSharp
{
	public abstract class ItemsSearchEngine {
		private XmlNode currentItem;
		private string wantedItemName;

		public ItemsSearchEngine() {
			resetCurrentItem();
			resetWantedItemName();
		}

		private void resetCurrentItem() {
			var xpath = getXpathForItems();
			var loader = getItemsLoader();
			var items = loader.getDocumentWithNewItem();
			var value = items.SelectSingleNode(xpath);
			setCurrentItem(value);
		}

		protected abstract string getXpathForItems();

		protected abstract ButtonsLoader getItemsLoader();

		private void setCurrentItem(XmlNode value) {
			currentItem = value;
		}

		private void resetWantedItemName() {
			const string VALUE = "";
			setWantedItemName(VALUE);
		}

		private void setWantedItemName(string value) {
			wantedItemName = value;
		}

		public string get(string name) {
			setWantedItemName(name);
			lookForWantedItem();
			return getResultOfSearch();
		}

		private void lookForWantedItem() {
			while (needInSearchIsStillPresent()) {
				moveOnToTheNextItem();
			}
		}

		private bool needInSearchIsStillPresent() {
			var ba = currentItemHasItsFollower();
			var bb = wantedItemIsStillBeingWanted();
			return ba && bb;
		}

		private bool currentItemHasItsFollower() {
			var item = getCurrentItem();
			return item != null;
		}

		public XmlNode getCurrentItem() {
			return currentItem;
		}

		private bool wantedItemIsStillBeingWanted() {
			var currentItemMainAttributeValue = getValueForCurrentItemMainAttribute();
			var value = getWantedItemName();
			return !currentItemMainAttributeValue.Equals(value);
		}

		private string getValueForCurrentItemMainAttribute() {
			string name = getNameForCurrentItemMainAttribute();
			return getValueFor(name);
		}

		protected abstract string getNameForCurrentItemMainAttribute();

		private string getValueFor(string name) {
			var item = getCurrentItem();
			var xac = item.Attributes;
			var attribute = xac[name];
			return attribute.Value;
		}

		private string getWantedItemName() {
			return wantedItemName;
		}

		private void moveOnToTheNextItem() {
			var item = getCurrentItem();
			var value = item.NextSibling;
			setCurrentItem(value);
		}

		private string getResultOfSearch() {
			var item = getCurrentItem();
			return item.OuterXml;
		}
	}
}