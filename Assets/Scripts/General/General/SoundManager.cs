﻿using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour {	
	private void Start() {
		setUpButton();
	}

	private void setUpButton() {
		var toggle = GetComponent<Toggle>();
		var value = toggleIsOn();
		toggle.isOn = value;
	}

	private bool toggleIsOn() {
		const string KEY = "Sound Is Off";
		bool b = PlayerPrefs.HasKey(KEY);
		return !b;
	}

	public void toggleSound() {
		if (soundIsOn()) {
			turnSoundOff();
		} else {
			turnSoundOn();
		}
	}

	private bool soundIsOn() {
		var source = GetComponent<AudioSource>();
		return !source.mute;
	}

	private void turnSoundOff() {
		toggleAudio();
		makeSoundToggleBackgroundImageVisible();
		setLabelForSoundOff();
		setButtonStateForSoundOff();
	}

	private void toggleAudio() {
		var source = GetComponent<AudioSource>();
		var mute = source.mute;
		source.mute = !mute;
	}

	private void makeSoundToggleBackgroundImageVisible() {
		const float R = 255f / 255f;
		const float G = 255f / 255f;
		const float B = 255f / 255f;
		const float A = 255f / 255f;
		var color = new Color(R, G, B, A);
		setSoundToggleBackgroundImageColor(color);
	}

	private void setSoundToggleBackgroundImageColor(Color color) {
		const string NAME = "Background";
		var background = transform.Find(NAME);
		var image = background.GetComponent<Image>();
		image.color = color;
	}

	private void setLabelForSoundOff() {
		const string TITLE = "SOUND OFF";
		setLabelFor(TITLE);
	}

	private void setLabelFor(string title) {
		var t = transform.GetComponentInChildren<Text>();
		t.text = title;
	}

	private void setButtonStateForSoundOff() {
		const string KEY = "Sound Is Off";
		const string VALUE = "Yes";
		PlayerPrefs.SetString(KEY, VALUE);
	}

	private void turnSoundOn() {
		toggleAudio();
		makeSoundToggleBackgroundImageInvisible();
		setLabelForSoundOn();
		setButtonStateForSoundOn();
	}

	private void makeSoundToggleBackgroundImageInvisible() {
		const float R = 255f / 255f;
		const float G = 255f / 255f;
		const float B = 255f / 255f;
		const float A = 0f / 255f;
		var color = new Color(R, G, B, A);
		setSoundToggleBackgroundImageColor(color);
	}

	private void setLabelForSoundOn() {
		const string TITLE = "SOUND ON";
		setLabelFor(TITLE);
	}

	private void setButtonStateForSoundOn() {
		const string KEY = "Sound Is Off";
		PlayerPrefs.DeleteKey(KEY);
	}
}