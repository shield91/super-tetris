﻿using UnityEngine;
using System.Xml;
using System.Collections.Generic;

namespace AssemblyCSharp
{
	public class ProgressInCampaignCalculator {
		private StatusScreen requester;
		private int campaignsActualLevelsNumber;
		private int campaignsFinishedLevelsNumber;
		private int currentCampaignIndex;

		public ProgressInCampaignCalculator(StatusScreen suppliant) {
			setRequester(suppliant);
			resetCalculationData();
		}

		private void setRequester(StatusScreen value) {
			requester = value;
		}

		private void resetCalculationData() {
			resetCampaignsActualLevelsNumber();
			resetCampaignsFinishedLevelsNumber();
			resetCurrentCampaignIndex();
		}

		private void resetCampaignsActualLevelsNumber() {
			const int VALUE = 0;
			setCampaignsActualLevelsNumber(VALUE);
		}

		private void setCampaignsActualLevelsNumber(int value) {
			campaignsActualLevelsNumber = value;
		}

		private void resetCampaignsFinishedLevelsNumber() {
			const int VALUE = 0;
			setCampaignsFinishedLevelsNumber(VALUE);
		}

		private void setCampaignsFinishedLevelsNumber(int value) {
			campaignsFinishedLevelsNumber = value;
		}

		private void resetCurrentCampaignIndex() {
			const int VALUE = 0;
			setCurrentCampaignIndex(VALUE);
		}

		private void setCurrentCampaignIndex(int value) {
			currentCampaignIndex = value;
		}

		public string getProgressInCampaign() {
			resetCalculationData();
			calculateCampaignsFinishedLevelNumber();
			resetCurrentCampaignIndex();
			calculateCampaignsActualLevelsNumber();
			return getCalculationsResult();
		}

		private void calculateCampaignsFinishedLevelNumber() {
			while(thereAreOpenedCampaignsNeedToBeAnalyzed()) {
				processCurrentOpenedCampaign();
			}
		}

		private bool thereAreOpenedCampaignsNeedToBeAnalyzed() {
			int i = getCurrentCampaignIndex();
			int n = getOpenedCampaignsNumber();
			return i < n;
		}

		private int getCurrentCampaignIndex() {
			return currentCampaignIndex;
		}

		private int getOpenedCampaignsNumber() {
			var campaigns = getOpenedCampaigns();
			return campaigns.Count;
		}

		private List<Campaign> getOpenedCampaigns() {
			var suppliant = getRequester();
			var playerProgress = suppliant.getPlayerProgress();
			return playerProgress.campaigns;
		}

		private StatusScreen getRequester() {
			return requester;
		}

		private void processCurrentOpenedCampaign() {
			updateCampaignsFinishedLevelsNumberByAddingCurrentOpenedCampaignLevelsNumber();
			moveOnToTheNextCampaign();
		}

		private void updateCampaignsFinishedLevelsNumberByAddingCurrentOpenedCampaignLevelsNumber() {
			var campaigns = getOpenedCampaigns();
			var index = getCurrentCampaignIndex();
			var currentCampaign = campaigns[index];
			var currentCampaignLevels = currentCampaign.levels;
			var currentCampaignLevelsNumber = currentCampaignLevels.Count;
			var campaignsLevelsNumber = getCampaignsFinishedLevelsNumber();
			var value = currentCampaignLevelsNumber + campaignsLevelsNumber;
			setCampaignsFinishedLevelsNumber(value);
		}

		private int getCampaignsFinishedLevelsNumber() {
			return campaignsFinishedLevelsNumber;
		}

		private void moveOnToTheNextCampaign() {
			const int STEP = 1;
			int index = getCurrentCampaignIndex();
			int value = index + STEP;
			setCurrentCampaignIndex(value);
		}

		private void calculateCampaignsActualLevelsNumber() {
			while (thereAreCreatedCampaignsNeedToBeAnalyzed()) {
				processCurrentCreatedCampaign();
			}
		}

		private bool thereAreCreatedCampaignsNeedToBeAnalyzed() {
			int i = getCurrentCampaignIndex();
			int n = getCreatedCampaignsNumber();
			return i < n;
		}

		private int getCreatedCampaignsNumber() {
			var campaigns = getCreatedCampaigns();
			return campaigns.Count;
		}

		private XmlNodeList getCreatedCampaigns() {
			const string XPATH = "Campaigns/Campaign";
			var document = getDocumentForCreatedCampaigns();
			return document.SelectNodes(XPATH);
		}

		private XmlDocument getDocumentForCreatedCampaigns() {
			const string KEY = "Campaigns";
			var xml = PlayerPrefs.GetString(KEY);
			var document = new XmlDocument();
			document.LoadXml(xml);
			return document;
		}

		private void processCurrentCreatedCampaign() {
			updateCampaignsActualLevelsNumberByAddingCurrentCreatedCampaignLevelsNumber();
			moveOnToTheNextCampaign();
		}

		private void updateCampaignsActualLevelsNumberByAddingCurrentCreatedCampaignLevelsNumber() {
			var campaigns = getCreatedCampaigns();
			var index = getCurrentCampaignIndex();
			var currentCampaign = campaigns.Item(index);
			var currentCampaignLevels = currentCampaign.ChildNodes;
			var currentCampaignLevelsNumber = currentCampaignLevels.Count;
			var campaignsLevelsNumber = getCampaignsActualLevelsNumber();
			var value = campaignsLevelsNumber + currentCampaignLevelsNumber;
			setCampaignsActualLevelsNumber(value);
		}

		private int getCampaignsActualLevelsNumber() {
			return campaignsActualLevelsNumber;
		}

		private string getCalculationsResult() {
			const string SEPARATOR = "/";
			int finishedLevelsNumber = getCampaignsFinishedLevelsNumber();
			int actualLevelsNumber = getCampaignsActualLevelsNumber();
			return finishedLevelsNumber + SEPARATOR + actualLevelsNumber;
		}
	}
}