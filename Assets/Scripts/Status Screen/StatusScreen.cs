﻿using UnityEngine;
using UnityEngine.UI;
using AssemblyCSharp;
using System;
using System.IO;
using System.Xml.Serialization;

public class StatusScreen : MonoBehaviour {
	private FileStream fileStream;

	private void Start() {
		showGameTime();
		showPointsInFreeMode();
		showPointsInCampaign();
		showPointsSummary();
		showProgressInCampaign();
		showMostPlayedLevel();
	}

	private void showGameTime() {
		var t = getTextComponentForGameTime();
		var value = getTextForGameTime();
		t.text = value;
	}

	private Text getTextComponentForGameTime() {
		const string TITLE = "Game's Time";
		return getTextComponentFor(TITLE);
	}

	private Text getTextComponentFor(string title) {
		const string NAME = "/Value";
		var name = title + NAME;
		var child = transform.Find(name);
		return child.GetComponent<Text>();
	}

	private string getTextForGameTime() {
		const string SEPARATOR = ":";
		string timeInHours = getTimeInHours();
		string timeInMinutes = getTimeInMinutes();
		string timeInSeconds = getTimeInSeconds();
		return timeInHours + SEPARATOR + timeInMinutes + SEPARATOR + timeInSeconds;
	}

	private string getTimeInHours() {
		const int DIVIDER = 3600;
		int gameTime = getGameTimeFromPlayerProgress();
		int value = gameTime / DIVIDER;
		return getProcessedTimeFor(value);
	}

	private int getGameTimeFromPlayerProgress() {
		var playerProgress = getPlayerProgress();
		return playerProgress.gameTimeInSeconds;
	}

	public PlayerProgress getPlayerProgress() {
		try {
			return getAlreadyCreatedPlayerProgress();
		} catch (Exception e) {
			return getNewlyCreatedPlayerProgressBecauseOf(e);
		}
	}

	private PlayerProgress getAlreadyCreatedPlayerProgress() {
		var serializer = getSerializerForPlayerProgress();
		var stream = getFileStream();
		var playerProgress = serializer.Deserialize(stream) as PlayerProgress;
		stream.Close();
		return playerProgress;
	}

	private XmlSerializer getSerializerForPlayerProgress() {
		var type = typeof(PlayerProgress);
		return new XmlSerializer(type);
	}

	private FileStream getFileStream() {
		resetFileStream();
		return getStream();
	}

	private void resetFileStream() {
		var value = getStreamForPlayerProgress();
		setFileStream(value);
	}

	private FileStream getStreamForPlayerProgress() {
		var path = getPathToPlayerProgress();
		var mode = FileMode.OpenOrCreate;
		return new FileStream(path, mode);
	}

	private string getPathToPlayerProgress() {
		const string KEY = "Path To Player Progress";
		return PlayerPrefs.GetString(KEY);
	}

	private void setFileStream(FileStream value) {
		fileStream = value;
	}

	private FileStream getStream() {
		return fileStream;
	}

	private PlayerProgress getNewlyCreatedPlayerProgressBecauseOf(Exception e) {
		handle(e);
		return getNewlyCreatedPlayerProgress();
	}

	private void handle(Exception e) {
		e.GetType();
	}

	private PlayerProgress getNewlyCreatedPlayerProgress() {
		const int BALANCE = 2000;
		var serializer = getSerializerForPlayerProgress();
		var stream = getStream();
		var o = new PlayerProgress();
		o.balanceInCoins = BALANCE;
		serializer.Serialize(stream, o);
		stream.Close();
		return o;
	}

	private string getProcessedTimeFor(int value) {
		if (numberHasOnlyOneDigit(value)) {
			return getProcessedNumberFor(value);
		} else {
			return getUnprocessedNumberFor(value);
		}
	}

	private bool numberHasOnlyOneDigit(int value) {
		const int VALUE = 10;
		return value < VALUE;
	}

	private string getProcessedNumberFor(int value) {
		const string VALUE = "0";
		return VALUE + value;
	}

	private string getUnprocessedNumberFor(int value) {
		return value.ToString();
	}

	private string getTimeInMinutes() {
		const int DIVIDER = 60;
		int gameTime = getGameTimeFromPlayerProgress();
		int value = gameTime / DIVIDER % DIVIDER;
		return getProcessedTimeFor(value);
	}

	private string getTimeInSeconds() {
		const int DIVIDER = 60;
		int gameTime = getGameTimeFromPlayerProgress();
		int value = gameTime % DIVIDER;
		return getProcessedTimeFor(value);
	}

	private void showPointsInFreeMode() {
		var t = getTextComponentForPointsInFreeMode();
		var value = getTextForPointsInFreeMode();
		t.text = value;
	}

	private Text getTextComponentForPointsInFreeMode() {
		const string TITLE = "Points In Free Mod";
		return getTextComponentFor(TITLE);
	}

	private string getTextForPointsInFreeMode() {
		var playerProgress = getPlayerProgress();
		var pointsInFreeMode = playerProgress.pointsInFreeMode;
		return pointsInFreeMode.ToString();
	}

	private void showPointsInCampaign() {
		var t = getTextComponentForPointsInCampaign();
		var value = getTextForPointsInCampaign();
		t.text = value;
	}

	private Text getTextComponentForPointsInCampaign() {
		const string TITLE = "Points In Campaign";
		return getTextComponentFor(TITLE);
	}

	private string getTextForPointsInCampaign() {
		var playerProgress = getPlayerProgress();
		var pointsInCampaign = playerProgress.pointsInCampaign;
		return pointsInCampaign.ToString();
	}

	private void showPointsSummary() {
		var t = getTextComponentForPointsSummary();
		var value = getTextForPointsSummary();
		t.text = value;
	}

	private Text getTextComponentForPointsSummary() {
		const string TITLE = "Points Summary";
		return getTextComponentFor(TITLE);
	}

	private string getTextForPointsSummary() {
		var playerProgress = getPlayerProgress();
		var pointsInFreeMode = playerProgress.pointsInFreeMode;
		var pointsInCampaign = playerProgress.pointsInCampaign;
		var pointsSummary = pointsInFreeMode + pointsInCampaign;
		return pointsSummary.ToString();
	}

	private void showProgressInCampaign() {
		var t = getTextComponentForProgressInCampaign();
		var value = getTextForProgressInCampaign();
		t.text = value;
	}

	private Text getTextComponentForProgressInCampaign() {
		const string TITLE = "Progress In Campaign";
		return getTextComponentFor(TITLE);
	}

	private string getTextForProgressInCampaign() {
		var suppliant = this;
		var calculator = new ProgressInCampaignCalculator(suppliant);
		return calculator.getProgressInCampaign();
	}

	private void showMostPlayedLevel() {
		var t = getTextComponentForMostPlayedLevel();
		var value = getTextForMostPlayedLevel();
		t.text = value;
	}

	private Text getTextComponentForMostPlayedLevel() {
		const string TITLE = "Most Played Level";
		return getTextComponentFor(TITLE);
	}

	private string getTextForMostPlayedLevel() {
		var suppliant = this;
		var calculator = new MostPlayedLevelCalculator(suppliant);
		return calculator.getMostPlayedLevel();
	}

	public void goToHomeScreen() {
		const string NAME = "Home Screen";
		Application.LoadLevel(NAME);
	}
}