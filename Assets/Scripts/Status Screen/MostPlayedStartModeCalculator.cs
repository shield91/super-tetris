﻿using System.Collections.Generic;

namespace AssemblyCSharp
{
	public class MostPlayedStartModeCalculator {
		private StatusScreen requester;
		private StartMode mostPlayedStartMode;
		private int currentStartModeIndex;

		public MostPlayedStartModeCalculator(StatusScreen suppliant) {
			setRequester(suppliant);
			resetCalculationData();
		}

		private void setRequester(StatusScreen value) {
			requester = value;
		}

		private void resetCalculationData() {
			resetMostPlayedStartMode();
			resetCurrentStartModeIndex();
		}

		private void resetMostPlayedStartMode() {
			var value = getInitialMostPlayedStartMode();
			setMostPlayedStartMode(value);
		}

		private StartMode getInitialMostPlayedStartMode() {
			const int INDEX = 0;
			var startModes = getStartModesFromRequester();
			return startModes[INDEX];
		}

		private List<StartMode> getStartModesFromRequester() {
			var suppliant = getRequester();
			var playerProgress = suppliant.getPlayerProgress();
			return playerProgress.startModes;
		}

		private StatusScreen getRequester() {
			return requester;
		}

		private void setMostPlayedStartMode(StartMode value) {
			mostPlayedStartMode = value;
		}

		private void resetCurrentStartModeIndex() {
			const int VALUE = 0;
			setCurrentStartModeIndex(VALUE);
		}

		private void setCurrentStartModeIndex(int value) {
			currentStartModeIndex = value;
		}

		public StartMode retrieveMostPlayedStartMode() {
			resetCalculationData();
			calculateMostPlayedStartMode();
			return getCalculationsResult();
		}

		private void calculateMostPlayedStartMode() {
			while(thereAreStartModesNeedToBeProcessed()) {
				processCurrentStartMode();
			}
		}

		private bool thereAreStartModesNeedToBeProcessed() {
			int i = getCurrentStartModeIndex();
			int n = getStartModesNumber();
			return i < n;
		}

		private int getCurrentStartModeIndex() {
			return currentStartModeIndex;
		}

		private int getStartModesNumber() {
			var startModes = getStartModesFromRequester();
			return startModes.Count;
		}

		private void processCurrentStartMode() {
			processCurrentMode();
			moveOnToTheNextMode();
		}

		private void processCurrentMode() {
			if (currentStartModeDeservesToBeTheMostPlayedOne()) {
				setCurrentStartModeAsTheMostPlayedOne();
			}
		}

		private bool currentStartModeDeservesToBeTheMostPlayedOne() {
			var currentStartMode = getCurrentStartMode();
			var mostPlayedMode = getMostPlayedStartMode();
			return currentStartMode.timesPlayed > mostPlayedMode.timesPlayed;
		}

		private StartMode getCurrentStartMode() {
			var startModes = getStartModesFromRequester();
			var index = getCurrentStartModeIndex();
			return startModes[index];
		}

		private StartMode getMostPlayedStartMode() {
			return mostPlayedStartMode;
		}

		private void setCurrentStartModeAsTheMostPlayedOne() {
			var value = getCurrentStartMode();
			setMostPlayedStartMode(value);
		}

		private void moveOnToTheNextMode() {
			const int STEP = 1;
			int index = getCurrentStartModeIndex();
			int value = index + STEP;
			setCurrentStartModeIndex(value);
		}

		private StartMode getCalculationsResult() {
			return getMostPlayedStartMode();
		}
	}
}