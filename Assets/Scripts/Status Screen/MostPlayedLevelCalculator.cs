﻿using System;

namespace AssemblyCSharp
{
	public class MostPlayedLevelCalculator {
		private StatusScreen requester;

		public MostPlayedLevelCalculator(StatusScreen suppliant) {
			setRequester(suppliant);
		}

		private void setRequester(StatusScreen value) {
			requester = value;
		}

		public string getMostPlayedLevel() {
			try {
				return getMostPlayedLevelName();
			} catch (Exception e) {
				return getMostPlayedLevelBecauseOf(e);
			}
		}

		private string getMostPlayedLevelName() {
			if (mostPlayedStartModeTurnsOutToBeTheMostPlayedLevel()) {
				return getMostPlayedStartModeName();
			} else {
				return getMostPlayedCampaignLevelName();
			}
		}

		private bool mostPlayedStartModeTurnsOutToBeTheMostPlayedLevel() {
			var mostPlayedStartMode = getMostPlayedStartMode();
			var mostPlayedCampaignLevel = getMostPlayedCampaignLevel();
			return mostPlayedStartMode.timesPlayed >= mostPlayedCampaignLevel.timesPlayed;
		}

		private StartMode getMostPlayedStartMode() {
			var suppliant = getRequester();
			var calculator = new MostPlayedStartModeCalculator(suppliant);
			return calculator.retrieveMostPlayedStartMode();
		}

		private StatusScreen getRequester() {
			return requester;
		}

		private Level getMostPlayedCampaignLevel() {
			var suppliant = getRequester();
			var calculator = new MostPlayedCampaignLevelCalculator(suppliant);
			return calculator.retrieveMostPlayedCampaignLevel();
		}

		private string getMostPlayedStartModeName() {
			var mostPlayedStartMode = getMostPlayedStartMode();
			var mostPlayedStartModeName = mostPlayedStartMode.name;
			return mostPlayedStartModeName.ToUpper();
		}

		private string getMostPlayedCampaignLevelName() {
			const string HEADER = "LEVEL ";
			var mostPlayedCampaignLevel = getMostPlayedCampaignLevel();
			var mostPlayedCampaignLevelNumber = mostPlayedCampaignLevel.number;
			return HEADER + mostPlayedCampaignLevelNumber;
		}

		private string getMostPlayedLevelBecauseOf(Exception e) {
			handle(e);
			return getMostPlayedLevelInCaseOfException();
		}

		private void handle(Exception e) {
			e.GetType();
		}

		private string getMostPlayedLevelInCaseOfException() {
			try {
				return getMostPlayedStartModeName();
			} catch (Exception e) {
				return getMostPlayedCampaignLevelNameBecauseOf(e);
			}
		}

		private string getMostPlayedCampaignLevelNameBecauseOf(Exception e) {
			handle(e);
			return getMostPlayedCampaignLevelNameInCaseOfException();
		}

		private string getMostPlayedCampaignLevelNameInCaseOfException() {
			if (thereAreNoCampaignLevels()) {
				return getStringByDefault();
			} else {
				return getMostPlayedCampaignLevelName();
			}
		}

		private bool thereAreNoCampaignLevels() {
			var mostPlayedCampaignLevel = getMostPlayedCampaignLevel();
			var mostPlayedCampaignLevelNumber = mostPlayedCampaignLevel.number;
			return mostPlayedCampaignLevelNumber == 0;
		}

		private string getStringByDefault() {
			return "NONE";
		}
	}
}