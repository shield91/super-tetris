﻿using System.Collections.Generic;

namespace AssemblyCSharp
{
	public class MostPlayedCampaignLevelCalculator {
		private StatusScreen requester;
		private Level mostPlayedCampaignLevel;
		private int currentCampaignIndex;
		private int currentLevelIndex;

		public MostPlayedCampaignLevelCalculator(StatusScreen suppliant) {
			setRequester(suppliant);
			resetMostPlayedCampaignLevel();
			resetCurrentCampaignIndex();
			resetCurrentLevelIndex();
		}

		private void setRequester(StatusScreen value) {
			requester = value;
		}

		private void resetMostPlayedCampaignLevel() {
			var value = new Level();
			setMostPlayedCampaignLevel(value);
		}

		private void setMostPlayedCampaignLevel(Level value) {
			mostPlayedCampaignLevel = value;
		}

		private void resetCurrentCampaignIndex() {
			const int VALUE = 0;
			setCurrentCampaignIndex(VALUE);
		}

		private void setCurrentCampaignIndex(int value) {
			currentCampaignIndex = value;
		}

		private void resetCurrentLevelIndex() {
			const int VALUE = 0;
			setCurrentLevelIndex(VALUE);
		}

		private void setCurrentLevelIndex(int value) {
			currentLevelIndex = value;
		}

		public Level retrieveMostPlayedCampaignLevel() {
			resetMostPlayedCampaignLevel();
			resetCurrentCampaignIndex();
			resetCurrentLevelIndex();
			calculateMostPlayedCampaignLevel();
			return getCalculationsResult();
		}

		private void calculateMostPlayedCampaignLevel() {
			while (thereAreCampaignsNeedToBeProcessed()) {
				processCurrentCampaign();
			}
		}

		private bool thereAreCampaignsNeedToBeProcessed() {
			int i = getCurrentCampaignIndex();
			int n = getCampaignsNumber();
			return i < n;
		}

		private int getCurrentCampaignIndex() {
			return currentCampaignIndex;
		}

		private int getCampaignsNumber() {
			var campaigns = getCampaignsFromRequester();
			return campaigns.Count;
		}

		private List<Campaign> getCampaignsFromRequester() {
			var suppliant = getRequester();
			var playerProgress = suppliant.getPlayerProgress();
			return playerProgress.campaigns;
		}

		private StatusScreen getRequester() {
			return requester;
		}

		private void processCurrentCampaign() {
			processRecurringCampaign();
			moveOnToTheNextCampaign();
		}

		private void processRecurringCampaign() {
			while (thereAreLevelsNeedToBeProcessed()) {
				processCurrentLevel();
			}
		}

		private bool thereAreLevelsNeedToBeProcessed() {
			int i = getCurrentLevelIndex();
			int n = getLevelsNumber();
			return i < n;
		}

		private int getCurrentLevelIndex() {
			return currentLevelIndex;
		}

		private int getLevelsNumber() {
			var levels = getLevelsFromCurrentCampaign();
			return levels.Count;
		}

		private List<Level> getLevelsFromCurrentCampaign() {
			var currentCampaign = getCurrentCampaignFromRequester();
			return currentCampaign.levels;
		}

		private Campaign getCurrentCampaignFromRequester() {
			var campaigns = getCampaignsFromRequester();
			var index = getCurrentCampaignIndex();
			return campaigns[index];
		}

		private void processCurrentLevel() {
			processRecurringLevel();
			moveOnToTheNextLevel();
		}

		private void processRecurringLevel() {
			if (currentLevelTurnsOutToBeTheMostPlayedCampaignOne()) {
				setCurrentLevelAsTheMostPlayedCampaignOne();
			}
		}

		private bool currentLevelTurnsOutToBeTheMostPlayedCampaignOne() {
			var currentLevel = getCurrentLevelFromCurrentCampaign();
			var mostPlayedLevel = getMostPlayedCampaignLevel();
			return currentLevel.timesPlayed > mostPlayedLevel.timesPlayed;
		}

		private Level getCurrentLevelFromCurrentCampaign() {
			var levels = getLevelsFromCurrentCampaign();
			var index = getCurrentLevelIndex();
			return levels[index];
		}

		private Level getMostPlayedCampaignLevel() {
			return mostPlayedCampaignLevel;
		}

		private void setCurrentLevelAsTheMostPlayedCampaignOne() {
			var value = getCurrentLevelFromCurrentCampaign();
			setMostPlayedCampaignLevel(value);
		}

		private void moveOnToTheNextLevel() {
			const int STEP = 1;
			int index = getCurrentLevelIndex();
			int value = index + STEP;
			setCurrentLevelIndex(value);
		}

		private void moveOnToTheNextCampaign() {
			const int STEP = 1;
			int index = getCurrentCampaignIndex();
			int value = index + STEP;
			setCurrentCampaignIndex(value);
		}

		private Level getCalculationsResult() {
			return getMostPlayedCampaignLevel();
		}
	}
}