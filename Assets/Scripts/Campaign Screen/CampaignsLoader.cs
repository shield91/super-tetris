using UnityEngine;

namespace AssemblyCSharp
{
	public class CampaignsLoader : ButtonsLoader {
		protected override string getXpathForItems() {
			return "Campaigns/Campaign";
		}

		protected override string getKeyForModifiedDocument() {
			return "Modified Campaigns";
		}

		protected override string getKeyForOriginalDocument() {
			return "Campaigns";
		}

		protected override string getPathForCurrentItem() {
			if (itIsWarmUpButton()) {
				return getPathForWarmUpButton();
			} else {
				return getPathForCampaignButton();
			}
		}

		private bool itIsWarmUpButton() {
			const string VALUE = "Warm-Up";
			string name = getNameForCurrentItem();
			return name.Equals(VALUE);
		}

		protected override string getNameForCurrentItem() {
			const string NAME = "Name";
			return getValueFor(NAME);
		}

		private string getPathForWarmUpButton() {
			return "Warm-Up Button";
		}

		private string getPathForCampaignButton() {
			return "Campaign Button";
		}

		protected override void setUpSpecificParametersForCurrentItem() {
			if (itIsOrdinaryCampaignButton()) {
				setUpLabelForOrdinaryCampaignButton();
			}
		}

		private bool itIsOrdinaryCampaignButton() {
			return !itIsWarmUpButton();
		}

		private void setUpLabelForOrdinaryCampaignButton() {
			const string NAME = "Text";
			var button = getInstantiatedItem();
			var t = getTextComponentFor(NAME);
			var buttonName = button.name;
			t.text = buttonName.ToUpper();
		}

		protected override string getKeyForDocumentWithNewItem() {
			return "Modified Campaigns";
		}

		protected override string getNewItemName() {
			return "Campaign";
		}

		protected override string getNameForMainAttributeOfNewItem() {
			return "Name";
		}

		protected override string getValueForMainAttributeOfNewItem() {
			const string TAG = "Canvas";
			var canvas = GameObject.FindWithTag(TAG);
			var ncpuw = canvas.GetComponentInChildren<NewCampaignPopUpWindow>();
			return ncpuw.getTextFromInputField();
		}
	}
}