﻿using UnityEngine;
using UnityEngine.UI;
using System.Xml;

public class NewCampaignPopUpWindow : MonoBehaviour {
	public void showPopUpWindow() {
		const bool VALUE = true;
		gameObject.SetActive(VALUE);
	}

	public void addNewCampaign() {
		if (inputFieldIsFilled()) {
			processPopUpWindowClosure();
		}
	}

	private bool inputFieldIsFilled() {
		const string VALUE = "";
		var text = getTextFromInputField();
		return !text.Equals(VALUE);
	}

	public string getTextFromInputField() {
		var inputField = transform.GetComponentInChildren<InputField>();
		return inputField.text;
	}

	private void processPopUpWindowClosure() {
		addButtonForNewCampaign();
		hidePopUpWindow();
	}

	private void addButtonForNewCampaign() {
		var cs = GetComponentInParent<CampaignScreen>();
		cs.addButtonForNewCampaign();
	}

	private void hidePopUpWindow() {
		const bool VALUE = false;
		gameObject.SetActive(VALUE);
	}
}