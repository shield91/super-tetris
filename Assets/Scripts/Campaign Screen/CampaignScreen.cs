using UnityEngine;
using AssemblyCSharp;
using System.Xml;

public class CampaignScreen : MonoBehaviour {
	private void Start() {
		setUpThisScreen();
		deleteDataAboutChosenCampaign();
		loadCampaigns();
	}

	private void setUpThisScreen() {
		if (levelEditorModeIsEnabled()) {
			setUpThisScreenInLevelEditorMode();
		}
	}

	public bool levelEditorModeIsEnabled() {
		const string KEY = "Level Editor Mode Is Enabled";
		const string VALUE = "";
		string value = PlayerPrefs.GetString(KEY);
		return !value.Equals(VALUE);
	}

	private void setUpThisScreenInLevelEditorMode() {
		hideSoundButton();
		showAddNewCampaignButton();
	}

	private void hideSoundButton() {
		const string NAME = "Sound";
		const bool VALUE = false;
		var go = getGameObjectFor(NAME);
		go.SetActive(VALUE);
	}

	private GameObject getGameObjectFor(string name) {
		var child = transform.Find(name);
		return child.gameObject;
	}

	private void showAddNewCampaignButton() {
		const string NAME = "Add New Campaign";
		show(NAME);
	}

	private void show(string name) {
		const bool VALUE = true;
		var go = getGameObjectFor(name);
		go.SetActive(VALUE);
	}

	private void deleteDataAboutChosenCampaign() {
		deleteDataAboutOriginalChosenCampaign();
		deleteDataAboutModifiedChosenCampaign();
	}

	private void deleteDataAboutOriginalChosenCampaign() {
		const string NAME = "Chosen Campaign";
		deleteDataAbout(NAME);
	}

	private void deleteDataAbout(string name) {
		string key = name;
		PlayerPrefs.DeleteKey(key);
	}

	private void deleteDataAboutModifiedChosenCampaign() {
		const string NAME = "Modified Chosen Campaign";
		deleteDataAbout(NAME);
	}

	private void loadCampaigns() {
		ItemsLoader l = new CampaignsLoader();
		l.load();
	}

	public void goToHomeScreen() {
		disableLevelEditorModeIfItIsEnabled();
		loadHomeScreen();
	}

	private void disableLevelEditorModeIfItIsEnabled() {
		if (levelEditorModeIsEnabled()) {
			disableLevelEditorMode();
		}
	}

	private void disableLevelEditorMode() {
		const string KEY = "Level Editor Mode Is Enabled";
		PlayerPrefs.DeleteKey(KEY);
	}

	private void loadHomeScreen() {
		const string NAME = "Home Screen";
		Application.LoadLevel(NAME);
	}

	public XmlDocument getDocumentForCampaignsInLevelEditorMode() {
		var filename = getFileNameForCampaignsInLevelEditorMode();
		var document = new XmlDocument();
		document.Load(filename);
		return document;
	}

	private string getFileNameForCampaignsInLevelEditorMode() {
		const string KEY = "Path To Campaigns";
		const string SA = "\\Campaigns.xml";
		var path = PlayerPrefs.GetString(KEY);
		return path + SA;
	}

	public void addButtonForNewCampaign() {
		var l = new CampaignsLoader();
		l.addNewButton();
	}
}