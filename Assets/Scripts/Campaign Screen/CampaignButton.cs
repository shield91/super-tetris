﻿using UnityEngine;
using AssemblyCSharp;

public class CampaignButton : MonoBehaviour {
	public void goToLevelsScreenForChosenCampaign() {
		packDataForChosenCampaign();
		loadLevelsScreen();
	}

	private void packDataForChosenCampaign() {
		const string KEY = "Chosen Campaign";
		var engine = new CampaignsSearchEngine();
		var value = engine.get(name);
		PlayerPrefs.SetString(KEY, value);
	}
	
	private void loadLevelsScreen() {
		const string NAME = "Levels Screen";
		Application.LoadLevel(NAME);
	}
}