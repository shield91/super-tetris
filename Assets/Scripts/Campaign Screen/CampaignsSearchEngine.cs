namespace AssemblyCSharp
{
	public class CampaignsSearchEngine : ItemsSearchEngine {
		protected override string getXpathForItems() {
			return "Campaigns/Campaign";
		}

		protected override ButtonsLoader getItemsLoader() {
			return new CampaignsLoader();
		}

		protected override string getNameForCurrentItemMainAttribute() {
			return "Name";
		}
	}
}