﻿using UnityEngine;

public class LevelEditorBlock : LevelEditorItem {
	protected override void processOnMouseDragEvent() {
		var copyTransform = getItemCopyTransform();
		var position = getNewPositionForItemCopy();
		var unadjustedX = position.x;
		var unadjustedY = position.y;
		var x = (int) unadjustedX;
		var y = (int) unadjustedY;
		copyTransform.localPosition = new Vector2(x, y);
	}

	protected override void processOnMouseUpEvent() {
		if (cellIsOccupiedByAnotherBlock()) {
			destroyThisBlock();
		}
	}

	private bool cellIsOccupiedByAnotherBlock() {
		var items = getItems();
		var n = items.Length;
		return n > 1;
	}

	private void destroyThisBlock() {
		var obj = getItemCopy();
		Destroy(obj);
	}
}