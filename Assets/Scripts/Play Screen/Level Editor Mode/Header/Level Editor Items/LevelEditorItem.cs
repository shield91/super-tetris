﻿using UnityEngine;
using AssemblyCSharp;

public abstract class LevelEditorItem : MonoBehaviour {
	private GameObject itemCopy;
	
	private void Start() {
		resetItemCopy();
	}

	private void resetItemCopy() {
		const GameObject VALUE = null;
		setItemCopy(VALUE);
	}

	private void setItemCopy(GameObject value) {
		itemCopy = value;
	}
	
	private void OnMouseDown() {
		instantiateItemCopy();
		setUpItemCopyScale();
	}

	private void instantiateItemCopy() {
		const string NAME = "Campaign Brick";
		var original = gameObject;
		var panel = transform.parent;
		var playField = panel.parent;
		var parent = playField.Find(NAME);
		var value = Instantiate(original, parent);
		var obj = value.GetComponent<LevelEditorItem>();
		value.name = name;
		Destroy(obj);
		setItemCopy(value);
	}

	private void setUpItemCopyScale() {
		var copyTransform = getItemCopyTransform();
		copyTransform.localScale = Vector3.one;
	}
	
	protected Transform getItemCopyTransform() {
		var copy = getItemCopy();
		return copy.transform;
	}
	
	protected GameObject getItemCopy() {
		return itemCopy;
	}

	private void OnMouseDrag() {
		processOnMouseDragEvent();
	}

	protected abstract void processOnMouseDragEvent();

	protected Vector3 getNewPositionForItemCopy() {
		float x = getAdjustedPositionForItemCopyOnX();
		float y = getAdjustedPositionForItemCopyOnY();
		return new Vector2 (x, y);
	}

	private float getAdjustedPositionForItemCopyOnX() {
		var position = getUnadjustedPositionForItemCopy();
		var adjuster = new PositionAdjusterOnX(position);
		return adjuster.getAdjustedPositionOnAxis();
	}

	private Vector2 getUnadjustedPositionForItemCopy() {
		var copyTransform = getItemCopyTransform();
		var playField = copyTransform.parent;
		var position = getInputWorldPosition();
		return playField.InverseTransformPoint(position);
	}

	private Vector2 getInputWorldPosition() {
		var position = Input.mousePosition;
		var mainCamera = Camera.main;
		return mainCamera.ScreenToWorldPoint(position);
	}

	private float getAdjustedPositionForItemCopyOnY() {
		var position = getUnadjustedPositionForItemCopy();
		var adjuster = new PositionAdjusterOnY(position);
		return adjuster.getAdjustedPositionOnAxis();
	}

	private void OnMouseUp() {
		processOnMouseUpEvent();
	}

	protected abstract void processOnMouseUpEvent();

	protected Collider2D[] getItems() {
		var copy = getItemCopy();
		var boxCollider = copy.GetComponent<BoxCollider2D>();
		var bounds = boxCollider.bounds;
		var pointA = bounds.min;
		var pointB = bounds.max;
		return Physics2D.OverlapAreaAll(pointA, pointB);
	}
}