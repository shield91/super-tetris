﻿using UnityEngine;

public class LevelEditorDeleteItem : LevelEditorBlock {
	private Collider2D[] elements;
	private int currentItemIndex;

	private void Start() {
		resetElements();
		resetCurrentItemIndex();
	}

	private void resetElements() {
		const Collider2D[] VALUE = null;
		setElements(VALUE);
	}
	
	private void setElements(Collider2D[] value) {
		elements = value;
	}

	private void resetCurrentItemIndex() {
		const int VALUE = 0;
		setCurrentItemIndex(VALUE);
	}

	private void setCurrentItemIndex(int value) {
		currentItemIndex = value;
	}

	protected override void processOnMouseDragEvent() {
		var copyTransform = getItemCopyTransform();
		copyTransform.localPosition = getNewPositionForItemCopy();
	}

	protected override void processOnMouseUpEvent() {
		resetItems();
		resetCurrentItemIndex();
		deleteItems();
	}

	private void resetItems() {
		Collider2D[] value = getItems();
		setElements(value);
	}

	private void deleteItems() {
		while (thereAreItemsNeedToBeDeleted()) {
			deleteCurrentItem();
		}
	}

	private bool thereAreItemsNeedToBeDeleted() {
		var items = getElements();
		var i = getCurrentItemIndex();
		var n = items.Length;
		return i < n;
	}

	private Collider2D[] getElements() {
		return elements;
	}

	private int getCurrentItemIndex() {
		return currentItemIndex;
	}

	private void deleteCurrentItem() {
		deleteItem();
		moveOnToTheNextItem();
	}

	private void deleteItem() {
		var items = getElements();
		var index = getCurrentItemIndex();
		var currentItem = items[index];
		var obj = currentItem.gameObject;
		Destroy(obj);
	}

	private void moveOnToTheNextItem() {
		int index = getCurrentItemIndex();
		int value = index + 1;
		setCurrentItemIndex(value);
	}
}