using UnityEngine;

namespace AssemblyCSharp
{
	public abstract class PositionAdjuster {
		private Vector2 unadjustedPosition;

		public PositionAdjuster(Vector2 position) {
			Vector2 value = position;
			setUnadjustedPosition(value);
		}

		private void setUnadjustedPosition(Vector2 value) {
			unadjustedPosition = value;
		}

		public float getAdjustedPositionOnAxis() {
			float a = getLowerBoundOnAxis();
			float b = getUpperBoundOnAxis();
			return Mathf.Min(a, b);
		}

		private float getLowerBoundOnAxis() {
			const float A = 0;
			float b = getUnadjustedPositionOnAxis();
			return Mathf.Max(A, b);
		}

		protected abstract float getUnadjustedPositionOnAxis();

		protected Vector2 getUnadjustedPosition() {
			return unadjustedPosition;
		}

		protected abstract float getUpperBoundOnAxis();
	}
}