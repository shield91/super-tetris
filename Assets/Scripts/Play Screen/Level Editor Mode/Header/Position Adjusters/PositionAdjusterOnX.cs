using UnityEngine;

namespace AssemblyCSharp
{
	public class PositionAdjusterOnX : PositionAdjuster	{
		public PositionAdjusterOnX(Vector2 position) : base(position) {}

		protected override float getUnadjustedPositionOnAxis() {
			var position = getUnadjustedPosition();
			return position.x;
		}

		protected override float getUpperBoundOnAxis() {
			return 9f;
		}
	}
}