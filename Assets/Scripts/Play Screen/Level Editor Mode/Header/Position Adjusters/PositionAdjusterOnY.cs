using UnityEngine;

namespace AssemblyCSharp
{
	public class PositionAdjusterOnY : PositionAdjuster	{
		public PositionAdjusterOnY(Vector2 position) : base(position) {}

		protected override float getUnadjustedPositionOnAxis() {
			var position = getUnadjustedPosition();
			return position.y;
		}

		protected override float getUpperBoundOnAxis() {
			return 25f;
		}
	}
}