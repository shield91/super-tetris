﻿using UnityEngine;

public class PreviousPageButton : CampaignLevelScreenButton {
	protected override void processClick () {
		const string NAME = "Levels Screen";
		Application.LoadLevel(NAME);
	}
}