﻿using UnityEngine;

public abstract class CampaignLevelScreenButton : MonoBehaviour {
	public Sprite pressedButton;
	public Sprite releasedButton;

	private void OnMouseDown() {
		highlightButton();
	}

	private void highlightButton() {
		var renderer = GetComponent<SpriteRenderer>();
		renderer.sprite = getPressedButton();
	}

	private Sprite getPressedButton() {
		return pressedButton;
	}

	private void OnMouseUp() {
		unhighlightButton();
		processClick();
	}

	private void unhighlightButton() {
		var renderer = GetComponent<SpriteRenderer>();
		renderer.sprite = getReleasedButton();
	}

	private Sprite getReleasedButton() {
		return releasedButton;
	}

	protected abstract void processClick();
}