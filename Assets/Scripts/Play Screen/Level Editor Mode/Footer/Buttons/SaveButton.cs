﻿using UnityEngine;
using AssemblyCSharp;
using System.Xml;

public class SaveButton : CampaignLevelScreenButton {
	protected override void processClick() {
		disableButton();
		removeOldDataAboutPlayFields();
		addLeftPlayFieldToLevelXml();
		addRightPlayFieldToLevelXml();
		updateCampaigns();
		updateModifiedChosenCampaign();
		updateOriginalChosenLevel();
		enableButton();
	}

	private void disableButton() {
		const bool VALUE = false;
		name = "Saving...";
		gameObject.SetActive(VALUE);
	}

	private void removeOldDataAboutPlayFields() {
		var originalChosenLevel = getChosenLevel();
		var documentElement = originalChosenLevel.DocumentElement;
		documentElement.IsEmpty = true;
		update(originalChosenLevel);
	}

	public XmlDocument getChosenLevel() {
		const string KEY = "Modified Chosen Level";
		var xml = PlayerPrefs.GetString(KEY);
		var document = new XmlDocument();
		document.LoadXml(xml);
		return document;
	}

	public void update(XmlDocument originalChosenLevel) {
		const string KEY = "Modified Chosen Level";
		string value = originalChosenLevel.OuterXml;
		PlayerPrefs.SetString(KEY, value);
	}

	private void addLeftPlayFieldToLevelXml() {
		const string NAME = "Left Play Field";
		add(NAME);
	}

	private void add(string name) {
		var suppliant = this;
		var adder = new PlayFieldAdder(suppliant);
		adder.add(name);
	}

	private void addRightPlayFieldToLevelXml() {
		const string NAME = "Right Play Field";
		add(NAME);
	}

	private void updateCampaigns() {
		const string KEY = "Path To Campaigns";
		const string S = "\\Campaigns.xml";
		var path = PlayerPrefs.GetString(KEY);
		var filename = path + S;
		var document = new XmlDocument();
		var xml = getXmlForUpdatedCampaigns();
		document.LoadXml(xml);
		document.Save(filename);
	}

	private string getXmlForUpdatedCampaigns() {
		var campaigns = getCampaigns();
		var oldValue = getChosenCampaign();
		var newValue = getUpdatedModifiedChosenCampaign();
		return campaigns.Replace(oldValue, newValue);
	}

	private string getCampaigns() {
		const string KEY = "Modified Campaigns";
		string defaultValue = getOriginalCampaigns();
		return PlayerPrefs.GetString(KEY, defaultValue);
	}

	private string getOriginalCampaigns() {
		const string NAME = "Campaigns";
		return get(NAME);
	}

	private string get(string name) {
		string key = name;
		return PlayerPrefs.GetString(key);
	}

	private string getChosenCampaign() {
		const string NAME = "Chosen Campaign";
		return get(NAME);
	}

	private string getUpdatedModifiedChosenCampaign() {
		string modifiedChosenCampaign = getModifiedChosenCampaign();
		string oldValue = getOriginalChosenLevel();
		string newValue = getModifiedChosenLevel();
		return modifiedChosenCampaign.Replace(oldValue, newValue);
	}

	private string getModifiedChosenCampaign() {
		const string KEY = "Modified Chosen Campaign";
		string defaultValue = getChosenCampaign();
		return PlayerPrefs.GetString(KEY, defaultValue);
	}

	private string getOriginalChosenLevel() {
		const string NAME = "Chosen Level";
		return get(NAME);
	}

	private string getModifiedChosenLevel() {
		var document = getChosenLevel();
		return document.OuterXml;
	}

	private void updateModifiedChosenCampaign() {
		const string KEY = "Modified Chosen Campaign";
		string value = getUpdatedModifiedChosenCampaign();
		PlayerPrefs.SetString(KEY, value);
	}

	private void updateOriginalChosenLevel() {
		const string KEY = "Chosen Level";
		string value = getModifiedChosenLevel();
		PlayerPrefs.SetString(KEY, value);
	}

	private void enableButton() {
		const bool VALUE = true;
		name = "Save";
		gameObject.SetActive(VALUE);
	}
}