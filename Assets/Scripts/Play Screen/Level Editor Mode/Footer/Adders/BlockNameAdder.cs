namespace AssemblyCSharp
{
	public class BlockNameAdder : BlockAttributeAdder {
		public BlockNameAdder(PlayFieldBlocksAdder suppliant) : base(suppliant) {}

		protected override string getAttributeName() {
			return "Name";
		}

		protected override string getAttributeValue() {
			var blockTransform = getBlockTransform();
			return blockTransform.name;
		}
	}
}