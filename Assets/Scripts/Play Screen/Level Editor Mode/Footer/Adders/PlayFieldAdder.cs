using UnityEngine;
using System.Xml;

namespace AssemblyCSharp
{
	public class PlayFieldAdder {
		private SaveButton requester;
		private string playFieldName;

		public PlayFieldAdder(SaveButton suppliant) {
			setRequester(suppliant);
			resetPlayFieldName();
		}

		private void setRequester(SaveButton value) {
			requester = value;
		}

		private void resetPlayFieldName() {
			const string VALUE = "";
			setPlayFieldName(VALUE);
		}

		private void setPlayFieldName(string value) {
			playFieldName = value;
		}

		public void add(string playFieldName) {
			setPlayFieldName(playFieldName);
			addPlayFieldToLevelXml();
			setNameForAddedPlayField();
			addBlocksToAddedPlayField();
		}

		private void addPlayFieldToLevelXml() {
			const string NAME = "PlayField";
			var suppliant = getRequester();
			var originalChosenLevel = getOriginalChosenLevelFromRequester();
			var documentElement = originalChosenLevel.DocumentElement;
			var newChild = originalChosenLevel.CreateElement(NAME);
			documentElement.AppendChild(newChild);
			suppliant.update(originalChosenLevel);
		}

		public SaveButton getRequester() {
			return requester;
		}

		private XmlDocument getOriginalChosenLevelFromRequester() {
			var suppliant = getRequester();
			return suppliant.getChosenLevel();
		}

		private void setNameForAddedPlayField() {
			const string NAME = "Name";
			var suppliant = getRequester();
			var originalChosenLevel = getOriginalChosenLevelFromRequester();
			var documentElement = originalChosenLevel.DocumentElement;
			var playField = documentElement.LastChild;
			var node = originalChosenLevel.CreateAttribute(NAME);
			var attributes = playField.Attributes;
			node.Value = getPlayFieldName();
			attributes.Append(node);
			suppliant.update(originalChosenLevel);
		}

		public string getPlayFieldName() {
			return playFieldName;
		}

		private void addBlocksToAddedPlayField() {
			var suppliant = this;
			var adder = new PlayFieldBlocksAdder(suppliant);
			adder.addBlocksToPlayField();
		}
	}
}