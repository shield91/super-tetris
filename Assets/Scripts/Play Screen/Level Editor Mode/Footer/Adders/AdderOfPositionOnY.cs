namespace AssemblyCSharp
{
	public class AdderOfPositionOnY : BlockPositionAdder {
		public AdderOfPositionOnY(PlayFieldBlocksAdder suppliant) : base(suppliant)	{}
		
		protected override string getAttributeName() {
			return "Y";
		}
		
		protected override float getBlockPositionOnAxis() {
			var position = getBlockPosition();
			return position.y;
		}
	}
}