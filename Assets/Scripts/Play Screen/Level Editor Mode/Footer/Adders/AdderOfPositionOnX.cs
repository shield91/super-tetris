namespace AssemblyCSharp
{
	public class AdderOfPositionOnX : BlockPositionAdder {
		public AdderOfPositionOnX(PlayFieldBlocksAdder suppliant) : base(suppliant)	{}

		protected override string getAttributeName() {
			return "X";
		}

		protected override float getBlockPositionOnAxis() {
			var position = getBlockPosition();
			return position.x;
		}
	}
}