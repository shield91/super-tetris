using UnityEngine;
using System.Xml;

namespace AssemblyCSharp {
	public abstract class BlockAttributeAdder {
		private PlayFieldBlocksAdder requester;

		public BlockAttributeAdder(PlayFieldBlocksAdder suppliant) {
			var value = suppliant;
			setRequester(value);
		}

		private void setRequester(PlayFieldBlocksAdder value) {
			requester = value;
		}

		public void addDataAboutAttribute() {
			var suppliant = getRequester();
			var saveButton = suppliant.getSaveButtonFromRequester();
			var originalChosenLevel = suppliant.getOriginalChosenLevelFromRequester();
			var documentElement = originalChosenLevel.DocumentElement;
			var playField = documentElement.LastChild;
			var block = playField.LastChild;
			var attributes = block.Attributes;
			var name = getAttributeName();
			var node = originalChosenLevel.CreateAttribute(name);
			node.Value = getAttributeValue();
			attributes.Append(node);
			saveButton.update(originalChosenLevel);
		}

		protected PlayFieldBlocksAdder getRequester() {
			return requester;
		}

		protected abstract string getAttributeName();

		protected abstract string getAttributeValue();

		protected Transform getBlockTransform() {
			var suppliant = getRequester();
			return suppliant.getCurrentBlockTransform();
		}
	}
}