using UnityEngine;

namespace AssemblyCSharp
{
	public abstract class BlockPositionAdder : BlockAttributeAdder	{
		public BlockPositionAdder(PlayFieldBlocksAdder suppliant) : base(suppliant) {}

		protected override string getAttributeValue() {
			float axisPosition = getBlockPositionOnAxis();
			return axisPosition.ToString();
		}

		protected abstract float getBlockPositionOnAxis();

		protected Vector2 getBlockPosition() {
			var blockTransform = getBlockTransform();
			return blockTransform.localPosition;
		}
	}
}