using UnityEngine;
using System.Xml;

namespace AssemblyCSharp
{
	public class PlayFieldBlocksAdder {
		private PlayFieldAdder requester;
		private int currentBlockIndex;

		public PlayFieldBlocksAdder(PlayFieldAdder suppliant) {
			setRequester(suppliant);
			resetCurrentBlockIndex();
		}

		private void setRequester(PlayFieldAdder value) {
			requester = value;
		}

		private void resetCurrentBlockIndex() {
			const int VALUE = 0;
			setCurrentBlockIndex(VALUE);
		}

		private void setCurrentBlockIndex(int value) {
			currentBlockIndex = value;
		}

		public void addBlocksToPlayField() {
			resetCurrentBlockIndex();
			addMinosToPlayField();
		}

		private void addMinosToPlayField() {
			while (thereAreBlocksNeedToBeAdded()) {
				addCurrentBlockToPlayField();
			}
		}

		private bool thereAreBlocksNeedToBeAdded() {
			var campaignBrick = getCampaignBrickForPlayField();
			var i = getCurrentBlockIndex();
			var n = campaignBrick.childCount;
			return i < n;
		}

		private Transform getCampaignBrickForPlayField() {
			const string SA = "Play Fields/";
			const string SB = "/Play Field/Campaign Brick";
			var nameOfPlayField = getPlayFieldName();
			var name = SA + nameOfPlayField + SB;
			var gameWindow = getGameWindowFromRequester();
			return gameWindow.Find(name);
		}

		private string getPlayFieldName() {
			var suppliant = getRequester();
			return suppliant.getPlayFieldName();
		}
		
		public PlayFieldAdder getRequester() {
			return requester;
		}

		private Transform getGameWindowFromRequester() {
			var saveButton = getSaveButtonFromRequester();
			var playScreen = saveButton.GetComponentInParent<PlayScreen>();
			return playScreen.transform;
		}

		public SaveButton getSaveButtonFromRequester() {
			var suppliant = getRequester();
			return suppliant.getRequester();
		}

		private int getCurrentBlockIndex() {
			return currentBlockIndex;
		}

		private void addCurrentBlockToPlayField() {
			addCurrentMinoToPlayField();
			addDataToCurrentBlock();
			moveOnTheNextBlock();
		}

		private void addCurrentMinoToPlayField() {
			const string NAME = "Block";
			var saveButton = getSaveButtonFromRequester();
			var originalChosenLevel = getOriginalChosenLevelFromRequester();
			var documentElement = originalChosenLevel.DocumentElement;
			var playField = documentElement.LastChild;
			var newChild = originalChosenLevel.CreateElement(NAME);
			playField.AppendChild(newChild);
			saveButton.update(originalChosenLevel);
		}

		public XmlDocument getOriginalChosenLevelFromRequester() {
			var saveButton = getSaveButtonFromRequester();
			return saveButton.getChosenLevel();
		}

		private void addDataToCurrentBlock() {
			addDataAboutBlockName();
			addDataAboutBlockPosition();
		}

		private void addDataAboutBlockName() {
			var suppliant = this;
			var adder = new BlockNameAdder(suppliant);
			adder.addDataAboutAttribute();
		}

		public Transform getCurrentBlockTransform() {
			var campaignBrick = getCampaignBrickForPlayField();
			var index = getCurrentBlockIndex();
			return campaignBrick.GetChild(index);
		}

		private void addDataAboutBlockPosition() {
			addDataAboutPositionOnX();
			addDataAboutPositionOnY();
		}

		private void addDataAboutPositionOnX() {
			var suppliant = this;
			var adder = new AdderOfPositionOnX(suppliant);
			adder.addDataAboutAttribute();
		}

		private void addDataAboutPositionOnY() {
			var suppliant = this;
			var adder = new AdderOfPositionOnY(suppliant);
			adder.addDataAboutAttribute();
		}

		private void moveOnTheNextBlock() {
			int index = getCurrentBlockIndex();
			int value = index + 1;
			setCurrentBlockIndex(value);
		}
	}
}