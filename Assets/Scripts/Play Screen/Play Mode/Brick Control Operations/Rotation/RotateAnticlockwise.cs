using UnityEngine;

namespace AssemblyCSharp {
	public class RotateAnticlockwise : Rotate {
		public RotateAnticlockwise(Transform activeBrick) : base(activeBrick) {}

		protected override void employOperation() {
			var brick = getBrickScriptFromFallingBrick();
			brick.rotateAnticlockwise();
		}

		protected override void revertOperation() {
			var brick = getBrickScriptFromFallingBrick();
			brick.rotateClockwise();
		}
	}
}