using UnityEngine;

namespace AssemblyCSharp
{
	public abstract class Rotate: BrickControlOperation {
		private bool brickShouldMoveToTheRight;
		private int brickAdjustmentMovementsQuantity;

		public Rotate(Transform activeBrick) : base(activeBrick) {
			resetDataAboutBrickDefinedDirection();
			resetBrickAdjustmentMovementsQuantity();
		}

		private void resetDataAboutBrickDefinedDirection() {
			const bool VALUE = false;
			setBrickMovementToTheRight(VALUE);
		}

		private void setBrickMovementToTheRight(bool value) {
			brickShouldMoveToTheRight = value;
		}

		private void resetBrickAdjustmentMovementsQuantity() {
			const int VALUE = 0;
			setBrickAdjustmentMovementsQuantity(VALUE);
		}

		private void setBrickAdjustmentMovementsQuantity(int value) {
			brickAdjustmentMovementsQuantity = value;
		}
		
		protected override void applyOperation() {
			resetBrickAdjustmentMovementsQuantity();
			employOperation();
		}

		protected abstract void employOperation();

		protected override void processResult() {
			if (brickNewPositionIsValid()) {
				processBrickNewPosition();
			} else {
				tryToAdjustRotationBehaviour();
			}
		}

		private void tryToAdjustRotationBehaviour() {
			if(brickIsVerticallyInsidePlayField()) {
				goToTheSecondPhaseOfRotationBehaviourAdjustment();
			} else {
				revertOperation();
			}
		}

		private bool brickIsVerticallyInsidePlayField() {
			var activeBrick = getFallingBrick();
			var validator = new BrickPositionValidator(activeBrick);
			return validator.brickIsVerticallyInsidePlayField();
		}

		private void goToTheSecondPhaseOfRotationBehaviourAdjustment() {
			defineBrickRotationBehaviourAdjustmentDirection();
			moveBrickInsidePlayField();
			processResultOfBrickMovementInsidePlayField();
		}

		private void defineBrickRotationBehaviourAdjustmentDirection() {
			if (brickIsAtLeastPartiallyOutsidePlayFieldFromTheLeft()) {
				defineThatBrickShouldMoveToTheRight();
			}
		}

		private bool brickIsAtLeastPartiallyOutsidePlayFieldFromTheLeft() {
			var activeBrick = getFallingBrick();
			var validator = new BrickPositionValidator(activeBrick);
			return !validator.brickIsAtMostNearTheLeftWall();
		}

		private void defineThatBrickShouldMoveToTheRight() {
			const bool VALUE = true;
			setBrickMovementToTheRight(VALUE);
		}

		private void moveBrickInsidePlayField() {
			while (brickIsAtLeastPartiallyHorizontallyOutsidePlayField()) {
				moveBrickInsidePlayFieldInDefinedDirection();
			}
		}

		private bool brickIsAtLeastPartiallyHorizontallyOutsidePlayField() {
			var activeBrick = getFallingBrick();
			var validator = new BrickPositionValidator(activeBrick);
			return !validator.brickIsHorizontallyInsidePlayField();
		}

		private void moveBrickInsidePlayFieldInDefinedDirection() {
			moveBrickInsidePlayFieldInChosenDirection();
			increaseBrickAdjustmentMovementsQuantity();
		}

		private void moveBrickInsidePlayFieldInChosenDirection() {
			if (tetrominoShouldMoveToTheRight()) {
				moveBrickToTheRight();
			} else {
				moveBrickToTheLeft();
			}
		}

		private bool tetrominoShouldMoveToTheRight() {
			return brickShouldMoveToTheRight;
		}

		private void moveBrickToTheRight() {
			var brick = getFallingBrick();
			brick.localPosition += Vector3.right;
		}

		private void moveBrickToTheLeft() {
			var brick = getFallingBrick();
			brick.localPosition += Vector3.left;
		}

		private void increaseBrickAdjustmentMovementsQuantity() {
			int i = getBrickAdjustmentMovementsQuantity();
			int value = i + 1;
			setBrickAdjustmentMovementsQuantity(value);
		}

		private int getBrickAdjustmentMovementsQuantity() {
			return brickAdjustmentMovementsQuantity;
		}

		private void processResultOfBrickMovementInsidePlayField() {
			if (fallingBrickCollidesWithOtherBricks()) {
				revertRotationCompletely();
			} else {
				processBrickNewPosition();
			}
		}

		private bool fallingBrickCollidesWithOtherBricks() {
			var activeBrick = getFallingBrick();
			var validator = new BrickPositionValidator(activeBrick);
			return !validator.clashBetweenThisAndOtherBricksIsAbsent();
		}

		private void revertRotationCompletely() {
			revertBrickMovementInsidePlayField();
			revertRotation();
		}

		private void revertBrickMovementInsidePlayField() {
			while (needInRevertingBrickAdjustmentMovementsIsStillPresent()) {
				moveBrickInTheDirectionThatIsOppositeToTheDefinedOne();
			}
		}

		private bool needInRevertingBrickAdjustmentMovementsIsStillPresent() {
			int i = getBrickAdjustmentMovementsQuantity();
			return i > 0;
		}

		private void moveBrickInTheDirectionThatIsOppositeToTheDefinedOne() {
			moveBrickInTheDirectionThatIsOppositeToTheChosenOne();
			decreaseBrickAdjustmentMovementsQuantity();
		}

		private void moveBrickInTheDirectionThatIsOppositeToTheChosenOne() {
			if (brickHasBeenMovedToTheRight()) {
				moveBrickToTheLeft();
			} else {
				moveBrickToTheRight();
			}
		}

		private bool brickHasBeenMovedToTheRight() {
			return tetrominoShouldMoveToTheRight();
		}

		private void decreaseBrickAdjustmentMovementsQuantity() {
			int i = getBrickAdjustmentMovementsQuantity();
			int value = i - 1;
			setBrickAdjustmentMovementsQuantity(value);
		}

		private void revertRotation() {
			revertOperation();
		}
	}
}