using UnityEngine;

namespace AssemblyCSharp {
	public class RotateClockwise : RotateAnticlockwise {
		public RotateClockwise(Transform activeBrick) : base(activeBrick) {}

		protected override void employOperation() {
			base.revertOperation();
		}

		protected override void revertOperation() {
			base.employOperation();
		}
	}
}