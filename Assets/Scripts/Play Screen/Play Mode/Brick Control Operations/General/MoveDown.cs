using UnityEngine;
using System.Collections;

namespace AssemblyCSharp
{
	public class MoveDown : BrickControlOperation {
		public MoveDown(Transform activeBrick) : base(activeBrick) {}
		
		protected override void applyOperation() {
			modifyBrickPosition();
			setNewTimeSinceLastFallTick();
		}

		private void modifyBrickPosition() {
			var brick = getFallingBrick();
			brick.localPosition += Vector3.down;
		}

		private void setNewTimeSinceLastFallTick() {
			var value = Time.time;
			var brick = getBrickScriptFromFallingBrick();
			brick.setTimeSinceLastFallTick(value);
		}

		protected override void processBrickNewPosition() {
			goThroughPositionProcessingFirstPhase();
			goThroughPositionProcessingSecondPhase();
		}

		private void goThroughPositionProcessingFirstPhase() {
			base.processBrickNewPosition();
		}

		private void goThroughPositionProcessingSecondPhase() {
			if (brickIsMotionless()) {
				tellBrickToStartItsMotionAgain();
			}
		}

		private void tellBrickToStartItsMotionAgain() {
			tellBrickToContinueFall();
		}

		protected override void revertOperation() {
			goThroughOperationRevertingFirstPhase();
			goThroughOperationRevertingSecondPhase();
		}

		private void goThroughOperationRevertingFirstPhase() {
			var brick = getFallingBrick();
			brick.localPosition += Vector3.up;
		}

		private void goThroughOperationRevertingSecondPhase() {
			if (brickIsInMotion()) {
				tellBrickToStopAgain();
			}
		}

		private bool brickIsInMotion() {
			return !brickIsMotionless();
		}

		private void tellBrickToStopAgain() {
			markThatBrickIsNowMotionlessAgain();
			processBrickMotionStop();
		}

		private void markThatBrickIsNowMotionlessAgain() {
			const bool VALUE = true;
			var brick = getBrickScriptFromFallingBrick();
			brick.setMotion(VALUE);
		}

		private void processBrickMotionStop() {
			if (fallingBrickIsTheRealOne()) {
				processRealFallingBrickMotionStop();
			}
		}

		private bool fallingBrickIsTheRealOne() {
			const string VALUE = "Falling Brick";
			var brick = getFallingBrick();
			var brickName = brick.name;
			return brickName.Equals(VALUE);
		}

		private void processRealFallingBrickMotionStop() {
			var routine = processBrickLanding();
			var brick = getBrickScriptFromFallingBrick();
			brick.StartCoroutine(routine);
		}

		private IEnumerator processBrickLanding() {
			yield return waitForOneSecond();
			goToNextPhaseOfProcessingBrickLanding();
		}
		
		private WaitForSeconds waitForOneSecond() {
			const float SECONDS = 1f;
			return new WaitForSeconds(SECONDS);
		}

		private void goToNextPhaseOfProcessingBrickLanding() {
			fastenBrickPosition();
			processRows();
			destroySilhouette();
			launchNextBrick();
			untagBrick();
		}

		private void fastenBrickPosition() {
			goThroughPositionProcessingFirstPhase();
		}

		private void processRows() {
			var requester = getFallingBrick();
			var processor = new RowsProcessor(requester);
			processor.process();
		}

		private void destroySilhouette() {
			var brick = getFallingBrick();
			var playField = brick.GetComponentInParent<PlayField>();
			var silhouette = playField.getSilhouette();
			var obj = silhouette.gameObject;
			silhouette.name = "Disappearing Silhouette";
			GameObject.Destroy(obj);
		}

		private void launchNextBrick() {
			var brick = getFallingBrick();
			var playField = brick.parent;
			var spawner = playField.GetComponentInChildren<Spawner>();
			spawner.spawnBrick();
		}

		private void untagBrick() {
			const string NAME = "Blocks";
			var brick = getFallingBrick();
			brick.name = NAME;
		}

		protected override void checkTheFallingBrickAbilityToContinueFall() {
		}
	}
}