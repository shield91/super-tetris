using UnityEngine;

namespace AssemblyCSharp
{
	public abstract class BrickControlOperation {
		private Transform fallingBrick;

		public BrickControlOperation(Transform activeBrick) {
			var value = activeBrick;
			setFallingBrick(value);
		}

		private void setFallingBrick(Transform value) {
			fallingBrick = value;
		}

		public void apply() {
			applyOperation();
			processResult();
			checkTheFallingBrickAbilityToContinueFall();
		}

		protected abstract void applyOperation();

		protected Brick getBrickScriptFromFallingBrick() {
			var activeBrick = getFallingBrick();
			return activeBrick.GetComponent<Brick>();
		}

		protected Transform getFallingBrick() {
			return fallingBrick;
		}

		protected virtual void processResult() {
			if (brickNewPositionIsValid()) {
				processBrickNewPosition();
			} else {
				revertOperation();
			}
		}

		protected bool brickNewPositionIsValid() {
			var activeBrick = getFallingBrick();
			var validator = new BrickPositionValidator(activeBrick);
			return validator.brickPositionIsValid();
		}

		protected virtual void processBrickNewPosition() {
			updateGrid();
		}
		
		private void updateGrid() {
			var minosset = getFallingBrick();
			var updater = new GridUpdater(minosset);
			updater.updateGrid();
		}

		protected abstract void revertOperation();

		protected virtual void checkTheFallingBrickAbilityToContinueFall() {
			var activeBrick = getFallingBrick();
			var checker = new BrickAbilityToFallChecker(activeBrick);
			checker.checkBrickAbilityToContinueFall();
		}

		protected bool brickIsMotionless() {
			var brick = getBrickScriptFromFallingBrick();
			return brick.isMotionless();
		}

		protected void tellBrickToContinueFall() {
			allowBrickToContinueFall();
			stopProcessingBrickLanding();
		}
		
		private void allowBrickToContinueFall() {
			const bool VALUE = false;
			var brick = getBrickScriptFromFallingBrick();
			brick.setMotion(VALUE);
		}
		
		private void stopProcessingBrickLanding() {
			var brick = getBrickScriptFromFallingBrick();
			brick.StopAllCoroutines();
		}
	}
}