using UnityEngine;

namespace AssemblyCSharp
{
	public class MoveLeft : BrickControlOperation {
		public MoveLeft(Transform activeBrick) : base(activeBrick) {}

		protected override void applyOperation() {
			var brick = getFallingBrick();
			brick.localPosition += Vector3.left;
		}

		protected override void revertOperation() {
			var brick = getFallingBrick();
			brick.localPosition += Vector3.right;
		}
	}
}