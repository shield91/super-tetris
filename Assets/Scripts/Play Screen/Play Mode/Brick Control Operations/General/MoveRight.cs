using UnityEngine;

namespace AssemblyCSharp
{
	public class MoveRight : BrickControlOperation {
		public MoveRight(Transform activeBrick) : base(activeBrick) {}
		
		protected override void applyOperation() {
			var brick = getFallingBrick();
			brick.localPosition += Vector3.right;
		}
		
		protected override void revertOperation() {
			var brick = getFallingBrick();
			brick.localPosition += Vector3.left;
		}
	}
}