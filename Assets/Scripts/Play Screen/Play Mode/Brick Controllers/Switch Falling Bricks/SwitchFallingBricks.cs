﻿using UnityEngine;
using AssemblyCSharp;

public class SwitchFallingBricks : MonoBehaviour {
	public Sprite switchIsOn;
	public Sprite switchIsOff;

	private void OnMouseDown() {
		toggleButton();
		swapFallingBricks();
	}

	private void toggleButton() {
		if (switchHasBeenTurnedOn()) {
			turnOffTheSwitch();
		} else {
			turnOnTheSwitch();
		}
	}

	private bool switchHasBeenTurnedOn() {
		var sr = GetComponent<SpriteRenderer>();
		return sr.sprite == switchIsOn;
	}

	private void turnOffTheSwitch() {
		var sr = GetComponent<SpriteRenderer>();
		sr.sprite = switchIsOff;
	}

	private void turnOnTheSwitch() {
		var sr = GetComponent<SpriteRenderer>();
		sr.sprite = switchIsOn;
	}

	private void swapFallingBricks() {
		swapFallingBricksPositions();
		adjustBricksSilhouettesPositions();
		adjustBricksPositions();
		checkBricksAbilityToContinueFall();
	}

	private void swapFallingBricksPositions() {
		var suppliant = this;
		var swapper = new BricksPositionsSwapper(suppliant);
		swapper.swapBricksPositions();
	}

	public Transform getLeftFallingBrick() {
		const string PLAY_FIELD_NAME = "Left Play Field";
		return getFallingBrickFrom(PLAY_FIELD_NAME);
	}

	private Transform getFallingBrickFrom(string playFieldName) {
		const string HEADER = "Play Fields/";
		var playScreen = GetComponentInParent<PlayScreen>();
		var gameWindow = playScreen.transform;
		var name = HEADER + playFieldName;
		var playField = gameWindow.Find(name);
		var pf = playField.GetComponentInChildren<PlayField>();
		return pf.getFallingBrick();
	}

	public Transform getRightFallingBrick() {
		const string NAME = "Right Play Field";
		return getFallingBrickFrom(NAME);
	}

	private void adjustBricksSilhouettesPositions() {
		adjustLeftFallingBrickSilhouettePosition();
		adjustRightFallingBrickSilhouettePosition();
	}

	private void adjustLeftFallingBrickSilhouettePosition() {
		adjustPositionOfLeftFallingBrickSilhouette();
		removeLeftFallingBrickSilhouetteFromGridToAvoidCollisionWithLeftFallingBrick();
	}

	private void adjustPositionOfLeftFallingBrickSilhouette() {
		var fallingBrick = getLeftFallingBrickSilhouette();
		adjustPositionOf(fallingBrick);
	}

	private Transform getLeftFallingBrickSilhouette() {
		var brick = getLeftFallingBrick();
		return getSilhouetteOf(brick);
	}

	private Transform getSilhouetteOf(Transform brick) {
		const string NAME = "Silhouette";
		var playField = brick.parent;
		return playField.Find(NAME);
	}

	private void adjustPositionOf(Transform fallingBrick) {
		var adjuster = new BrickPositionAdjuster(fallingBrick);
		adjuster.adjustBrickPosition();
	}

	private void removeLeftFallingBrickSilhouetteFromGridToAvoidCollisionWithLeftFallingBrick() {
		var minosset = getLeftFallingBrickSilhouette();
		var updater = new GridUpdater(minosset);
		updater.removeBrickFromOldPositionInGrid();
	}

	private void adjustRightFallingBrickSilhouettePosition() {
		adjustPositionOfRightFallingBrickSilhouette();
		removeRightFallingBrickSilhouetteFromGridToAvoidCollisionWithRightFallingBrick();
	}

	private void adjustPositionOfRightFallingBrickSilhouette() {
		var fallingBrick = getRightFallingBrickSilhouette();
		adjustPositionOf(fallingBrick);
	}

	private Transform getRightFallingBrickSilhouette() {
		var brick = getRightFallingBrick();
		return getSilhouetteOf(brick);
	}

	private void removeRightFallingBrickSilhouetteFromGridToAvoidCollisionWithRightFallingBrick() {
		var minosset = getRightFallingBrickSilhouette();
		var updater = new GridUpdater(minosset);
		updater.removeBrickFromOldPositionInGrid();
	}

	private void checkBricksAbilityToContinueFall() {
		checkLeftFallingBrickAbilityToContinueFall();
		checkRightFallingBrickAbilityToContinueFall();
	}

	private void checkLeftFallingBrickAbilityToContinueFall() {
		var activeBrick = getLeftFallingBrick();
		checkAbilityToContinueFallFor(activeBrick);
	}

	private void checkAbilityToContinueFallFor(Transform activeBrick) {
		var checker = new BrickAbilityToFallChecker(activeBrick);
		checker.checkBrickAbilityToContinueFall();
	}

	private void checkRightFallingBrickAbilityToContinueFall() {
		var activeBrick = getRightFallingBrick();
		checkAbilityToContinueFallFor(activeBrick);
	}

	private void adjustBricksPositions() {
		adjustLeftFallingBrickPosition();
		adjustRightFallingBrickPosition();
	}

	private void adjustLeftFallingBrickPosition() {
		var brick = getLeftFallingBrick();
		adjustPositionOf(brick);
	}

	private void adjustRightFallingBrickPosition() {
		var brick = getRightFallingBrick();
		adjustPositionOf(brick);
	}
}