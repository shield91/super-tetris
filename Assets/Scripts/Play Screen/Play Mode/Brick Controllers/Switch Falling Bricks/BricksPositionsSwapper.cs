using UnityEngine;

namespace AssemblyCSharp
{
	public class BricksPositionsSwapper	{
		private SwitchFallingBricks requester;
		private Vector3 leftFallingBrickLocalPosition;
		private Vector3 rightFallingBrickLocalPosition;
		private Vector3 leftFallingBrickSilhouetteLocalPosition;
		private Vector3 rightFallingBrickSilhouetteLocalPosition;

		public BricksPositionsSwapper(SwitchFallingBricks suppliant) {
			setRequester(suppliant);
			saveFallingBricksLocalPositions();
			saveFallingBricksSilhouettesLocalPositions();
		}

		private void setRequester(SwitchFallingBricks value) {
			requester = value;
		}

		private void saveFallingBricksLocalPositions() {
			saveLeftFallingBrickLocalPosition();
			saveRightFallingBrickLocalPosition();
		}
		
		private void saveLeftFallingBrickLocalPosition() {
			var brick = getLeftFallingBrickFromRequester();
			var value = brick.localPosition;
			setLeftFallingBrickLocalPosition(value);
		}
		
		private Transform getLeftFallingBrickFromRequester() {
			var suppliant = getRequester();
			return suppliant.getLeftFallingBrick();
		}
		
		private SwitchFallingBricks getRequester() {
			return requester;
		}

		private void setLeftFallingBrickLocalPosition(Vector3 value) {
			leftFallingBrickLocalPosition = value;
		}
		
		private void saveRightFallingBrickLocalPosition() {
			var brick = getRightFallingBrickFromRequester();
			var value = brick.localPosition;
			setRightFallingBrickLocalPosition(value);
		}
		
		private Transform getRightFallingBrickFromRequester() {
			var suppliant = getRequester();
			return suppliant.getRightFallingBrick();
		}

		private void setRightFallingBrickLocalPosition(Vector3 value) {
			rightFallingBrickLocalPosition = value;
		}

		private void saveFallingBricksSilhouettesLocalPositions() {
			saveLeftFallingBrickSilhouetteLocalPosition();
			saveRightFallingBrickSilhouetteLocalPosition();
		}

		private void saveLeftFallingBrickSilhouetteLocalPosition() {
			var silhouette = getLeftFallingBrickSilhouetteFromRequester();
			var value = silhouette.localPosition;
			setLeftFallingBrickSilhouetteLocalPosition(value);
		}

		private Transform getLeftFallingBrickSilhouetteFromRequester() {
			const string NAME = "Silhouette";
			var fallingBrick = getLeftFallingBrickFromRequester();
			var playField = fallingBrick.parent;
			return playField.Find(NAME);
		}

		private void setLeftFallingBrickSilhouetteLocalPosition(Vector3 value) {
			leftFallingBrickSilhouetteLocalPosition = value;
		}

		private void saveRightFallingBrickSilhouetteLocalPosition() {
			var silhouette = getRightFallingBrickSilhouetteFromRequester();
			var value = silhouette.localPosition;
			setRightFallingBrickSilhouetteLocalPosition(value);
		}

		private Transform getRightFallingBrickSilhouetteFromRequester() {
			const string NAME = "Silhouette";
			var fallingBrick = getRightFallingBrickFromRequester();
			var playField = fallingBrick.parent;
			return playField.Find(NAME);
		}

		private void setRightFallingBrickSilhouetteLocalPosition(Vector3 value) {
			rightFallingBrickSilhouetteLocalPosition = value;
		}

		public void swapBricksPositions() {
			saveFallingBricksLocalPositions();
			swapFallingBricksParents();
			loadFallingBricksLocalPositions();
			saveFallingBricksSilhouettesLocalPositions();
			swapFallingBricksSilhouettesParents();
			loadFallingBricksSilhouettesLocalPositions();
		}

		private void swapFallingBricksParents() {
			var leftFallingBrick = getLeftFallingBrickFromRequester();
			var rightFallingBrick = getRightFallingBrickFromRequester();
			var updaterForLeftFallingBrick = new GridUpdater(leftFallingBrick);
			var updaterForRightFallingBrick = new GridUpdater(rightFallingBrick);
			var leftPlayField = leftFallingBrick.parent;
			var rightPlayField = rightFallingBrick.parent;
			updaterForLeftFallingBrick.removeBrickFromOldPositionInGrid();
			updaterForRightFallingBrick.removeBrickFromOldPositionInGrid();
			leftFallingBrick.SetParent(rightPlayField);
			rightFallingBrick.SetParent(leftPlayField);
		}

		private void loadFallingBricksLocalPositions() {
			loadLeftFallingBrickLocalPosition();
			loadRightFallingBrickLocalPosition();
		}

		private void loadLeftFallingBrickLocalPosition() {
			var brick = getLeftFallingBrickFromRequester();
			var localPosition = getRightFallingBrickLocalPosition();
			brick.localPosition = localPosition;
		}

		private Vector3 getRightFallingBrickLocalPosition() {
			return rightFallingBrickLocalPosition;
		}

		private void loadRightFallingBrickLocalPosition() {
			var brick = getRightFallingBrickFromRequester();
			var localPosition = getLeftFallingBrickLocalPosition();
			brick.localPosition = localPosition;
		}

		private Vector3 getLeftFallingBrickLocalPosition() {
			return leftFallingBrickLocalPosition;
		}

		private void swapFallingBricksSilhouettesParents() {
			var leftFallingBrick = getLeftFallingBrickFromRequester();
			var rightFallingBrick = getRightFallingBrickFromRequester();
			var leftPlayField = leftFallingBrick.parent;
			var rightPlayField = rightFallingBrick.parent;
			var leftFallingBrickSilhouette = getLeftFallingBrickSilhouetteFromRequester();
			var rightFallingBrickSilhouette = getRightFallingBrickSilhouetteFromRequester();
			leftFallingBrickSilhouette.SetParent(rightPlayField);
			rightFallingBrickSilhouette.SetParent(leftPlayField);
			leftFallingBrick.SetAsLastSibling();
			rightFallingBrick.SetAsLastSibling();
		}

		private void loadFallingBricksSilhouettesLocalPositions() {
			loadLeftFallingBrickSilhouetteLocalPosition();
			loadRightFallingBrickSilhouetteLocalPosition();
		}

		private void loadLeftFallingBrickSilhouetteLocalPosition() {
			var silhouette = getLeftFallingBrickSilhouetteFromRequester();
			var localPosition = getAdjustedLocalPositionForLeftFallingBrickSilhouette();
			silhouette.localPosition = localPosition;
		}

		private Vector3 getAdjustedLocalPositionForLeftFallingBrickSilhouette() {
			var rightFallingBrickPosition = getRightFallingBrickLocalPosition();
			var leftFallingBrickSilhouettePosition = getLeftFallingBrickSilhouetteLocalPosition();
			var x = rightFallingBrickPosition.x;
			var y = leftFallingBrickSilhouettePosition.y;
			var z = leftFallingBrickSilhouettePosition.z;
			return new Vector3(x, y, z);
		}

		private Vector3 getLeftFallingBrickSilhouetteLocalPosition() {
			return leftFallingBrickSilhouetteLocalPosition;
		}

		private void loadRightFallingBrickSilhouetteLocalPosition() {
			var silhouette = getRightFallingBrickSilhouetteFromRequester();
			var localPosition = getAdjustedLocalPositionForRightFallingBrickSilhouette();
			silhouette.localPosition = localPosition;
		}

		private Vector3 getAdjustedLocalPositionForRightFallingBrickSilhouette() {
			var leftFallingBrickPosition = getLeftFallingBrickLocalPosition();
			var rightFallingBrickSilhouettePosition = getRightFallingBrickSilhouetteLocalPosition();
			var x = leftFallingBrickPosition.x;
			var y = rightFallingBrickSilhouettePosition.y;
			var z = rightFallingBrickSilhouettePosition.z;
			return new Vector3(x, y, z);
		}

		private Vector3 getRightFallingBrickSilhouetteLocalPosition() {
			return rightFallingBrickSilhouetteLocalPosition;
		}
	}
}