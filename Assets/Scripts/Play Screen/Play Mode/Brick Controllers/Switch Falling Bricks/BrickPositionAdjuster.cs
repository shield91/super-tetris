using UnityEngine;

namespace AssemblyCSharp
{
	public class BrickPositionAdjuster {
		private Transform fallingTetromino;

		public BrickPositionAdjuster(Transform fallingBrick) {
			var value = fallingBrick;
			setFallingTetromino(value);
		}

		private void setFallingTetromino(Transform value) {
			fallingTetromino = value;
		}

		public void adjustBrickPosition() {
			startPhaseOne();
			fastenBrickPosition();
		}

		private void startPhaseOne() {
			while (brickPositionIsInvalid()) {
				goToPhaseTwo();
			}
		}

		private bool brickPositionIsInvalid() {
			var fallingBrick = getFallingTetromino();
			var validator = new BrickPositionValidator(fallingBrick);
			return !validator.brickPositionIsValid();
		}

		private Transform getFallingTetromino() {
			return fallingTetromino;
		}

		private void goToPhaseTwo() {
			var fallingBrick = getFallingTetromino();
			fallingBrick.localPosition += Vector3.up;
		}

		private void fastenBrickPosition() {
			var fallingBrick = getFallingTetromino();
			var updater = new GridUpdater(fallingBrick);
			updater.addBrickToNewPositionInGrid();
		}
	}
}