﻿using UnityEngine;
using AssemblyCSharp;

public class FallingBrickRotateClockwiseGestureListener : FallingBrickDragGestureListener {
	protected override Vector2 getMovement() {
		var movement = base.getMovement();
		return -movement;
	}

	protected override BrickControlOperation getOperationForFallingBrick() {
		var activeBrick = getFallingBrick();
		return new RotateClockwise(activeBrick);
	}
}