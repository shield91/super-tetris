﻿using UnityEngine;

public abstract class FallingBrickDragGestureListener : FallingBrickControlGestureListener {
	private Vector2 gestureStartPosition;

	private void Start() {
		resetGestureStartTime();
		resetDelayTime();
		resetGestureStartPosition();
	}

	private void resetGestureStartPosition() {
		var value = Vector2.zero;
		setGestureStartPosition(value);
	}

	private void setGestureStartPosition(Vector2 value) {
		gestureStartPosition = value;
	}

	public override void processPlayerGestureStart() {
		setGestureStartTime();
		setGestureStartPosition();
	}

	private void setGestureStartTime() {
		base.processPlayerGestureStart();
	}

	private void setGestureStartPosition() {
		var value = getInputWorldPosition();
		setGestureStartPosition(value);
	}

	public override bool playerHasFinishedTheRightGesture() {
		bool ba = playerHasDraggedFingerFarEnough();
		bool bb = itIsTimeToApplyOperation();
		return ba && bb;
	}

	private bool playerHasDraggedFingerFarEnough() {
		float distance = getDistance();
		float movementOnAxis = getMovementOnAxis();
		return movementOnAxis > distance;
	}

	protected virtual float getDistance() {
		return 2f;
	}

	protected virtual float getMovementOnAxis() {
		var movement = getMovement();
		return movement.x;
	}

	protected virtual Vector2 getMovement() {
		var startPosition = getGestureStartPosition();
		var endPosition = getGestureEndPosition();
		return startPosition - endPosition;
	}

	private Vector2 getGestureStartPosition() {
		return gestureStartPosition;
	}

	private Vector2 getGestureEndPosition() {
		return getInputWorldPosition();
	}

	protected override bool itIsTimeToApplyOperation() {
		bool b = base.itIsTimeToApplyOperation();
		return !b;
	}

	public override void apllyGestureOperationOnBrick() {
		applyOperationOnBrick();
	}

	private void OnMouseUp() {
	}
}