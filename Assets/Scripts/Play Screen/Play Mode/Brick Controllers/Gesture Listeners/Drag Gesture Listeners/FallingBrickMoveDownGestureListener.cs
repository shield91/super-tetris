﻿using UnityEngine;
using AssemblyCSharp;
using System.Collections;

public class FallingBrickMoveDownGestureListener : FallingBrickDragGestureListener {
	protected override float getDistance() {
		return 3f;
	}

	protected override float getMovementOnAxis() {
		var movement = getMovement();
		return movement.y;
	}

	protected override void applyOperationOnBrick() {
		var routine = landFallingBrick();
		StartCoroutine(routine);
	}

	private IEnumerator landFallingBrick() {
		while (brickIsInMotion()) {
			yield return processBrickMotionDuringWaitingForTheStop();
		}
	}
	
	private bool brickIsInMotion() {
		var fallingBrick = getFallingBrick();
		var b = fallingBrick.GetComponent<Brick>();
		return !b.isMotionless();
	}
	
	private WaitForSeconds processBrickMotionDuringWaitingForTheStop() {
		processFallingBrickMotion();
		return waitForFewMoments();
	}
	
	private void processFallingBrickMotion() {
		for (int i = 0; i < 3; ++i) {
			applyMoveDownwardsOnBrick();
		}
	}
	
	private void applyMoveDownwardsOnBrick() {
		base.applyOperationOnBrick();
	}

	protected override BrickControlOperation getOperationForFallingBrick() {
		var activeBrick = getFallingBrick();
		return new MoveDown(activeBrick);
	}
	
	private WaitForSeconds waitForFewMoments() {
		const float F = 10f;
		const float P = -10f;
		float seconds = Mathf.Pow(F, P);
		return new WaitForSeconds(seconds);
	}
}