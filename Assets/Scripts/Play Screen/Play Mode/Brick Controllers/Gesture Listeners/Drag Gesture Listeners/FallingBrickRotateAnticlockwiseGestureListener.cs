﻿using AssemblyCSharp;

public class FallingBrickRotateAnticlockwiseGestureListener : FallingBrickDragGestureListener {
	protected override BrickControlOperation getOperationForFallingBrick() {
		var activeBrick = getFallingBrick();
		return new RotateAnticlockwise(activeBrick);
	}
}