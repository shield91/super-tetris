﻿public abstract class FallingBrickTapGestureListener : FallingBrickControlGestureListener {
	private void OnMouseDrag() {
	}

	public override void apllyGestureOperationOnBrick() {
		if (itIsTimeToApplyOperation()) {
			applyOperation();
		}
	}

	private void applyOperation() {
		applyTapOperationOnBrick();
		updateDelayTime();
	}

	public void applyTapOperationOnBrick() {
		applyOperationOnBrick();
		updateGestureStartTime();
	}

	private void updateGestureStartTime() {
		processPlayerGestureStart();
	}

	private void updateDelayTime() {
		const float VALUE = 0.2f;
		setDelayTime(VALUE);
	}
	
	private void OnMouseUp() {
	}
}