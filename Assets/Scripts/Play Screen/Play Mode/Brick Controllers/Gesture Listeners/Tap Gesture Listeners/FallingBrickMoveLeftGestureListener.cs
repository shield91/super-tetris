﻿using UnityEngine;
using AssemblyCSharp;

public class FallingBrickMoveLeftGestureListener : FallingBrickTapGestureListener {
	public override bool playerHasFinishedTheRightGesture() {
		return playerFingerIsOnTheLeftHalfOfThePlayField();
	}

	private bool playerFingerIsOnTheLeftHalfOfThePlayField() {
		var inputPosition = getInputWorldPosition();
		var collider = GetComponent<Collider2D>();
		var bounds = collider.bounds;
		var center = bounds.center;
		var inputX = inputPosition.x;
		var centerX = center.x;
		return inputX <= centerX;
	}

	protected override BrickControlOperation getOperationForFallingBrick() {
		var activeBrick = getFallingBrick();
		return new MoveLeft(activeBrick);
	}
}