﻿using AssemblyCSharp;

public class FallingBrickMoveRightGestureListener : FallingBrickMoveLeftGestureListener {
	public override bool playerHasFinishedTheRightGesture() {
		return playerFingerIsOnTheRightHalfOfThePlayField();
	}

	private bool playerFingerIsOnTheRightHalfOfThePlayField() {
		bool b = base.playerHasFinishedTheRightGesture();
		return !b;
	}

	protected override BrickControlOperation getOperationForFallingBrick() {
		var activeBrick = getFallingBrick();
		return new MoveRight(activeBrick);
	}
}