using UnityEngine;
using AssemblyCSharp;

public class FallingBrickControlGestureListener : MonoBehaviour {
	private float gestureStartTime;
	private float delayTime;

	private void Start() {
		resetGestureStartTime();
		resetDelayTime();
	}

	protected void resetGestureStartTime() {
		const float VALUE = 0f;
		setGestureStartTime(VALUE);
	}

	private void setGestureStartTime(float value) {
		gestureStartTime = value;
	}

	protected void resetDelayTime() {
		const float VALUE = 0.5f;
		setDelayTime(VALUE);
	}

	protected void setDelayTime(float value) {
		delayTime = value;
	}

	private void OnMouseDown() {
		processPlayerGestureStart();
	}
	
	public virtual void processPlayerGestureStart() {
		float value = Time.time;
		setGestureStartTime(value);
	}

	protected Vector2 getInputWorldPosition() {
		var mainCamera = Camera.main;
		var position = Input.mousePosition;
		return mainCamera.ScreenToWorldPoint(position);
	}

	private void processPlayerGestureEnd() {
		if (playerHasFinishedTheRightGesture()) {
			apllyGestureOperationOnBrick();
		}
	}
	
	public virtual bool playerHasFinishedTheRightGesture() {
		return false;
	}
	
	public virtual void apllyGestureOperationOnBrick() {
	}

	protected virtual bool itIsTimeToApplyOperation() {
		float endTime = getGestureEndTime();
		float startTime = getGestureStartTime();
		float time = getDelayTime();
		return endTime - startTime >= time;
	}

	private float getGestureEndTime() {
		return Time.time;
	}

	private float getGestureStartTime() {
		return gestureStartTime;
	}

	private float getDelayTime() {
		return delayTime;
	}

	protected virtual void applyOperationOnBrick() {
		var operation = getOperationForFallingBrick();
		operation.apply();
	}
	
	protected virtual BrickControlOperation getOperationForFallingBrick() {
		return null;
	}

	protected Transform getFallingBrick() {
		var pf = GetComponentInParent<PlayField>();
		return pf.getFallingBrick();
	}
	
	private void OnMouseDrag() {
		processPlayerGestureEndForMoveLeft();
		processPlayerGestureEndForMoveRight();
		updateSilhouetteInOnMouseDragMethod();
	}

	private void processPlayerGestureEndForMoveLeft() {
		var listener = GetComponent<FallingBrickMoveLeftGestureListener>();
		listener.processPlayerGestureEnd();
	}

	private void processPlayerGestureEndForMoveRight() {
		var listener = GetComponent<FallingBrickMoveRightGestureListener>();
		listener.processPlayerGestureEnd();
	}

	private void updateSilhouetteInOnMouseDragMethod() {
		var silhouetteStartPosition = getSilhouetteStartPositionForOnMouseDragMethod();
		updateSilhouetteStartingAt(silhouetteStartPosition);
	}

	private Vector3 getSilhouetteStartPositionForOnMouseDragMethod() {
		var fallingBrick = getFallingBrick();
		var silhouette = getSilhouette();
		var fallingBrickPosition = fallingBrick.position;
		var silhouettePosition = silhouette.position;
		var x = fallingBrickPosition.x;
		var y = silhouettePosition.y;
		var z = silhouettePosition.z;
		return new Vector3(x, y, z);
	}

	private Transform getSilhouette() {
		var pf = GetComponentInParent<PlayField>();
		return pf.getSilhouette();
	}

	private void updateSilhouetteStartingAt(Vector3 silhouetteStartPosition) {
		setUpSilhouetteTransformAt(silhouetteStartPosition);
		removeBrickFromGridToAvoidCollisionWithSilhouette();
		adjustSilhouettePosition();
		moveSilhouetteDownToItsFirstObstacle();
		removeSilhouetteFromGridToAllowBrickToReturnThere();
		returnBrickToGrid();
	}

	private void setUpSilhouetteTransformAt(Vector3 silhouetteStartPosition) {
		var fallingBrick = getFallingBrick();
		var silhouette = getSilhouette();
		silhouette.position = silhouetteStartPosition;
		silhouette.rotation = fallingBrick.rotation;
	}

	private void removeBrickFromGridToAvoidCollisionWithSilhouette() {
		var minosset = getFallingBrick();
		var updater = new GridUpdater(minosset);
		updater.removeBrickFromOldPositionInGrid();
	}

	private void adjustSilhouettePosition() {
		var fallingBrick = getSilhouette();
		var adjuster = new BrickPositionAdjuster(fallingBrick);
		adjuster.adjustBrickPosition();
	}

	private void moveSilhouetteDownToItsFirstObstacle() {
		resetSilhouetteMotion();
		moveSilhouetteDownwardToItsFirstObstacle();
	}

	private void resetSilhouetteMotion() {
		var b = getBrickComponentFromSilhouette();
		b.resetMotion();
	}

	private Brick getBrickComponentFromSilhouette() {
		var silhouette = getSilhouette();
		return silhouette.GetComponent<Brick>();
	}

	private void moveSilhouetteDownwardToItsFirstObstacle() {
		while (silhouetteIsFarEnoughFromItsFirstObstacle()) {
			moveSilhouetteDown();
		}
	}

	private bool silhouetteIsFarEnoughFromItsFirstObstacle() {
		var brick = getBrickComponentFromSilhouette();
		var b = brick.isMotionless();
		return !b;
	}

	private void moveSilhouetteDown() {
		var activeBrick = getSilhouette();
		var operation = new MoveDown(activeBrick);
		operation.apply();
	}

	private void removeSilhouetteFromGridToAllowBrickToReturnThere() {
		var minosset = getSilhouette();
		var updater = new GridUpdater(minosset);
		updater.removeBrickFromOldPositionInGrid();
	}

	private void returnBrickToGrid() {
		var minosset = getFallingBrick();
		var updater = new GridUpdater(minosset);
		updater.addBrickToNewPositionInGrid();
	}
	
	private void OnMouseUp() {
		updateBrick();
		updateSilhouetteInOnMouseUpMethod();
	}

	private void updateBrick() {
		if (playerHasAskedToMoveBrickDown()) {
			moveBrickDown();
		} else if (playerHasAskedToRotateBrickAnticlockwise()) {
			rotateBrickAnticlockwise();
		} else if (playerHasAskedToRotateBrickClockwise()) {
			rotateBrickClockwise();
		} else if (playerHasAskedToMoveBrickLeft()) {
			moveBrickLeft();
		} else if (playerHasAskedToMoveBrickRight()) {
			moveBrickRight();
		}
	}

	private bool playerHasAskedToMoveBrickDown() {
		var listener = GetComponent<FallingBrickMoveDownGestureListener>();
		return listener.playerHasFinishedTheRightGesture();
	}

	private void moveBrickDown() {
		var listener = GetComponent<FallingBrickMoveDownGestureListener>();
		listener.apllyGestureOperationOnBrick();
	}

	private bool playerHasAskedToRotateBrickAnticlockwise() {
		var listener = GetComponent<FallingBrickRotateAnticlockwiseGestureListener>();
		return listener.playerHasFinishedTheRightGesture();
	}

	private void rotateBrickAnticlockwise() {
		var listener = GetComponent<FallingBrickRotateAnticlockwiseGestureListener>();
		listener.apllyGestureOperationOnBrick();
	}

	private bool playerHasAskedToRotateBrickClockwise() {
		var listener = GetComponent<FallingBrickRotateClockwiseGestureListener>();
		return listener.playerHasFinishedTheRightGesture();
	}

	private void rotateBrickClockwise() {
		var listener = GetComponent<FallingBrickRotateClockwiseGestureListener>();
		listener.apllyGestureOperationOnBrick();
	}

	private bool playerHasAskedToMoveBrickLeft() {
		var listener = GetComponent<FallingBrickMoveLeftGestureListener>();
		return playerHasMeetTheConditionsFor(listener);
	}

	private bool playerHasMeetTheConditionsFor(FallingBrickTapGestureListener listener) {
		bool ba = playerHasFinishedTheRightGestureFor(listener);
		bool bb = itIsTimeToApplyOperationFor(listener);
		return ba && bb;
	}

	private bool playerHasFinishedTheRightGestureFor(FallingBrickTapGestureListener listener) {
		return listener.playerHasFinishedTheRightGesture();
	}

	private bool itIsTimeToApplyOperationFor(FallingBrickTapGestureListener listener) {
		bool b = listener.itIsTimeToApplyOperation();
		return !b;
	}

	private void moveBrickLeft() {
		var listener = GetComponent<FallingBrickMoveLeftGestureListener>();
		listener.applyTapOperationOnBrick();
	}

	private bool playerHasAskedToMoveBrickRight() {
		var listener = GetComponent<FallingBrickMoveRightGestureListener>();
		return playerHasMeetTheConditionsFor(listener);
	}

	private void moveBrickRight() {
		var listener = GetComponent<FallingBrickMoveRightGestureListener>();
		listener.applyTapOperationOnBrick();
	}

	private void updateSilhouetteInOnMouseUpMethod() {
		var silhouetteStartPosition = getSilhouetteStartPositionForOnMouseUpMethod();
		updateSilhouetteStartingAt(silhouetteStartPosition);
	}

	private Vector3 getSilhouetteStartPositionForOnMouseUpMethod() {
		var fallingBrick = getFallingBrick();
		return fallingBrick.position;
	}
}