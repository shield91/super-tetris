using UnityEngine;

namespace AssemblyCSharp
{
	public class BrickPainter {
		private Transform brick;
		private Transform blockWithDesiredColor;
		private int currentBlockIndex;

		public BrickPainter(Transform minosset) {
			setBrick(minosset);
			resetBlockWithDesiredColor();
			resetCurrentBlockIndex();
		}

		private void setBrick(Transform value) {
			brick = value;
		}

		private void resetBlockWithDesiredColor() {
			var obj = new GameObject();
			var value = obj.transform;
			Object.Destroy(obj);
			setBlockWithDesiredColor(value);
		}

		private void setBlockWithDesiredColor(Transform value) {
			blockWithDesiredColor = value;
		}

		private void resetCurrentBlockIndex() {
			const int VALUE = 0;
			setCurrentBlockIndex(VALUE);
		}

		private void setCurrentBlockIndex(int value) {
			currentBlockIndex = value;
		}

		public void paintBrick() {
			setBlockWithDesiredColorRandomly();
			resetCurrentBlockIndex();
			paintBlocks();
		}

		private void setBlockWithDesiredColorRandomly() {
			const string NAME = "Level Editor Panel";
			const int MIN = 0;
			var minosset = getBrick();
			var playField = minosset.parent;
			var panel = playField.Find(NAME);
			var childCount = panel.childCount;
			var max = childCount - 1;
			var index = Random.Range(MIN, max);
			var value = panel.GetChild(index);
			setBlockWithDesiredColor(value);
		}

		private Transform getBrick() {
			return brick;
		}

		private void paintBlocks() {
			while (thereAreBlocksNeedToBePainted()) {
				processCurrentBlock();
			}
		}
		
		private bool thereAreBlocksNeedToBePainted() {
			int i = getCurrentBlockIndex();
			int n = getNumberOfBlocks();
			return i < n;
		}
		
		private int getCurrentBlockIndex() {
			return currentBlockIndex;
		}
		
		private int getNumberOfBlocks() {
			var tetromino = getBrick();
			return tetromino.childCount;
		}

		private void processCurrentBlock() {
			paintCurrentBlock();
			moveOnToTheNextBlock();
		}
		
		private void paintCurrentBlock() {
			var sra = getSpriteRendererFromCurrentBlock();
			var srb = getSpriteRendererFromBlockWithDesiredColor();
			sra.sprite = srb.sprite;
			sra.sortingOrder = srb.sortingOrder;
		}

		private SpriteRenderer getSpriteRendererFromCurrentBlock() {
			var block = getCurrentBlockByItsIndex();
			return block.GetComponent<SpriteRenderer>();
		}
		
		private Transform getCurrentBlockByItsIndex() {
			var index = getCurrentBlockIndex();
			var tetromino = getBrick();
			return tetromino.GetChild(index);
		}

		private SpriteRenderer getSpriteRendererFromBlockWithDesiredColor() {
			var block = getBlockWithDesiredColor();
			return block.GetComponent<SpriteRenderer>();
		}

		private Transform getBlockWithDesiredColor() {
			return blockWithDesiredColor;
		}
		
		private void moveOnToTheNextBlock() {
			int index = getCurrentBlockIndex();
			int value = index + 1;
			setCurrentBlockIndex(value);
		}
	}
}