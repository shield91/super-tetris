﻿using UnityEngine;

namespace AssemblyCSharp
{
	public class SilhouetteLookAdjuster {
		private Transform silhouette;
		private int currentBlockIndex;

		public SilhouetteLookAdjuster(Transform ghostBrick)	{
			setSilhouette(ghostBrick);
			resetCurrentBlockIndex();
		}

		private void setSilhouette(Transform value) {
			silhouette = value;
		}

		private void resetCurrentBlockIndex() {
			const int VALUE = 0;
			setCurrentBlockIndex(VALUE);
		}

		private void setCurrentBlockIndex(int value) {
			currentBlockIndex = value;
		}

		public void setUpSilhouetteLook() {
			resetCurrentBlockIndex();
			setUpBlocksLook();
		}

		private void setUpBlocksLook() {
			while (thereAreBlocksNeedToBeSetUp()) {
				setUpCurrentBlock();
			}
		}

		private bool thereAreBlocksNeedToBeSetUp() {
			int n = getSilhouetteBlocksNumber();
			int i = getCurrentBlockIndex();
			return i < n;
		}

		private int getSilhouetteBlocksNumber() {
			var renderers = getSpriteRenderersFromSilhouette();
			return renderers.Length;
		}

		private SpriteRenderer[] getSpriteRenderersFromSilhouette() {
			var ghostBrick = getSilhouette();
			return ghostBrick.GetComponentsInChildren<SpriteRenderer>();
		}

		private Transform getSilhouette() {
			return silhouette;
		}

		private int getCurrentBlockIndex() {
			return currentBlockIndex;
		}

		private void setUpCurrentBlock() {
			setUpCurrentBlockLook();
			moveOnToTheNextBlock();
		}

		private void setUpCurrentBlockLook() {
			var renderers = getSpriteRenderersFromSilhouette();
			var index = getCurrentBlockIndex();
			var renderer = renderers[index];
			renderer.sortingOrder = -6;
		}

		private void moveOnToTheNextBlock() {
			int index = getCurrentBlockIndex();
			int value = index + 1;
			setCurrentBlockIndex(value);
		}
	}
}