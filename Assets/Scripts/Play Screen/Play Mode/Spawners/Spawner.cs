﻿using UnityEngine;
using AssemblyCSharp;

public class Spawner : MonoBehaviour {
	private void Start() {
		if (levelEditorIsEnabled()) {
			destroySpawner();
		} else {
			setUpSpawnerInPlayMode();
		}
	}
	
	private bool levelEditorIsEnabled() {
		var ps = GetComponentInParent<PlayScreen>();
		return ps.levelEditorModeIsEnabled();
	}
	
	private void destroySpawner() {
		var obj = gameObject;
		Destroy(obj);
	}

	private void setUpSpawnerInPlayMode() {
		fillUpBrickPreviewWindow();
		spawnBrick();
	}

	private void fillUpBrickPreviewWindow() {
		var parent = getBrickPreview();
		var path = getPathForNewBrick();
		var original = Resources.Load(path);
		Instantiate(original, parent);
	}

	private Transform getBrickPreview() {
		const string NAME = "Brick Preview";
		var playField = transform.parent;
		return playField.Find(NAME);
	}
	
	private string getPathForNewBrick() {
		const string HEADER = "Brick ";
		int number = getNumberOfNewBrick();
		return HEADER + number;
	}
	
	private int getNumberOfNewBrick() {
		const int MIN = 1;
		const int MAX = 8;
		return Random.Range(MIN, MAX);
	}

	public void spawnBrick() {
		moveBrickFromBrickPreviewWindowToSpawnPoint();
		setUpBrickAtSpawnPoint();
		fillUpBrickPreviewWindow();
		processBrickPositionAtSpawnPoint();
	}

	private void moveBrickFromBrickPreviewWindowToSpawnPoint() {
		var brick = getBrickInBrickPreviewWindow();
		var parent = transform.parent;
		brick.SetParent(parent);
	}

	private Transform getBrickInBrickPreviewWindow() {
		var preview = getBrickPreview();
		var brick = preview.GetComponentInChildren<Brick>();
		return brick.transform;
	}

	private void setUpBrickAtSpawnPoint() {
		var brick = getBrickAtSpawnPoint();
		brick.position = transform.position;
		brick.localScale = Vector3.one;
	}

	private Transform getBrickAtSpawnPoint() {
		var playField = transform.parent;
		var childCount = playField.childCount;
		var index = childCount - 1;
		return playField.GetChild(index);
	}

	private void processBrickPositionAtSpawnPoint() {
		if (brickPositionIsValid()) {
			processBrickLaunch();
		} else {
			processGameFinish();
		}
	}

	private bool brickPositionIsValid() {
		var fallingBrick = getBrickAtSpawnPoint();
		var validator = new BrickPositionValidator(fallingBrick);
		return validator.brickPositionIsValid();
	}

	private void processBrickLaunch() {
		setUpSilhouette();
		setColorForNewBrick();
		launchBrick();
	}

	private void setUpSilhouette() {
		removeBrickFromGridToAvoidCollisionWithSilhouette();
		createSilhouette();
		setUpSilhouetteLook();
		moveSilhouetteToItsFirstObstacle();
		removeSilhouetteFromGridToAllowBrickToReturnThere();
		returnBrickToGrid();
	}

	private void removeBrickFromGridToAvoidCollisionWithSilhouette() {
		var minosset = getBrickAtSpawnPoint();
		var updater = new GridUpdater(minosset);
		updater.removeBrickFromOldPositionInGrid();
	}

	private void createSilhouette() {
		const int INDEX = 4;
		var brick = getBrickAtSpawnPoint();
		var parent = brick.parent;
		var silhouette = Instantiate(brick, parent);
		silhouette.name = "Silhouette";
		silhouette.rotation = brick.rotation;
		silhouette.SetSiblingIndex(INDEX);
	}

	private void setUpSilhouetteLook() {
		var ghostBrick = getSilhouette();
		var adjuster = new SilhouetteLookAdjuster(ghostBrick);
		adjuster.setUpSilhouetteLook();
	}

	private Transform getSilhouette() {
		var playField = GetComponentInParent<PlayField>();
		return playField.getSilhouette();
	}

	private void moveSilhouetteToItsFirstObstacle() {
		while (silhouetteIsFarEnoughFromItsFirstObstacle()) {
			continueSilhouetteTrip();
		}
	}

	private bool silhouetteIsFarEnoughFromItsFirstObstacle() {
		var silhouette = getSilhouette();
		var brick = silhouette.GetComponent<Brick>();
		var b = brick.isMotionless();
		return !b;
	}

	private void continueSilhouetteTrip() {
		var activeBrick = getSilhouette();
		var operation = new MoveDown(activeBrick);
		operation.apply();
	}

	private void removeSilhouetteFromGridToAllowBrickToReturnThere() {
		var minosset = getSilhouette();
		var updater = new GridUpdater(minosset);
		updater.removeBrickFromOldPositionInGrid();	
	}

	private void returnBrickToGrid() {
		var minosset = getBrickAtSpawnPoint();
		var updater = new GridUpdater(minosset);
		updater.addBrickToNewPositionInGrid();
	}

	private void setColorForNewBrick() {
		var minosset = getBrickAtSpawnPoint();
		var painter = new BrickPainter(minosset);
		painter.paintBrick();
	}

	private void launchBrick() {
		const string NAME = "Falling Brick";
		var brick = getBrickAtSpawnPoint();
		brick.name = NAME;
	}

	private void processGameFinish() {
		pauseGame();
		markThatGameIsCompleted();
		destroyBrickAtSpawnPoint();
		showGameIncompletePopUpWindow();
		disableMenuButton();
		disableSwitchButton();
		disablePauseButton();
		disableLeftPlayField();
		disableRightPlayField();
	}

	private void pauseGame() {
		Time.timeScale = 0;
	}

	private void markThatGameIsCompleted() {
		const bool VALUE = true;
		var ps = GetComponentInParent<PlayScreen>();
		ps.setGameComplete(VALUE);
	}

	private void destroyBrickAtSpawnPoint() {
		var brick = getBrickAtSpawnPoint();
		var obj = brick.gameObject;
		Destroy(obj);
	}

	private void showGameIncompletePopUpWindow() {
		const string NAME = "Game Incomplete Pop-Up Window";
		const bool VALUE = true;
		var playScreen = GetComponentInParent<PlayScreen>();
		var gameWindow = playScreen.transform;
		var child = gameWindow.Find(NAME);
		var go = child.gameObject;
		go.SetActive(VALUE);
	}

	private void disableMenuButton() {
		const string NAME = "Footer/Menu";
		disable(NAME);
	}

	private void disable(string name) {
		var playScreen = GetComponentInParent<PlayScreen>();
		var gameWindow = playScreen.transform;
		var child = gameWindow.Find(name);
		var collider = child.GetComponentInChildren<Collider2D>();
		collider.enabled = false;
	}
	
	private void disableSwitchButton() {
		const string NAME = "Footer/Switch";
		disable(NAME);
	}
	
	private void disablePauseButton() {
		const string NAME = "Footer/Pause";
		disable(NAME);
	}

	private void disableLeftPlayField() {
		const string NAME = "Play Fields/Left Play Field/Play Field/Background";
		disable(NAME);
	}

	private void disableRightPlayField() {
		const string NAME = "Play Fields/Right Play Field/Play Field/Background";
		disable(NAME);
	}
}