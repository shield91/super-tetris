using UnityEngine;

public class PlayField : MonoBehaviour {
	private Transform[,] grid;

	private void Start() {
		createGrid();
		setUpPlayField();
	}

	private void createGrid() {
		var width = getWidthOfPlayField();
		var height = getHeightOfPlayField();
		var value = new Transform[width, height];
		setGrid(value);
	}

	private int getWidthOfPlayField() {
		var size = getSizeOfPlayField();
		var width = size.x;
		return (int) width;
	}

	private Vector3 getSizeOfPlayField() {
		const float X = 10f;
		const float Y = 25f;
		return new Vector2(X, Y);
	}

	private int getHeightOfPlayField() {
		const int RESERVE_UNITS = 7;
		var size = getSizeOfPlayField();
		var sizeY = size.y;
		var height = (int) sizeY;
		return height + RESERVE_UNITS;
	}

	private void setGrid(Transform[,] value) {
		grid = value;
	}

	private void setUpPlayField() {
		if (levelEditorModeIsEnabled()) {
			setUpPlayFieldInLevelEditorMode();
		}
	}

	private bool levelEditorModeIsEnabled() {
		var ps = GetComponentInParent<PlayScreen>();
		return ps.levelEditorModeIsEnabled();
	}

	private void setUpPlayFieldInLevelEditorMode() {
		disablePlayField();
		hideBrickPreview();
		showLevelEditorPanel();
	}

	private void disablePlayField() {
		const string NAME = "Play Field/Background";
		var child = get(NAME);
		var obj = child.GetComponent<BoxCollider2D>();
		Destroy(obj);
	}

	private Transform get(string name) {
		return transform.Find(name);
	}

	private void hideBrickPreview() {
		const string NAME = "Play Field/Brick Preview";
		const bool VALUE = false;
		var go = getGameObjectFor(NAME);
		go.SetActive(VALUE);
	}

	private GameObject getGameObjectFor(string name) {
		var child = get(name);
		return child.gameObject;
	}

	private void showLevelEditorPanel() {
		const string NAME = "Play Field/Level Editor Panel";
		const bool VALUE = true;
		var go = getGameObjectFor(NAME);
		go.SetActive(VALUE);
	}

	public int getGridWidth() {
		const int DIMENSION = 0;
		return getGridLengthBy(DIMENSION);
	}

	private int getGridLengthBy(int dimension) {
		var matrix = getGrid();
		return matrix.GetLength(dimension);
	}

	public Transform[,] getGrid() {
		return grid;
	}
	
	public int getGridHeight() {
		const int DIMENSION = 1;
		return getGridLengthBy(DIMENSION);
	}

	public Transform getFallingBrick() {
		const string NAME = "Play Field";
		var playField = get(NAME);
		var childCount = playField.childCount;
		var index = childCount - 1;
		return playField.GetChild(index);
	}

	public Transform getSilhouette() {
		const string NAME = "Play Field/Silhouette";
		return get(NAME);
	}
}