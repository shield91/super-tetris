using UnityEngine;

namespace AssemblyCSharp
{
	public class TimeInMinutesAndSecondsUpdater	{
		private PlayScreen requester;
		private string timeInMinutes;
		private string timeInSeconds;

		public TimeInMinutesAndSecondsUpdater(PlayScreen suppliant) {
			setRequester(suppliant);
			setTimeInMinutes();
			setTimeInSeconds();
		}

		private void setRequester(PlayScreen value) {
			requester = value;
		}

		private void setTimeInMinutes() {
			const int ONE_MINUTE = 60;
			var gameTime = getGameTimeFromRequester();
			var gameTimeInMinutes = gameTime / ONE_MINUTE;
			var value = gameTimeInMinutes.ToString();
			setTimeInMinutes(value);
		}

		private int getGameTimeFromRequester() {
			var suppliant = getRequester();
			return suppliant.getGameTime();
		}

		private PlayScreen getRequester() {
			return requester;
		}

		private void setTimeInMinutes(string value) {
			timeInMinutes = value;
		}

		private void setTimeInSeconds() {
			const int ONE_MINUTE = 60;
			var gameTime = getGameTimeFromRequester();
			var gameTimeInSeconds = gameTime % ONE_MINUTE;
			var value = gameTimeInSeconds.ToString();
			setTimeInSeconds(value);
		}

		private void setTimeInSeconds(string value) {
			timeInSeconds = value;
		}

		public void updateTime() {
			if (timeHasReachedItsMaximum()) {
				setMaximumTime();
			} else {
				setOrdinaryTime();
			}
		}

		private bool timeHasReachedItsMaximum() {
			const int MAXIMUM = 99;
			int time = getIntegerRepresentationOfTimeInMinutes();
			return time > MAXIMUM;
		}

		private int getIntegerRepresentationOfTimeInMinutes() {
			string time = getTimeInMinutes();
			return getIntegerRepresentationOf(time);
		}

		private string getTimeInMinutes() {
			return timeInMinutes;
		}

		private int getIntegerRepresentationOf(string time) {
			string s = time;
			return int.Parse(s);
		}

		private void setMaximumTime() {
			const string VALUE = "99:59";
			setTime(VALUE);
		}

		private void setTime(string value) {
			var tm = getTextMeshFromRequester();
			tm.text = value;
		}

		private TextMesh getTextMeshFromRequester() {
			const string NAME = "Header/Time/Value";
			var suppliant = getRequester();
			var gameWindow = suppliant.transform;
			var time = gameWindow.Find(NAME);
			return time.GetComponent<TextMesh>();
		}

		private void setOrdinaryTime() {
			string value = getOrdinaryTime();
			setTime(value);
		}

		private string getOrdinaryTime() {
			const string SEPARATOR = ":";
			string minutes = getMinutes();
			string seconds = getSeconds();
			return minutes + SEPARATOR + seconds;
		}

		private string getMinutes() {
			string time = getTimeInMinutes();
			return getValidated(time);
		}

		private string getValidated(string time) {
			if (timeIsOneDigitNumber(time)) {
				return getAdjusted(time);
			} else {
				return getOrdinary(time);
			}
		}

		private bool timeIsOneDigitNumber(string time) {
			const int TEN = 10;
			var value = getIntegerRepresentationOf(time);
			return value < TEN;
		}

		private string getAdjusted(string time) {
			const string HEADER = "0";
			return HEADER + time;
		}

		private string getOrdinary(string time) {
			return time;
		}

		private string getSeconds() {
			string time = getTimeInSeconds();
			return getValidated(time);
		}

		private string getTimeInSeconds() {
			return timeInSeconds;
		}
	}
}