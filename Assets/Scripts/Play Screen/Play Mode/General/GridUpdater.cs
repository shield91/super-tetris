using UnityEngine;

namespace AssemblyCSharp
{
	public class GridUpdater {
		private Transform brick;
		private int currentRowIndex;
		private int currentCellIndex;
		private int currentBlockIndex;

		public GridUpdater(Transform minosset) {
			setBrick(minosset);
			resetCurrentRowIndex();
			resetCurrentCellIndex();
			resetCurrentBlockIndex();
		}

		private void setBrick(Transform value) {
			brick = value;
		}

		private void resetCurrentRowIndex() {
			const int VALUE = 0;
			setCurrentRowIndex(VALUE);
		}

		private void setCurrentRowIndex(int value) {
			currentRowIndex = value;
		}

		private void resetCurrentCellIndex() {
			const int VALUE = 0;
			setCurrentCellIndex(VALUE);
		}

		private void setCurrentCellIndex(int value) {
			currentCellIndex = value;
		}

		private void resetCurrentBlockIndex() {
			const int VALUE = 0;
			setCurrentBlockIndex(VALUE);
		}

		private void setCurrentBlockIndex(int value) {
			currentBlockIndex = value;
		}

		public void updateGrid() {
			removeBrickFromOldPositionInGrid();
			addBrickToNewPositionInGrid();
		}

		public void removeBrickFromOldPositionInGrid() {
			resetCurrentRowIndex();
			processRows();
		}

		private void processRows() {
			while (thereAreRowsNeedToBeProcessed()) {
				processCurrentRow();
			}
		}

		private bool thereAreRowsNeedToBeProcessed() {
			int y = getCurrentRowIndex();
			int h = getHeightOfGrid();
			return y < h;
		}

		private int getCurrentRowIndex() {
			return currentRowIndex;
		}

		private int getHeightOfGrid() {
			var playField = getPlayFieldFromBrick();
			return playField.getGridHeight();
		}

		private PlayField getPlayFieldFromBrick() {
			var fallingBrick = getBrick();
			return fallingBrick.GetComponentInParent<PlayField>();
		}

		private Transform getBrick() {
			return brick;
		}

		private void processCurrentRow() {
			resetCurrentCellIndex();
			processCells();
			moveOnToTheNextRow();
		}

		private void processCells() {
			while (thereAreCellsNeedToBeProcessed()) {
				processCurrentCell();
			}
		}

		private bool thereAreCellsNeedToBeProcessed() {
			int x = getCurrentCellIndex();
			int w = getWidthOfGrid();
			return x < w;
		}

		private int getCurrentCellIndex() {
			return currentCellIndex;
		}

		private int getWidthOfGrid() {
			var playField = getPlayFieldFromBrick();
			return playField.getGridWidth();
		}

		private void processCurrentCell() {
			processCell();
			moveOnToTheNextCell();
		}

		private void processCell() {
			if (cellIsOccupied()) {
				processOccupiedCell();
			}
		}

		private bool cellIsOccupied() {
			Transform cell = getCurrentCellByItsCoordinates();
			return cell != null;
		}

		private Transform getCurrentCellByItsCoordinates() {
			var grid = getGridFromPlayField();
			var x = getCurrentCellIndex();
			var y = getCurrentRowIndex();
			return grid[x, y];
		}

		private Transform[,] getGridFromPlayField() {
			var playField = getPlayFieldFromBrick();
			return playField.getGrid();
		}

		private void processOccupiedCell() {
			if (occupiedCellBelongsToFallingBrick()) {
				clearCell();
			}
		}

		private bool occupiedCellBelongsToFallingBrick() {
			var cell = getCurrentCellByItsCoordinates();
			var cellParent = cell.parent;
			var fallingBrick = getBrick();
			return cellParent == fallingBrick;
		}

		private void clearCell() {
			var grid = getGridFromPlayField();
			var x = getCurrentCellIndex();
			var y = getCurrentRowIndex();
			grid[x, y] = null;
		}

		private void moveOnToTheNextCell() {
			int x = getCurrentCellIndex();
			int value = x + 1;
			setCurrentCellIndex(value);
		}

		private void moveOnToTheNextRow() {
			int y = getCurrentRowIndex();
			int value = y + 1;
			setCurrentRowIndex(value);
		}

		public void addBrickToNewPositionInGrid() {
			resetCurrentBlockIndex();
			addBlocksToGrid();
		}

		private void addBlocksToGrid() {
			while(thereAreBlocksNeedToBeAdded()) {
				addCurrentBlockToGrid();
			}
		}

		private bool thereAreBlocksNeedToBeAdded() {
			var fallintBrick = getBrick();
			var i = getCurrentBlockIndex();
			var n = fallintBrick.childCount;
			return i < n;
		}

		private int getCurrentBlockIndex() {
			return currentBlockIndex;
		}

		private void addCurrentBlockToGrid() {
			addBlockToGrid();
			moveOnToTheNextBlock();
		}

		private void addBlockToGrid() {
			var block = getCurrentBlockByItsIndex();
			var grid = getGridFromPlayField();
			var fallingBrick = getBrick();
			var playField = fallingBrick.parent;
			var position = block.position;
			var localPosition = playField.InverseTransformPoint(position);
			var localPositionX = localPosition.x;
			var localPositionY = localPosition.y;
			var x = Mathf.RoundToInt(localPositionX);
			var y = Mathf.RoundToInt(localPositionY);
			grid[x, y] = block;
		}

		private Transform getCurrentBlockByItsIndex() {
			var fallingBrick = getBrick();
			var index = getCurrentBlockIndex();
			return fallingBrick.GetChild(index);
		}

		private void moveOnToTheNextBlock() {
			int i = getCurrentBlockIndex();
			int value = i + 1;
			setCurrentBlockIndex(value);
		}
	}
}