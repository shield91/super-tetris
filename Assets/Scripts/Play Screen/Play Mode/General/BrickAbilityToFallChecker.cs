using UnityEngine;

namespace AssemblyCSharp
{
	public class BrickAbilityToFallChecker {
		private Transform brick;

		public BrickAbilityToFallChecker(Transform activeBrick) {
			var value = activeBrick;
			setBrick(value);
		}

		private void setBrick(Transform value) {
			brick = value;
		}

		public void checkBrickAbilityToContinueFall() {
			moveBrickDownwardsByOneUnit();
			processResultOfMovingDownwards();
			moveBrickBack();
		}
		
		private void moveBrickDownwardsByOneUnit() {
			var activeBrick = getBrick();
			activeBrick.localPosition += Vector3.down;
		}

		private Transform getBrick() {
			return brick;
		}
		
		private void processResultOfMovingDownwards() {
			if (brickFallIsPossible()) {
				allowFallingBrickToContinueFall();
			}
		}
		
		private bool brickFallIsPossible() {
			var fallingBrick = getBrick();
			var validator = new BrickPositionValidator(fallingBrick);
			return validator.brickPositionIsValid();
		}
		
		private void allowFallingBrickToContinueFall() {
			if (brickIsMotionless()) {
				tellBrickToContinueFall();
			}
		}
		
		private bool brickIsMotionless() {
			var b = getBrickScriptFromBrick();
			return b.isMotionless();
		}

		private Brick getBrickScriptFromBrick() {
			var activeBrick = getBrick();
			return activeBrick.GetComponent<Brick>();
		}
		
		private void tellBrickToContinueFall() {
			allowBrickToContinueFall();
			stopProcessingBrickLanding();
		}
		
		private void allowBrickToContinueFall() {
			const bool VALUE = false;
			var b = getBrickScriptFromBrick();
			b.setMotion(VALUE);
		}
		
		private void stopProcessingBrickLanding() {
			var b = getBrickScriptFromBrick();
			b.StopAllCoroutines();
		}
		
		private void moveBrickBack() {
			var activeBrick = getBrick();
			activeBrick.localPosition += Vector3.up;
		}
	}
}