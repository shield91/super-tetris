using UnityEngine;

namespace AssemblyCSharp
{
	public class BrickPositionValidator {
		private Transform brick;
		private bool brickPositionIsAcceptable;
		private int currentBlockIndex;

		public BrickPositionValidator(Transform fallingBrick) {
			setBrick(fallingBrick);
			resetCalculationData();
		}

		private void setBrick(Transform value) {
			brick = value;
		}

		private void resetCalculationData() {
			resetBrickPositionValidity();
			resetCurrentBlockIndex();
		}

		private void resetBrickPositionValidity() {
			const bool VALUE = true;
			setBrickPositionValidity(VALUE);
		}
		
		private void setBrickPositionValidity(bool value) {
			brickPositionIsAcceptable = value;
		}

		private void resetCurrentBlockIndex() {
			const int VALUE = 0;
			setCurrentBlockIndex(VALUE);
		}

		private void setCurrentBlockIndex(int value) {
			currentBlockIndex = value;
		}

		public bool brickPositionIsValid() {
			resetCalculationData();
			calculateBrickPositionValidity();
			return getResultOfCalculations();
		}

		private void calculateBrickPositionValidity() {
			while (needInCalculationsIsStillPresent()) {
				analyzeCurrentBlockPosition();
			}
		}

		private bool needInCalculationsIsStillPresent() {
			bool ba = thereAreBlocksNeedToBeAnalyzed();
			bool bb = brickPositionRemainsValid();
			return ba && bb;
		}

		private bool thereAreBlocksNeedToBeAnalyzed() {
			int i = getCurrentBlockIndex();
			int n = getBlocksQuantity();
			return i < n;
		}

		private int getCurrentBlockIndex() {
			return currentBlockIndex;
		}

		private int getBlocksQuantity() {
			var minosset = getBrick();
			return minosset.childCount;
		}

		private Transform getBrick() {
			return brick;
		}

		private bool brickPositionRemainsValid() {
			return brickPositionIsAcceptable;
		}

		private void analyzeCurrentBlockPosition() {
			if (currentBlockIsInsidePlayField()) {
				goToNextPhase();
			} else {
				endAnalysis();
			}
		}

		private bool currentBlockIsInsidePlayField() {
			bool ba = currentBlockIsHorizontallyInsidePlayField();
			bool bb = currentBlockIsVerticallyInsidePlayField();
			return ba && bb;
		}

		private bool currentBlockIsHorizontallyInsidePlayField() {
			var x = getHorizontalPositionOfCurrentBlock();
			var w = getWidthOfGrid();
			var ba = x >= 0;
			var bb = x < w;
			return ba && bb;
		}

		private int getHorizontalPositionOfCurrentBlock() {
			var position = getPositionOfCurrentBlock();
			var x = position.x;
			return Mathf.RoundToInt(x);
		}

		private Vector3 getPositionOfCurrentBlock() {
			var block = getCurrentBlockByItsIndex();
			var blockWorldPosition = block.position;
			var minosset = getBrick();
			var playField = minosset.parent;
			return playField.InverseTransformPoint(blockWorldPosition);
		}

		private Transform getCurrentBlockByItsIndex() {
			var minosset = getBrick();
			var index = getCurrentBlockIndex();
			return minosset.GetChild(index);
		}

		private int getWidthOfGrid() {
			var pf = getPlayFieldScriptFromBrick();
			return pf.getGridWidth();
		}

		private PlayField getPlayFieldScriptFromBrick() {
			var minosset = getBrick();
			return minosset.GetComponentInParent<PlayField>();
		}

		private bool currentBlockIsVerticallyInsidePlayField() {
			var y = getVerticalPositionOfCurrentBlock();
			return y >= 0;
		}

		private int getVerticalPositionOfCurrentBlock() {
			var position = getPositionOfCurrentBlock();
			var y = position.y;
			return Mathf.RoundToInt(y);
		}

		private void goToNextPhase() {
			if (collisionWithAnotherBrickIsAbsent()) {
				moveOnToTheNextBlock();
			} else {
				endAnalysis();
			}
		}

		private bool collisionWithAnotherBrickIsAbsent() {
			if (gridCellForCurrentBlockIsOccupied()) {
				return occupiedGridCellBelongsToAnotherPartOfSameBrick();
			} else {
				return gridCellIsFree();
			}
		}

		private bool gridCellForCurrentBlockIsOccupied() {
			var cell = getGridCellForCurrentBlock();
			return cell != null;
		}

		private Transform getGridCellForCurrentBlock() {
			var grid = getGridFromPlayField();
			var x = getHorizontalPositionOfCurrentBlock();
			var y = getVerticalPositionOfCurrentBlock();
			return grid[x, y];
		}

		private Transform[,] getGridFromPlayField() {
			var pf = getPlayFieldScriptFromBrick();
			return pf.getGrid();
		}

		private bool occupiedGridCellBelongsToAnotherPartOfSameBrick() {
			var minosset = getBrick();
			var cell = getGridCellForCurrentBlock();
			var blockParent = cell.parent;
			return blockParent == minosset;
		}

		private bool gridCellIsFree() {
			return !gridCellForCurrentBlockIsOccupied();
		}

		private void moveOnToTheNextBlock() {
			int i = getCurrentBlockIndex();
			int value = i + 1;
			setCurrentBlockIndex(value);
		}

		private void endAnalysis() {
			const bool VALUE = false;
			setBrickPositionValidity(VALUE);
		}

		private bool getResultOfCalculations() {
			return brickPositionRemainsValid();
		}

		public bool brickIsVerticallyInsidePlayField() {
			resetCalculationData();
			calculateCertaintyOfBrickBeingVerticallyInsidePlayField();
			return getResultOfCalculations();
		}

		private void calculateCertaintyOfBrickBeingVerticallyInsidePlayField() {
			while (needInCalculationsIsStillPresent()) {
				analyzeIfCurrentBlockIsVerticallyInsidePlayField();
			}
		}

		private void analyzeIfCurrentBlockIsVerticallyInsidePlayField() {
			if (currentBlockIsVerticallyInsidePlayField()) {
				moveOnToTheNextBlock();
			} else {
				endAnalysis();
			}
		}

		public bool brickIsAtMostNearTheLeftWall() {
			resetCalculationData();
			calculateCertaintyOfBrickBeingAtMostNearTheLeftWall();
			return getResultOfCalculations();
		}

		private void calculateCertaintyOfBrickBeingAtMostNearTheLeftWall() {
			while (needInCalculationsIsStillPresent()) {
				analyzeIfCurrentBlockIsAtMostNearTheLeftWall();
			}
		}

		private void analyzeIfCurrentBlockIsAtMostNearTheLeftWall() {
			if (currentBlockIsAtMostNearTheLeftWall()) {
				moveOnToTheNextBlock();
			} else {
				endAnalysis();
			}
		}

		private bool currentBlockIsAtMostNearTheLeftWall() {
			var x = getHorizontalPositionOfCurrentBlock();
			return x >= 0;
		}

		public bool brickIsHorizontallyInsidePlayField() {
			resetCalculationData();
			calculateCertaintyOfBrickBeingHorizontallyInsidePlayField();
			return getResultOfCalculations();
		}

		private void calculateCertaintyOfBrickBeingHorizontallyInsidePlayField() {
			while (needInCalculationsIsStillPresent()) {
				analyzeIfCurrentBlockIsHorizontallyInsidePlayField();
			}
		}

		private void analyzeIfCurrentBlockIsHorizontallyInsidePlayField() {
			if (currentBlockIsHorizontallyInsidePlayField()) {
				moveOnToTheNextBlock();
			} else {
				endAnalysis();
			}
		}

		public bool clashBetweenThisAndOtherBricksIsAbsent() {
			resetCalculationData();
			calculateCertaintyOfAbsenceOfClashBetweenThisAndOtherBricks();
			return getResultOfCalculations();
		}

		private void calculateCertaintyOfAbsenceOfClashBetweenThisAndOtherBricks() {
			while (needInCalculationsIsStillPresent()) {
				analyzeIfClashBetweenCurrentBlockAndOtherBrickIsAbsent();
			}
		}

		private void analyzeIfClashBetweenCurrentBlockAndOtherBrickIsAbsent() {
			if (currentBlockIsInsidePlayField()) {
				goToNextPhase();
			} else {
				moveOnToTheNextBlock();
			}
		}
	}
}