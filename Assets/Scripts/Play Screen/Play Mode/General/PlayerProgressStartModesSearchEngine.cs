using UnityEngine;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace AssemblyCSharp {
	public class PlayerProgressStartModesSearchEngine {
		private bool chosenStartModeIsFound;
		private int currentStartModeIndex;

		public PlayerProgressStartModesSearchEngine() {
			resetCalculationData();
		}

		private void resetCalculationData() {
			resetDataAboutChosenStartMode();
			resetCurrentStartModeIndex();
		}

		private void resetDataAboutChosenStartMode() {
			const bool VALUE = false;
			setDataAboutChosenStartMode(VALUE);
		}

		private void setDataAboutChosenStartMode(bool value) {
			chosenStartModeIsFound = value;
		}

		private void resetCurrentStartModeIndex() {
			const int VALUE = 0;
			setCurrentStartModeIndex(VALUE);
		}

		private void setCurrentStartModeIndex(int value) {
			currentStartModeIndex = value;
		}

		public StartMode getChosenStartMode() {
			resetCalculationData();
			searchForChosenStartMode();
			return getResultOfSearch();
		}

		private void searchForChosenStartMode() {
			while (needInSearchIsStillPresent()) {
				continueSearch();
			}
		}

		private bool needInSearchIsStillPresent() {
			bool ba = thereAreStartModesNeedToBeAnalyzed();
			bool bb = chosenStartModeIsStillWanted();
			return ba && bb;
		}

		private bool thereAreStartModesNeedToBeAnalyzed() {
			int n = getStartModesQuantity();
			int i = getCurrentStartModeIndex();
			return i < n;
		}

		private int getStartModesQuantity() {
			var startModes = getStartModes();
			return startModes.Count;
		}

		private List<StartMode> getStartModes() {
			var playerProgress = getPlayerProgress();
			return playerProgress.startModes;
		}

		private PlayerProgress getPlayerProgress() {
			var serializer = getSerializerForPlayerProgress();
			var stream = getStreamForPlayerProgress();
			var playerProgress = serializer.Deserialize(stream) as PlayerProgress;
			stream.Close();
			return playerProgress;
		}

		private XmlSerializer getSerializerForPlayerProgress() {
			var type = typeof(PlayerProgress);
			return new XmlSerializer(type);
		}
		
		private FileStream getStreamForPlayerProgress() {
			var path = getPathToPlayerProgress();
			var mode = FileMode.OpenOrCreate;
			return new FileStream(path, mode);
		}

		private string getPathToPlayerProgress() {
			const string KEY = "Path To Player Progress";
			return PlayerPrefs.GetString(KEY);
		}

		private int getCurrentStartModeIndex() {
			return currentStartModeIndex;
		}

		private bool chosenStartModeIsStillWanted() {
			return !chosenStartModeIsDetected();
		}

		private bool chosenStartModeIsDetected() {
			return chosenStartModeIsFound;
		}

		private void continueSearch() {
			if (currentStartModeIsChosenOne()) {
				stopSearch();
			} else {
				moveOnToTheNextStartMode();
			}
		}

		private bool currentStartModeIsChosenOne() {
			const string KEY = "Chosen Start Mode";
			var chosenStartModeName = PlayerPrefs.GetString(KEY);
			var currentStartMode = getCurrentStartMode();
			var value = currentStartMode.name;
			return chosenStartModeName.Equals(value);
		}

		private StartMode getCurrentStartMode() {
			var startModes = getStartModes();
			var index = getCurrentStartModeIndex();
			return startModes[index];
		}

		private void stopSearch() {
			const bool VALUE = true;
			setDataAboutChosenStartMode(VALUE);
		}

		private void moveOnToTheNextStartMode() {
			int index = getCurrentStartModeIndex();
			int value = index + 1;
			setCurrentStartModeIndex(value);
		}

		private StartMode getResultOfSearch() {
			return getCurrentStartMode();
		}

		public int getChosenStartModeIndex() {
			resetCalculationData();
			searchForChosenStartMode();
			return getSearchResult();
		}

		private int getSearchResult() {
			return getCurrentStartModeIndex();
		}
	}
}