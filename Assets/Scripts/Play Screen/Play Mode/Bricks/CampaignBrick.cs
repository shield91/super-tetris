﻿using UnityEngine;

public class CampaignBrick : Brick {
	protected override void processUpdateMethod() {
		if (playModeIsEnabled()) {
			processUpdateMethodForPlayMode();
		}
	}

	private bool playModeIsEnabled() {
		const string KEY = "Level Editor Mode Is Enabled";
		const string VALUE = "";
		string value = PlayerPrefs.GetString(KEY);
		return value.Equals(VALUE);
	}

	private void processUpdateMethodForPlayMode() {
		base.processUpdateMethod();
	}
}