﻿public class BrickZ : HalfRotateableBrick {
	protected override int getReflectionsQuantity() {
		return 2;
	}
}