﻿public class HalfRotateableBrick : RotateableBrick {
	protected override int getRotationsQuantity() {
		int quantity = base.getRotationsQuantity();
		return quantity / 2;
	}
}