﻿public class RotateableBrick : Brick {
	protected override int getRotationsQuantity() {
		return 4;
	}
}