﻿public class BrickJ : RotateableBrick {
	protected override int getReflectionsQuantity() {
		return 2;
	}
	
	public override void rotateAnticlockwise() {
		if (brickIsReflected()) {
			rotateBrickClockwise();
		} else {
			rotateBrickAnticlockwise();
		}
	}

	private bool brickIsReflected() {
		var rotation = transform.rotation;
		var eulerAngles = rotation.eulerAngles;
		var angleY = eulerAngles.y;
		return angleY >= 180f;
	}

	private void rotateBrickClockwise() {
		base.rotateClockwise();
	}

	private void rotateBrickAnticlockwise() {
		base.rotateAnticlockwise();
	}

	public override void rotateClockwise() {
		if (brickIsReflected()) {
			rotateBrickAnticlockwise();
		} else {
			rotateBrickClockwise();
		}
	}
}