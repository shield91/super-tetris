using UnityEngine;
using AssemblyCSharp;

public class Brick : MonoBehaviour {
	private float timeSinceLastFallTick;
	private bool brickIsMotionless;
	private int rotationMultiplier;
	
	private void Awake() {
		initializeFields();
		rotateBrick();
	}

	private void initializeFields() {
		resetTimeSinceLastFallTick();
		resetMotion();
		resetRotationMultiplier();
	}

	private void resetTimeSinceLastFallTick() {
		const float VALUE = 0f;
		setTimeSinceLastFallTick(VALUE);
	}

	public void setTimeSinceLastFallTick(float value) {
		timeSinceLastFallTick = value;
	}

	public void resetMotion() {
		const bool VALUE = false;
		setMotion(VALUE);
	}

	public void setMotion(bool value) {
		brickIsMotionless = value;
	}

	private void resetRotationMultiplier() {
		const int VALUE = 0;
		setRotationMultiplier(VALUE);
	}

	private void setRotationMultiplier(int value) {
		rotationMultiplier = value;
	}

	private void rotateBrick() {
		const float X = 0f;
		const float Z = 0f;
		var y = getReflectionForBrick();
		var rotation = transform.rotation;
		rotation.eulerAngles = new Vector3(X, y, Z);
		transform.rotation = rotation;
	}

	private int getReflectionForBrick() {
		const int COEFFICIENT = 180;
		int multiplier = getMultiplierForReflection();
		return COEFFICIENT * multiplier;
	}
	
	private int getMultiplierForReflection() {
		const int MIN = 0;
		int max = getReflectionsQuantity();
		return Random.Range(MIN, max);
	}
	
	protected virtual int getReflectionsQuantity() {
		return 1;
	}

	private void Update() {
		processUpdateMethod();
	}

	protected virtual void processUpdateMethod() {
		if (brickIsFalling()) {
			processBrickFall();
		} else if (allBlocksAreDestroyed()) {
			destroyThisBrick();
		}
	}

	private bool brickIsFalling() {
		const string VALUE = "Falling Brick";
		return name.Equals(VALUE);
	}

	private void processBrickFall() {
		if (gameIsCompleted()) {
			destroyThisBrick();
		} else if (itIsTimeToFallFurther()) {
			continueFall();
		}
	}

	private bool gameIsCompleted() {
		var playScreen = GetComponentInParent<PlayScreen>();
		return playScreen.gameIsAccomplished();
	}

	private void destroyThisBrick() {
		var obj = gameObject;
		Destroy(obj);
	}

	private bool itIsTimeToFallFurther() {
		float startTime = getTimeSinceLastFallTick();
		float endTime = getCurrentMomentInTime();
		float delayTime = getDelayTime();
		return endTime - startTime >= delayTime;
	}

	private float getTimeSinceLastFallTick() {
		return timeSinceLastFallTick;
	}

	private float getCurrentMomentInTime() {
		return Time.time;
	}

	private float getDelayTime() {
		const float F = 2f;
		var playScreen = GetComponentInParent<PlayScreen>();
		var speed = playScreen.getBrickSpeed();
		return F / speed;
	}

	private void continueFall() {
		var activeBrick = transform;
		var operation = new MoveDown(activeBrick);
		operation.apply();
	}

	private bool allBlocksAreDestroyed() {
		int n = transform.childCount;
		return n == 0;
	}

	public bool isMotionless() {
		return brickIsMotionless;
	}
	
	public virtual void rotateAnticlockwise() {
		moveOnToTheNextRotationMultiplier();
		rotate();
	}

	private void moveOnToTheNextRotationMultiplier() {
		int multiplier = getRotationMultiplier();
		int i = multiplier + 1;
		int n = getRotationsQuantity();
		int value = i % n;
		setRotationMultiplier(value);
	}

	private int getRotationMultiplier() {
		return rotationMultiplier;
	}

	protected virtual int getRotationsQuantity() {
		return 1;
	}

	private void rotate() {
		var rotation = transform.rotation;
		var eulerAngles = rotation.eulerAngles;
		var x = eulerAngles.x;
		var y = eulerAngles.y;
		var z = getRotationForBrick();
		rotation.eulerAngles = new Vector3(x, y, z);
		transform.rotation = rotation;
	}

	private int getRotationForBrick() {
		const int COEFFICIENT = 90;
		int multiplier = getRotationMultiplier();
		return COEFFICIENT * multiplier;
	}
	
	public virtual void rotateClockwise() {
		moveOnToThePreviousRotationMultiplier();
		rotate();
	}

	private void moveOnToThePreviousRotationMultiplier() {
		int multiplier = getRotationMultiplier();
		int i = multiplier - 1;
		int n = getRotationsQuantity();
		int j = i + n;
		int value = j % n;
		setRotationMultiplier(value);
	}
}