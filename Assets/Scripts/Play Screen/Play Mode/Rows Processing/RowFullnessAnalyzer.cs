using UnityEngine;

namespace AssemblyCSharp
{
	public class RowFullnessAnalyzer {
		private RowsProcessor requester;
		private int currentCellIndex;
		private bool rowIsComplete;

		public RowFullnessAnalyzer(RowsProcessor suppliant) {
			requester = suppliant;
			currentCellIndex = 0;
			rowIsComplete = true;
		}

		public bool rowIsFull() {
			resetCurrentCellIndex();
			resetDataAboutRow();
			calculateRowFullness();
			return getCalculatedResult();
		}

		private void resetCurrentCellIndex() {
			currentCellIndex = 0;
		}

		private void resetDataAboutRow() {
			rowIsComplete = true;
		}

		private void calculateRowFullness() {
			while(needInCalculationIsStillPresent()) {
				analyzeCurrentCell();
			}
		}

		private bool needInCalculationIsStillPresent() {
			bool ba = thereAreCellsNeedToBeAnalyzed();
			bool bb = rowRemainsFull();
			return ba && bb;
		}

		private bool thereAreCellsNeedToBeAnalyzed() {
			int x = currentCellIndex;
			int w = getWidthOfGrid();
			return x < w;
		}

		private int getWidthOfGrid() {
			var pf = getPlayFieldFromLandedBrick();
			return pf.getGridWidth();
		}

		private PlayField getPlayFieldFromLandedBrick() {
			var landedBrick = requester.getLandedBrick();
			return landedBrick.GetComponentInParent<PlayField>();
		}

		private bool rowRemainsFull() {
			return rowIsComplete;
		}

		private void analyzeCurrentCell() {
			if (currentCellIsEmpty()) {
				endAnalysis();
			} else {
				moveOnToTheNextCell();
			}
		}

		private bool currentCellIsEmpty() {
			var cell = getCurrentCellFromPlayField();
			return cell == null;
		}

		private Transform getCurrentCellFromPlayField() {
			var pf = getPlayFieldFromLandedBrick();
			var grid = pf.getGrid();
			var x = currentCellIndex;
			var y = requester.getCurrentRowIndex();
			return grid[x, y];
		}

		private void endAnalysis() {
			rowIsComplete = false;
		}

		private void moveOnToTheNextCell() {
			++currentCellIndex;
		}

		private bool getCalculatedResult() {
			return rowIsComplete;
		}
	}
}