using UnityEngine;

namespace AssemblyCSharp
{
	public class RowsDecreaser {
		private RowsProcessor requester;
		private int currentRowIndex;
		private int currentCellIndex;

		public RowsDecreaser(RowsProcessor suppliant) {
			requester = suppliant;
			currentRowIndex = getStartRowIndexFromRequester();
			currentCellIndex = 0;
		}

		private int getStartRowIndexFromRequester() {
			int rowIndex = requester.getCurrentRowIndex();
			return rowIndex + 1;
		}

		public void decreaseRows() {
			resetCurrentRowIndex();
			decreaseRowsAboveStartOne();
		}

		private void resetCurrentRowIndex() {
			currentRowIndex = getStartRowIndexFromRequester();
		}

		private void decreaseRowsAboveStartOne() {
			while (thereAreRowsNeedToBeDecreased()) {
				processCurrentRow();
			}
		}

		private bool thereAreRowsNeedToBeDecreased() {
			int i = currentRowIndex;
			int h = getHeightOfGrid();
			return i < h;
		}

		private int getHeightOfGrid() {
			var pf = getPlayFieldFromLandedBrick();
			return pf.getGridHeight();
		}

		private PlayField getPlayFieldFromLandedBrick() {
			var landedBrick = requester.getLandedBrick();
			return landedBrick.GetComponentInParent<PlayField>();
		}

		private void processCurrentRow() {
			resetCurrentCellIndex();
			decreaseCurrentRow();
			moveOnToTheNextRow();
		}

		private void resetCurrentCellIndex() {
			currentCellIndex = 0;
		}

		private void decreaseCurrentRow() {
			while (thereAreCellsNeedToBeProcessed()) {
				processCurrentCell();
			}
		}

		private bool thereAreCellsNeedToBeProcessed() {
			int x = currentCellIndex;
			int w = getWidthOfGrid();
			return x < w;
		}

		private int getWidthOfGrid() {
			var pf = getPlayFieldFromLandedBrick();
			return pf.getGridWidth();
		}

		private void processCurrentCell() {
			startFirstPhase();
			moveOnToTheNextCell();
		}

		private void startFirstPhase() {
			if (currentCellIsOccupied()) {
				goToSecondPhase();
			}
		}

		private bool currentCellIsOccupied() {
			var grid = getGridFromPlayField();
			int x = currentCellIndex;
			int y = currentRowIndex;
			return grid[x, y];
		}

		private Transform[,] getGridFromPlayField() {
			var pf = getPlayFieldFromLandedBrick();
			return pf.getGrid();
		}

		private void goToSecondPhase() {
			moveCurrentCellTowardsBottom();
			updateBlockPosition();
		}

		private void moveCurrentCellTowardsBottom() {
			var grid = getGridFromPlayField();
			var x = currentCellIndex;
			var y = currentRowIndex;
			grid[x, y - 1] = grid[x, y];
			grid[x, y] = null;
		}

		private void updateBlockPosition() {
			var grid = getGridFromPlayField();
			var x = currentCellIndex;
			var y = currentRowIndex;
			grid[x, y - 1].position += Vector3.down;
		}

		private void moveOnToTheNextCell() {
			++currentCellIndex;
		}

		private void moveOnToTheNextRow() {
			++currentRowIndex;
		}
	}
}