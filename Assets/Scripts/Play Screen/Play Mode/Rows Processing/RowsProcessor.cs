using UnityEngine;

namespace AssemblyCSharp
{
	public class RowsProcessor {
		private Transform landedBrick;
		private int currentRowIndex;
		private int coinsIncrement;

		public RowsProcessor(Transform requester) {
			setLandedBrick(requester);
			resetCurrentRowIndex();
			resetCoinsIncrement();
		}

		private void setLandedBrick(Transform value) {
			landedBrick = value;
		}

		private void resetCurrentRowIndex() {
			const int VALUE = 0;
			setCurrentRowIndex(VALUE);
		}

		private void setCurrentRowIndex(int value) {
			currentRowIndex = value;
		}

		private void resetCoinsIncrement() {
			const int VALUE = 0;
			setCoinsIncrement(VALUE);
		}

		private void setCoinsIncrement(int value) {
			coinsIncrement = value;
		}

		public void process() {
			resetCurrentRowIndex();
			resetCoinsIncrement();
			processRows();
			increaseCoinsByCoinsIncrement();
		}

		private void processRows() {
			while(thereAreRowsNeedToBeProcessed()) {
				processCurrentRow();
			}
		}

		private bool thereAreRowsNeedToBeProcessed() {
			int y = getCurrentRowIndex();
			int h = getHeightOfGrid();
			return y < h;
		}

		public int getCurrentRowIndex() {
			return currentRowIndex;
		}

		private int getHeightOfGrid() {
			var requester = getLandedBrick();
			var pf = requester.GetComponentInParent<PlayField>();
			return pf.getGridHeight();
		}

		public Transform getLandedBrick() {
			return landedBrick;
		}

		private void processCurrentRow() {
			if (currentRowIsFull()) {
				processFullRow();
			} else {
				moveOnToTheNextRow();
			}
		}

		private bool currentRowIsFull() {
			var requester = this;
			var analyzer = new RowFullnessAnalyzer(requester);
			return analyzer.rowIsFull();
		}

		private void processFullRow() {
			deleteCurrentRow();
			decreaseRowsAboveCurrentOne();
			updateCoinsIncrement();
		}

		private void deleteCurrentRow() {
			var requester = this;
			var destroyer = new RowDestroyer(requester);
			destroyer.destroyRow();
		}

		private void decreaseRowsAboveCurrentOne() {
			var requester = this;
			var decreaser = new RowsDecreaser(requester);
			decreaser.decreaseRows();
		}

		private void updateCoinsIncrement() {
			const int MULTIPLIER = 1;
			const int ADDEND = 15;
			int oldValue = getCoinsIncrement();
			int value = MULTIPLIER * oldValue + ADDEND;
			setCoinsIncrement(value);
		}

		private int getCoinsIncrement() {
			return coinsIncrement;
		}

		private void moveOnToTheNextRow() {
			int index = getCurrentRowIndex();
			int value = index + 1;
			setCurrentRowIndex(value);
		}

		private void increaseCoinsByCoinsIncrement() {
			const string NAME = "Header/Coins/Value";
			var requester = getLandedBrick();
			var ps = requester.GetComponentInParent<PlayScreen>();
			var gameWindow = ps.transform;
			var coinsValueTransform = gameWindow.Find(NAME);
			var tm = coinsValueTransform.GetComponent<TextMesh>();
			var s = tm.text;
			var increment = getCoinsIncrement();
			var oldValue = int.Parse(s);
			var newValue = oldValue + increment;
			tm.text = newValue.ToString();
		}
	}
}