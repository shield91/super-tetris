using UnityEngine;

namespace AssemblyCSharp
{
	public class RowDestroyer {
		private RowsProcessor requester;
		private int currentCellIndex;

		public RowDestroyer(RowsProcessor suppliant) {
			requester = suppliant;
			currentCellIndex = 0;
		}

		public void destroyRow() {
			resetCurrentCellIndex();
			destroyCellsInRow();
		}

		private void resetCurrentCellIndex() {
			currentCellIndex = 0;
		}

		private void destroyCellsInRow(){
			while (thereAreCellsNeedToBeDestroyed()) {
				processCurrentCell();
			}
		}

		private bool thereAreCellsNeedToBeDestroyed() {
			int x = currentCellIndex;
			int w = getWidthOfGrid();
			return x < w;
		}

		private int getWidthOfGrid() {
			var pf = getPlayFieldFromLandedBrick();
			return pf.getGridWidth();
		}
		
		private PlayField getPlayFieldFromLandedBrick() {
			var landedBrick = requester.getLandedBrick();
			return landedBrick.GetComponentInParent<PlayField>();
		}

		private void processCurrentCell() {
			destroyCurrentCellContent();
			clearCurrentCell();
			moveOnToTheNextCell();
		}

		private void destroyCurrentCellContent() {
			var grid = getGridFromPlayField();
			var x = currentCellIndex;
			var y = requester.getCurrentRowIndex();
			var cell = grid[x, y];
			var obj = cell.gameObject;
			Object.Destroy(obj);
		}

		private Transform[,] getGridFromPlayField() {
			var pf = getPlayFieldFromLandedBrick();
			return pf.getGrid();
		}

		private void clearCurrentCell() {
			var grid = getGridFromPlayField();
			var x = currentCellIndex;
			var y = requester.getCurrentRowIndex();
			grid[x, y] = null;
		}

		private void moveOnToTheNextCell() {
			++currentCellIndex;
		}
	}
}