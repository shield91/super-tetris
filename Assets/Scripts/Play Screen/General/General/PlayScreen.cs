﻿using UnityEngine;
using AssemblyCSharp;
using System.IO;
using System.Xml.Serialization;

public class PlayScreen : MonoBehaviour {
	private bool gameIsCompleted;
	private int gameTime;
	private int speed;

	private void Start() {
		resetDataAboutPreviousScreenName();
		resetDataAboutGameCompletion();
		resetGameTime();
		resetSpeed();
		scaleThisScreen();
		setUpThisScreen();
	}

	private void resetDataAboutPreviousScreenName() {
		const string KEY = "Previous Screen Name";
		PlayerPrefs.DeleteKey(KEY);
	}

	private void resetDataAboutGameCompletion() {
		const bool VALUE = false;
		setGameComplete(VALUE);
	}
	
	public void setGameComplete(bool value) {
		gameIsCompleted = value;
	}

	private void resetGameTime() {
		const int VALUE = 0;
		setGameTime(VALUE);
	}

	private void setGameTime(int value) {
		gameTime = value;
	}

	private void resetSpeed() {
		if (gameIsInSpecialMode()) {
			setSpeedForGameInSpecialMode();
		} else {
			setSpeedForGameInOrdinaryMode();
		}
	}

	private bool gameIsInSpecialMode() {
		bool ba = itIsCampaignLevel();
		bool bb = gameIsInFreeMode();
		return ba || bb;
	}

	public bool itIsCampaignLevel() {
		const string KEY = "Chosen Level";
		return valueIsAbsentFor(KEY);
	}

	private bool valueIsAbsentFor(string key) {
		const string VALUE = "";
		string value = PlayerPrefs.GetString(key);
		return !value.Equals(VALUE);
	}

	private bool gameIsInFreeMode() {
		const string KEY = "Chosen Start Mode";
		const string VALUE = "Free";
		string value = PlayerPrefs.GetString(KEY);
		return value.Equals(VALUE);
	}

	private void setSpeedForGameInSpecialMode() {
		const int VALUE = 1;
		setSpeed(VALUE);
	}

	private void setSpeed(int value) {
		speed = value;
	}

	private void setSpeedForGameInOrdinaryMode() {
		var chosenStartMode = getChosenStartMode();
		var value = chosenStartMode.speed;
		setSpeed(value);
	}
	
	private StartMode getChosenStartMode() {
		var engine = new PlayerProgressStartModesSearchEngine();
		return engine.getChosenStartMode();
	}

	private void scaleThisScreen() {
		var oldScale = transform.localScale;
		var x = getScaleMultiplier();
		var y = oldScale.y;
		var z = oldScale.z;
		var newScale = new Vector3(x, y, z);
		transform.localScale = newScale;
	}

	private float getScaleMultiplier() {
		const float TARGET_WIDTH = 183f;
		const float TARGET_HEIGHT = 326f;
		const float TARGET_RATIO = TARGET_WIDTH / TARGET_HEIGHT;
		float actualRatio = getActualRatio();
		return actualRatio / TARGET_RATIO;
	}

	private float getActualRatio() {
		float actualWidth = Screen.width;
		float actualHeight = Screen.height;
		return actualWidth / actualHeight;
	}

	private void setUpThisScreen() {
		if (levelEditorModeIsEnabled()) {
			setUpThisScreenInLevelEditorMode();
		} else {
			setUpThisScreenInPlayMode();
		}
	}

	public bool levelEditorModeIsEnabled() {
		const string KEY = "Level Editor Mode Is Enabled";
		return valueIsAbsentFor(KEY);
	}

	private void setUpThisScreenInLevelEditorMode() {
		hideHeader();
		hideFooter();
		showLevelEditorFooter();
		enableCampaignLevelScreenScript();
	}

	private void hideHeader() {
		const string NAME = "Header";
		hide(NAME);
	}

	private void hide(string name) {
		const bool VALUE = false;
		var child = transform.Find(name);
		var go = child.gameObject;
		go.SetActive(VALUE);
	}

	private void hideFooter() {
		const string NAME = "Footer";
		hide(NAME);
	}

	private void showLevelEditorFooter() {
		const string NAME = "Level Editor Footer";
		const bool VALUE = true;
		var child = transform.Find(NAME);
		var go = child.gameObject;
		go.SetActive(VALUE);
	}

	private void enableCampaignLevelScreenScript() {
		var cls = GetComponent<CampaignLevelScreen>();
		cls.enabled = true;
	}

	private void setUpThisScreenInPlayMode() {
		if (itIsCampaignLevel()) {
			setUpPlayScreenForCampaignLevel();
		} else {
			setUpPlayScreenForChosenStartMode();
		}
	}

	private void setUpPlayScreenForCampaignLevel() {
		enableCampaignLevelScreenScript();
		updateTimesPlayedForCampaignLevel();
	}

	private void updateTimesPlayedForCampaignLevel() {
		const int VALUE = 1;
		var o = getPlayerProgress();
		var campaigns = o.campaigns;
		var chosenCampaignIndex = getChosenCampaignIndex();
		var chosenCampaign = campaigns[chosenCampaignIndex];
		var chosenCampaignLevels = chosenCampaign.levels;
		var chosenLevelIndex = getChosenLevelIndex();
		var chosenLevel = chosenCampaignLevels[chosenLevelIndex];
		var serializer = getSerializerForPlayerProgress();
		var stream = getStreamForPlayerProgress();
		chosenLevel.timesPlayed += VALUE;
		serializer.Serialize(stream, o);
		stream.Close();
	}

	private PlayerProgress getPlayerProgress() {
		var serializer = getSerializerForPlayerProgress();
		var stream = getStreamForPlayerProgress();
		var playerProgress = serializer.Deserialize(stream) as PlayerProgress;
		stream.Close();
		return playerProgress;
	}

	private XmlSerializer getSerializerForPlayerProgress() {
		var type = typeof(PlayerProgress);
		return new XmlSerializer(type);
	}

	private FileStream getStreamForPlayerProgress() {
		var path = getPathToPlayerProgress();
		var mode = FileMode.OpenOrCreate;
		return new FileStream(path, mode);
	}

	private string getPathToPlayerProgress() {
		const string KEY = "Path To Player Progress";
		return PlayerPrefs.GetString(KEY);
	}

	private int getChosenCampaignIndex() {
		var engine = new PlayerProgressCampaignsSearchEngine();
		return engine.getChosenCampaignIndex();
	}

	private int getChosenLevelIndex() {
		var xmlForSelectedLevel = getXmlForChosenLevel();
		var engine = new PlayerProgressChosenCampaignLevelsSearchEngine(xmlForSelectedLevel);
		return engine.getChosenLevelIndex();
	}

	private string getXmlForChosenLevel() {
		const string KEY = "Chosen Level";
		return PlayerPrefs.GetString(KEY);
	}

	private void setUpPlayScreenForChosenStartMode() {
		updateTimesPlayedForChosenStartMode();
		launchFirstFallingBricks();
	}

	private void updateTimesPlayedForChosenStartMode() {
		const int VALUE = 1;
		var serializer = getSerializerForPlayerProgress();
		var o = getPlayerProgress();
		var startModes = o.startModes;
		var index = getChosenStartModeIndex();
		var chosenMode = startModes[index];
		var stream = getStreamForPlayerProgress();
		chosenMode.timesPlayed += VALUE;
		serializer.Serialize(stream, o);
		stream.Close();
	}

	private int getChosenStartModeIndex() {
		var engine = new PlayerProgressStartModesSearchEngine();
		return engine.getChosenStartModeIndex();
	}

	private void launchFirstFallingBricks() {
		var playWindow = transform;
		var launcher = new FirstFallingBricksLauncher(playWindow);
		launcher.launchBricks();
	}

	private void Update() {
		updateGameTime();
		updateTimeInMinutesAndSeconds();
		updateFallingBricksSpeed();
	}

	private void updateGameTime() {
		var time = Time.timeSinceLevelLoad;
		var value = (int) time;
		setGameTime(value);
	}

	private void updateTimeInMinutesAndSeconds() {
		var suppliant = this;
		var updater = new TimeInMinutesAndSecondsUpdater(suppliant);
		updater.updateTime();
	}

	public int getGameTime() {
		return gameTime;
	}

	private void updateFallingBricksSpeed() {
		if (itIsCampaignLevel()) {
			updateFallingBricksSpeedForCampaignLevel();
		} else if (gameIsInFreeMode()) {
			updateFallingBricksSpeedForGameInFreeMode();
		}
	}

	private void updateFallingBricksSpeedForCampaignLevel() {
		int value = getSpeedForCampaignLevel();
		setSpeed(value);
	}

	private int getSpeedForCampaignLevel() {
		const int MIN = 1;
		const int MAX = 5;
		int velocity = getOrdinarySpeed();
		int value = velocity + 1;
		return Mathf.Clamp(value, MIN, MAX);
	}

	private int getOrdinarySpeed() {
		const int SPEED_DURATION = 30;
		int time = getGameTime();
		return time / SPEED_DURATION;
	}

	private void updateFallingBricksSpeedForGameInFreeMode() {
		int value = getSpeedForGameInFreeMode();
		setSpeed(value);
	}

	private int getSpeedForGameInFreeMode() {
		const float LENGTH = 5f;
		var t = getOrdinarySpeed();
		var processedSpeed = Mathf.Repeat(t, LENGTH);
		var speedIntegerRepresentation = (int) processedSpeed;
		return speedIntegerRepresentation + 1;
	}

	public int getBrickSpeed() {
		return speed;
	}

	public bool gameIsAccomplished() {
		return gameIsCompleted;
	}
}