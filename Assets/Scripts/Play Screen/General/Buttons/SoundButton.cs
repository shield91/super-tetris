﻿using UnityEngine;

public class SoundButton : MonoBehaviour {
	public Sprite soundOn;
	public Sprite soundOff;

	private void Start() {
		if (PlayerPrefs.HasKey ("Sound Is Off")) {
			toggleSound();
		}
	}

	private void OnMouseDown() {
		toggleSound();
	}

	public void toggleSound() {
		if (soundIsOn()) {
			turnSoundOff();
		} else {
			turnSoundOn();
		}
	}
	
	private bool soundIsOn() {
		var source = GetComponent<AudioSource>();
		return !source.mute;
	}
	
	private void turnSoundOff() {
		toggleAudio();
		unhighlightButton();

		PlayerPrefs.SetString("Sound Is Off", "Yes");
	}
	
	private void toggleAudio() {
		var source = GetComponent<AudioSource>();
		var mute = source.mute;
		source.mute = !mute;
	}

	private void unhighlightButton() {
		var sr = GetComponent<SpriteRenderer>();
		sr.sprite = soundOff;
	}

	private void turnSoundOn() {
		toggleAudio();
		highlightButton();

		PlayerPrefs.DeleteKey("Sound Is Off");
	}

	private void highlightButton() {
		var sr = GetComponent<SpriteRenderer>();
		sr.sprite = soundOn;
	}
}