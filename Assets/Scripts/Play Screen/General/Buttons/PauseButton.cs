﻿using UnityEngine;

public class PauseButton : MonoBehaviour {
	public Sprite switchIsOn;
	public Sprite switchIsOff;

	private void OnMouseDown() {
		highlightButton();
	}

	private void highlightButton() {
		var sr = GetComponent<SpriteRenderer>();
		sr.sprite = switchIsOn;
	}
	
	private void OnMouseUp() {
		unhighlightButton();
		disableThisButton();
		pauseGame();
		disableMenuButton();
		disableSwitchButton();
		showPausePopUpWindow();
		disableLeftPlayField();
		disableRightPlayField();
	}

	private void unhighlightButton() {
		var sr = GetComponent<SpriteRenderer>();
		sr.sprite = switchIsOff;
	}

	private void disableThisButton() {
		var collider = GetComponent<Collider2D>();
		collider.enabled = false;
	}

	private void pauseGame() {
		Time.timeScale = 0;
	}

	private void disableMenuButton() {
		const string NAME = "Footer/Menu";
		disable(NAME);
	}

	private void disable(string name) {
		var playScreen = GetComponentInParent<PlayScreen>();
		var gameWindow = playScreen.transform;
		var child = gameWindow.Find(name);
		var collider = child.GetComponentInChildren<Collider2D>();
		collider.enabled = false;
	}

	private void disableSwitchButton() {
		const string NAME = "Footer/Switch";
		disable(NAME);
	}

	private void showPausePopUpWindow() {
		const string NAME = "Pause Pop-Up Window";
		const bool VALUE = true;
		var playScreen = GetComponentInParent<PlayScreen>();
		var gameWindow = playScreen.transform;
		var child = gameWindow.Find(NAME);
		var go = child.gameObject;
		go.SetActive(VALUE);
	}

	private void disableLeftPlayField() {
		const string NAME = "Play Fields/Left Play Field/Play Field/Background";
		disable(NAME);
	}

	private void disableRightPlayField() {
		const string NAME = "Play Fields/Right Play Field/Play Field/Background";
		disable(NAME);
	}
}