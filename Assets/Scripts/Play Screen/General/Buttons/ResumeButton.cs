﻿using UnityEngine;

public class ResumeButton : MonoBehaviour {
	public Sprite switchIsOn;
	public Sprite switchIsOff;
	
	private void OnMouseDown() {
		highlightButton();
	}

	private void highlightButton() {
		var sr = GetComponent<SpriteRenderer>();
		sr.sprite = switchIsOn;
	}
	
	private void OnMouseUp() {
		unhighlightButton();
		resumeGame();
		enableMenuButton();
		enableSwitchButton();
		enablePauseButton();
		hideMenuPopUpWindow();
		hidePausePopUpWindow();
		enableLeftPlayField();
		enableRightPlayField();
	}

	private void unhighlightButton() {
		var sr = GetComponent<SpriteRenderer>();
		sr.sprite = switchIsOff;
	}

	private void resumeGame() {
		Time.timeScale = 1;
	}

	private void enableMenuButton() {
		const string NAME = "Footer/Menu";
		enable(NAME);
	}

	private void enable(string name) {
		var playScreen = GetComponentInParent<PlayScreen>();
		var gameWindow = playScreen.transform;
		var child = gameWindow.Find(name);
		var collider = child.GetComponentInChildren<Collider2D>();
		collider.enabled = true;
	}

	private void enableSwitchButton() {
		const string NAME = "Footer/Switch";
		enable(NAME);
	}

	private void enablePauseButton() {
		const string NAME = "Footer/Pause";
		enable(NAME);
	}

	private void hideMenuPopUpWindow() {
		const string NAME = "Menu Pop-Up Window";
		hide(NAME);
	}

	private void hide(string name) {
		const bool VALUE = false;
		var playScreen = GetComponentInParent<PlayScreen>();
		var gameWindow = playScreen.transform;
		var child = gameWindow.Find(name);
		var go = child.gameObject;
		go.SetActive(VALUE);
	}

	private void hidePausePopUpWindow() {
		const string NAME = "Pause Pop-Up Window";
		hide(NAME);
	}

	private void enableLeftPlayField() {
		const string NAME = "Play Fields/Left Play Field/Play Field/Background";
		enable(NAME);
	}

	private void enableRightPlayField() {
		const string NAME = "Play Fields/Right Play Field/Play Field/Background";
		enable(NAME);
	}
}