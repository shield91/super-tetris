﻿using UnityEngine;
using AssemblyCSharp;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

public class NextLevelButton : MonoBehaviour {
	public Sprite switchIsOn;
	public Sprite switchIsOff;

	private void OnMouseDown() {
		highlightButton();
	}
	
	private void highlightButton() {
		var sr = GetComponent<SpriteRenderer>();
		sr.sprite = switchIsOn;
	}
	
	private void OnMouseUp() {
		unhighlightButton();
		setUpNextLevel();
		setNextLevelAsChosenOne();
		reloadThisScreen();
		putGameOutOfPause();
	}
	
	private void unhighlightButton() {
		var sr = GetComponent<SpriteRenderer>();
		sr.sprite = switchIsOff;
	}

	private void setUpNextLevel() {
		if (levelHasNotBeenPurchasedBefore()) {
			addNewLevelToPlayerProgress();
		}
	}

	private bool levelHasNotBeenPurchasedBefore() {
		int i = getChosenLevelIndex();
		int n = getChosenCampaignLevelsCount();
		return i == n;
	}

	private int getChosenLevelIndex() {
		var xmlForSelectedLevel = getXmlForNextLevel();
		var engine = new PlayerProgressChosenCampaignLevelsSearchEngine(xmlForSelectedLevel);
		return engine.getChosenLevelIndex();
	}

	private string getXmlForNextLevel() {
		var level = getNextLevel();
		return level.OuterXml;
	}

	private XmlNode getNextLevel() {
		var cls = GetComponentInParent<CampaignLevelScreen>();
		return cls.getNextLevel();
	}

	private int getChosenCampaignLevelsCount() {
		var playerProgress = getPlayerProgress();
		var campaigns = playerProgress.campaigns;
		var index = getChosenCampaignIndex();
		var chosenCampaign = campaigns[index];
		var chosenCampaignLevels = chosenCampaign.levels;
		return chosenCampaignLevels.Count;
	}

	private PlayerProgress getPlayerProgress() {
		var serializer = getSerializerForPlayerProgress();
		var stream = getStreamForPlayerProgress();
		var playerProgress = serializer.Deserialize(stream) as PlayerProgress;
		stream.Close();
		return playerProgress;
	}
	
	private XmlSerializer getSerializerForPlayerProgress() {
		var type = typeof(PlayerProgress);
		return new XmlSerializer(type);
	}
	
	private FileStream getStreamForPlayerProgress() {
		var path = getPathToPlayerProgress();
		var mode = FileMode.OpenOrCreate;
		return new FileStream(path, mode);
	}
	
	private string getPathToPlayerProgress() {
		const string KEY = "Path To Player Progress";
		return PlayerPrefs.GetString(KEY);
	}

	private int getChosenCampaignIndex() {
		var engine = new PlayerProgressCampaignsSearchEngine();
		return engine.getChosenCampaignIndex();
	}

	private void addNewLevelToPlayerProgress() {
		var o = getPlayerProgress();
		var serializer = getSerializerForPlayerProgress();
		var campaigns = o.campaigns;
		var index = getChosenCampaignIndex();
		var chosenCampaign = campaigns[index];
		var chosenCampaignLevels = chosenCampaign.levels;
		var item = new Level();
		var stream = getStreamForPlayerProgress();
		item.number = getNextLevelNumber();
		o.balanceInCoins -= getNextLevelCost();
		chosenCampaignLevels.Add(item);
		serializer.Serialize(stream, o);
		stream.Close();
	}

	private int getNextLevelNumber() {
		const string NAME = "Number";
		var nextLevel = getNextLevel();
		var attributes = nextLevel.Attributes;
		var attribute = attributes[NAME];
		var s = attribute.Value;
		return int.Parse(s);
	}

	private int getNextLevelCost() {
		var cls = GetComponentInParent<CampaignLevelScreen>();
		return cls.getNextLevelCost();
	}

	private void setNextLevelAsChosenOne() {
		const string KEY = "Chosen Level";
		string value = getXmlForNextLevel();
		PlayerPrefs.SetString(KEY, value);
	}

	private void reloadThisScreen() {
		string name = Application.loadedLevelName;
		Application.LoadLevel(name);
	}

	private void putGameOutOfPause() {
		Time.timeScale = 1;
	}
}