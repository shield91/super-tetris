﻿using UnityEngine;
using AssemblyCSharp;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;

public class QuitToMenuButton : MonoBehaviour {
	public Sprite switchIsOn;
	public Sprite switchIsOff;

	private void OnMouseDown() {
		highlightButton();
	}

	private void highlightButton() {
		var sr = GetComponent<SpriteRenderer>();
		sr.sprite = switchIsOn;
	}
	
	private void OnMouseUp() {
		unhighlightButton();
		updatePlayerProgress();
		loadPreviousScreen();
		resumeGame();
	}

	private void unhighlightButton() {
		var sr = GetComponent<SpriteRenderer>();
		sr.sprite = switchIsOff;
	}

	private void updatePlayerProgress() {
		updatePlayerProgressAccordingWithTypeOfGame();
		updateGameTimeInSecondsIfItIsNecessary();
	}

	private void updatePlayerProgressAccordingWithTypeOfGame() {
		if (itIsStartModeLevel()) {
			updatePlayerProgressForStartModeLevel();
		} else {
			updatePlayerProgressForCampaignLevelIfItIsNecessary();
		}
	}
	
	private bool itIsStartModeLevel() {
		var ps = GetComponentInParent<PlayScreen>();
		var b = ps.itIsCampaignLevel();
		return !b;
	}
	
	private void updatePlayerProgressForStartModeLevel() {
		if (gameIsInFreeMode()) {
			updatePlayerProgressForFreeMode();
		} else if (needInOpeningNextModeIsPresent()) {
			openNextMode();
		}
	}
	
	private bool gameIsInFreeMode() {
		const string START_MODE_NAME = "Free";
		return chosenStartModeIs(START_MODE_NAME);
	}
	
	private bool chosenStartModeIs(string startModeName) {
		string value = startModeName;
		string chosenStartModeName = getChosenStartModeName();
		return chosenStartModeName.Equals(value);
	}
	
	private string getChosenStartModeName() {
		const string KEY = "Chosen Start Mode";
		return PlayerPrefs.GetString(KEY);
	}

	private void updatePlayerProgressForFreeMode() {
		updateBalance();
		updatePointsInFreeMode();
	}
	
	private void updateBalance() {
		var o = getPlayerProgress();
		var serializer = getSerializerForPlayerProgress();
		var stream = getStreamForPlayerProgress();
		var coins = getEarnedCoins();
		o.balanceInCoins += coins;
		serializer.Serialize(stream, o);
		stream.Close();
	}
	
	private PlayerProgress getPlayerProgress() {
		var serializer = getSerializerForPlayerProgress();
		var stream = getStreamForPlayerProgress();
		var playerProgress = serializer.Deserialize(stream) as PlayerProgress;
		stream.Close();
		return playerProgress;
	}
	
	private XmlSerializer getSerializerForPlayerProgress() {
		var type = typeof(PlayerProgress);
		return new XmlSerializer(type);
	}
	
	private FileStream getStreamForPlayerProgress() {
		var path = getPathToPlayerProgress();
		var mode = FileMode.OpenOrCreate;
		return new FileStream(path, mode);
	}
	
	private string getPathToPlayerProgress() {
		const string KEY = "Path To Player Progress";
		return PlayerPrefs.GetString(KEY);
	}
	
	private int getEarnedCoins() {
		const string NAME = "Header/Coins/Value";
		var ps = GetComponentInParent<PlayScreen>();
		var gameWindow = ps.transform;
		var coinsValueTransform = gameWindow.Find(NAME);
		var tm = coinsValueTransform.GetComponent<TextMesh>();
		var s = tm.text;
		return int.Parse(s);
	}

	private void updatePointsInFreeMode() {
		var o = getPlayerProgress();
		var serializer = getSerializerForPlayerProgress();
		var stream = getStreamForPlayerProgress();
		var points = getEarnedCoins();
		o.pointsInFreeMode += points;
		serializer.Serialize(stream, o);
		stream.Close();
	}
	
	private bool needInOpeningNextModeIsPresent() {
		bool ba = playerGetsEnoughCoinsToOpenNextMode();
		bool bb = nextModeIsClosed();
		return ba && bb;
	}

	private bool playerGetsEnoughCoinsToOpenNextMode() {
		int earnedCoins = getEarnedCoins();
		int coinsToOpenNextMode = getCoinsToOpenNextMode();
		return earnedCoins >= coinsToOpenNextMode;
	}

	private int getCoinsToOpenNextMode() {
		var chosenStartMode = getChosenStartMode();
		return chosenStartMode.coinsToOpenNextMode;
	}

	private StartMode getChosenStartMode() {
		var engine = new PlayerProgressStartModesSearchEngine();
		return engine.getChosenStartMode();
	}

	private bool nextModeIsClosed() {
		int chosenStartModeIndex = getChosenStartModeIndex();
		int lastOpenedStartModeIndex = getLastOpenedStartModeIndex();
		return chosenStartModeIndex == lastOpenedStartModeIndex;
	}

	private int getChosenStartModeIndex() {
		var engine = new PlayerProgressStartModesSearchEngine();
		return engine.getChosenStartModeIndex();
	}

	private int getLastOpenedStartModeIndex() {
		const int VALUE = 1;
		int number = getStartModesNumber();
		return number - VALUE;
	}

	private int getStartModesNumber() {
		var startModes = getStartModes();
		return startModes.Count;
	}

	private List<StartMode> getStartModes() {
		var playerProgress = getPlayerProgress();
		return playerProgress.startModes;
	}
	
	private void openNextMode() {
		if (gameIsOnEasyMode()) {
			openMediumMode();
		} else if (gameIsOnMediumMode()) {
			openHardMode();
		} else if (gameIsOnHardMode()) {
			openFreeMode();
		}
	}
	
	private bool gameIsOnEasyMode() {
		const string START_MODE_NAME = "Easy";
		return chosenStartModeIs(START_MODE_NAME);
	}
	
	private void openMediumMode() {
		var nextStartMode = new StartMode();
		nextStartMode.name = "Medium";
		nextStartMode.speed = 3;
		nextStartMode.coinsToOpenNextMode = 500;
		open(nextStartMode);
	}
	
	private void open(StartMode nextStartMode) {
		var serializer = getSerializerForPlayerProgress();
		var o = getPlayerProgress();
		var stream = getStreamForPlayerProgress();
		var startModes = o.startModes;
		var item = nextStartMode;
		startModes.Add(item);
		serializer.Serialize(stream, o);
		stream.Close();
	}
	
	private bool gameIsOnMediumMode() {
		const string START_MODE_NAME = "Medium";
		return chosenStartModeIs(START_MODE_NAME);
	}
	
	private void openHardMode() {
		var nextStartMode = new StartMode();
		nextStartMode.name = "Hard";
		nextStartMode.speed = 5;
		nextStartMode.coinsToOpenNextMode = 1000;
		open(nextStartMode);
	}
	
	private bool gameIsOnHardMode() {
		const string START_MODE_NAME = "Hard";
		return chosenStartModeIs(START_MODE_NAME);
	}
	
	private void openFreeMode() {
		var nextStartMode = new StartMode();
		nextStartMode.name = "Free";
		nextStartMode.speed = 1;
		nextStartMode.coinsToOpenNextMode = 0;
		open(nextStartMode);
	}

	private void updatePlayerProgressForCampaignLevelIfItIsNecessary() {
		if (gameIsOnPause()) {
			updatePlayerProgressForCampaignLevelBeingOnPause();
		} else if (campaignLevelIsIncomplete()) {
			updatePlayerProgressForIncompleteCampaignLevel();
		}
	}

	private bool gameIsOnPause() {
		var playScreen = GetComponentInParent<PlayScreen>();
		var b = playScreen.gameIsAccomplished();
		return !b;
	}

	private void updatePlayerProgressForCampaignLevelBeingOnPause() {
		updateBalance();
		updatePointsInCampaign();
	}

	private void updatePointsInCampaign() {
		var o = getPlayerProgress();
		var serializer = getSerializerForPlayerProgress();
		var stream = getStreamForPlayerProgress();
		var points = getEarnedCoins();
		o.pointsInCampaign += points;
		serializer.Serialize(stream, o);
		stream.Close();
	}

	private bool campaignLevelIsIncomplete() {
		const int VALUE = 0;
		var number = getCampaignBricksNumber();
		return number > VALUE;
	}

	private int getCampaignBricksNumber() {
		var bricks = getCampaignBricks();
		return bricks.Length;
	}

	private CampaignBrick[] getCampaignBricks() {
		var playScreen = GetComponentInParent<PlayScreen>();
		return playScreen.GetComponentsInChildren<CampaignBrick>();
	}

	private void updatePlayerProgressForIncompleteCampaignLevel() {
		updateBalance();
		updatePointsInCampaign();
		updateGameTimeInSeconds();
	}

	private void updateGameTimeInSeconds() {
		var o = getPlayerProgress();
		var serializer = getSerializerForPlayerProgress();
		var stream = getStreamForPlayerProgress();
		var time = getGameTimeInSeconds();
		o.gameTimeInSeconds += time;
		serializer.Serialize(stream, o);
		stream.Close();
	}

	private int getGameTimeInSeconds() {
		float f = Time.timeSinceLevelLoad;
		return Mathf.RoundToInt(f);
	}

	private void updateGameTimeInSecondsIfItIsNecessary() {
		if (gameIsOnPause()) {
			updateGameTimeInSeconds();
		} else if (itIsIncompleteStartModeLevel()) {
			updateGameTimeInSeconds();
		}
	}

	private bool itIsIncompleteStartModeLevel() {
		bool ba = itIsStartModeLevel();
		bool bb = startModeLevelIsIncomplete();
		return ba && bb;
	}

	private bool startModeLevelIsIncomplete() {
		bool b = thereIsFallingBrickOnPlayField();
		return !b;
	}

	private bool thereIsFallingBrickOnPlayField() {
		const string VALUE = "Falling Brick";
		string fallingBrickName = getFallingBrickName();
		return fallingBrickName.Equals(VALUE);
	}

	private string getFallingBrickName() {
		var playScreen = GetComponentInParent<PlayScreen>();
		var playField = playScreen.GetComponentInChildren<PlayField>();
		var fallingBrick = playField.getFallingBrick();
		return fallingBrick.name;
	}

	private void loadPreviousScreen() {
		const string KEY = "Previous Screen Name";
		const string DEFAULT_VALUE = "Start Screen";
		string name = PlayerPrefs.GetString(KEY, DEFAULT_VALUE);
		Application.LoadLevel(name);
	}

	private void resumeGame() {
		Time.timeScale = 1;
	}
 }