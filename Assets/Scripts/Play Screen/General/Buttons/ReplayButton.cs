﻿using UnityEngine;
using AssemblyCSharp;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;

public class ReplayButton : MonoBehaviour {
	public Sprite switchIsOn;
	public Sprite switchIsOff;
	
	private void OnMouseDown() {
		highlightButton();
	}

	private void highlightButton() {
		var sr = GetComponent<SpriteRenderer>();
		sr.sprite = switchIsOn;
	}
	
	private void OnMouseUp() {
		unhighlightButton();
		reloadThisScreen();
		resumeGame();
		updatePlayerProgress();
	}

	private void unhighlightButton() {
		var sr = GetComponent<SpriteRenderer>();
		sr.sprite = switchIsOff;
	}

	private void reloadThisScreen() {
		string name = Application.loadedLevelName;
		Application.LoadLevel(name);
	}

	private void resumeGame() {
		Time.timeScale = 1;
	}

	private void updatePlayerProgress() {
		updatePlayerProgressAccordingWithTypeOfGame();
		updateGameTimeInSeconds();
	}

	private void updatePlayerProgressAccordingWithTypeOfGame() {
		if (itIsStartModeLevel()) {
			updatePlayerProgressForStartModeLevel();
		} else {
			updatePlayerProgressForCampaignLevel();
		}
	}

	private bool itIsStartModeLevel() {
		var ps = GetComponentInParent<PlayScreen>();
		var b = ps.itIsCampaignLevel();
		return !b;
	}

	private void updatePlayerProgressForStartModeLevel() {
		if (gameIsInFreeMode()) {
			updatePlayerProgressForFreeMode();
		} else if (needInOpeningNextModeIsPresent()) {
			openNextMode();
		}
	}

	private bool gameIsInFreeMode() {
		const string START_MODE_NAME = "Free";
		return chosenStartModeIs(START_MODE_NAME);
	}

	private bool chosenStartModeIs(string startModeName) {
		string value = startModeName;
		string chosenStartModeName = getChosenStartModeName();
		return chosenStartModeName.Equals(value);
	}

	private string getChosenStartModeName() {
		const string KEY = "Chosen Start Mode";
		return PlayerPrefs.GetString(KEY);
	}

	private void updatePlayerProgressForFreeMode() {
		updateBalance();
		updatePointsInFreeMode();
	}

	private void updateBalance() {
		var o = getPlayerProgress();
		var serializer = getSerializerForPlayerProgress();
		var stream = getStreamForPlayerProgress();
		var coins = getEarnedCoins();
		o.balanceInCoins += coins;
		serializer.Serialize(stream, o);
		stream.Close();
	}

	private PlayerProgress getPlayerProgress() {
		var serializer = getSerializerForPlayerProgress();
		var stream = getStreamForPlayerProgress();
		var playerProgress = serializer.Deserialize(stream) as PlayerProgress;
		stream.Close();
		return playerProgress;
	}

	private XmlSerializer getSerializerForPlayerProgress() {
		var type = typeof(PlayerProgress);
		return new XmlSerializer(type);
	}

	private FileStream getStreamForPlayerProgress() {
		var path = getPathToPlayerProgress();
		var mode = FileMode.OpenOrCreate;
		return new FileStream(path, mode);
	}

	private string getPathToPlayerProgress() {
		const string KEY = "Path To Player Progress";
		return PlayerPrefs.GetString(KEY);
	}


	private int getEarnedCoins() {
		const string NAME = "Header/Coins/Value";
		var ps = GetComponentInParent<PlayScreen>();
		var gameWindow = ps.transform;
		var coinsValueTransform = gameWindow.Find(NAME);
		var tm = coinsValueTransform.GetComponent<TextMesh>();
		var s = tm.text;
		return int.Parse(s);
	}

	private void updatePointsInFreeMode() {
		var o = getPlayerProgress();
		var serializer = getSerializerForPlayerProgress();
		var stream = getStreamForPlayerProgress();
		var coins = getEarnedCoins();
		o.pointsInFreeMode += coins;
		serializer.Serialize(stream, o);
		stream.Close();
	}

	private void updatePlayerProgressForCampaignLevel() {
		updateBalance();
		updatePointsInCampaign();
	}

	private void updatePointsInCampaign() {
		var o = getPlayerProgress();
		var serializer = getSerializerForPlayerProgress();
		var stream = getStreamForPlayerProgress();
		var coins = getEarnedCoins();
		o.pointsInCampaign += coins;
		serializer.Serialize(stream, o);
		stream.Close();
	}

	private void updateGameTimeInSeconds() {
		var o = getPlayerProgress();
		var serializer = getSerializerForPlayerProgress();
		var stream = getStreamForPlayerProgress();
		o.gameTimeInSeconds += getGameTimeInSeconds();
		serializer.Serialize(stream, o);
		stream.Close();
	}

	private int getGameTimeInSeconds() {
		float f = Time.timeSinceLevelLoad;
		return Mathf.RoundToInt(f);
	}		

	private bool needInOpeningNextModeIsPresent() {
		bool ba = playerGetsEnoughCoinsToOpenNextMode();
		bool bb = nextModeIsClosed();
		return ba && bb;
	}

	private bool playerGetsEnoughCoinsToOpenNextMode() {
		int earnedCoins = getEarnedCoins();
		int coinsToOpenNextMode = getCoinsToOpenNextMode();
		return earnedCoins >= coinsToOpenNextMode;
	}

	private int getCoinsToOpenNextMode() {
		var chosenStartMode = getChosenStartMode();
		return chosenStartMode.coinsToOpenNextMode;
	}

	private StartMode getChosenStartMode() {
		var engine = new PlayerProgressStartModesSearchEngine();
		return engine.getChosenStartMode();
	}

	private bool nextModeIsClosed() {
		int chosenStartModeIndex = getChosenStartModeIndex();
		int lastOpenedStartModeIndex = getLastOpenedStartModeIndex();
		return chosenStartModeIndex == lastOpenedStartModeIndex;
	}

	private int getChosenStartModeIndex() {
		var engine = new PlayerProgressStartModesSearchEngine();
		return engine.getChosenStartModeIndex();
	}

	private int getLastOpenedStartModeIndex() {
		const int VALUE = 1;
		int number = getStartModesNumber();
		return number - VALUE;
	}

	private int getStartModesNumber() {
		var startModes = getStartModes();
		return startModes.Count;
	}

	private List<StartMode> getStartModes() {
		var playerProgress = getPlayerProgress();
		return playerProgress.startModes;
	}

	private void openNextMode() {
		if (gameIsOnEasyMode()) {
			openMediumMode();
		} else if (gameIsOnMediumMode()) {
			openHardMode();
		} else if (gameIsOnHardMode()) {
			openFreeMode();
		}
	}

	private bool gameIsOnEasyMode() {
		const string START_MODE_NAME = "Easy";
		return chosenStartModeIs(START_MODE_NAME);
	}

	private void openMediumMode() {
		var nextStartMode = new StartMode();
		nextStartMode.name = "Medium";
		nextStartMode.speed = 3;
		nextStartMode.coinsToOpenNextMode = 500;
		open(nextStartMode);
	}

	private void open(StartMode nextStartMode) {
		var serializer = getSerializerForPlayerProgress();
		var o = getPlayerProgress();
		var stream = getStreamForPlayerProgress();
		var startModes = o.startModes;
		var item = nextStartMode;
		startModes.Add(item);
		serializer.Serialize(stream, o);
		stream.Close();
	}

	private bool gameIsOnMediumMode() {
		const string START_MODE_NAME = "Medium";
		return chosenStartModeIs(START_MODE_NAME);
	}

	private void openHardMode() {
		var nextStartMode = new StartMode();
		nextStartMode.name = "Hard";
		nextStartMode.speed = 5;
		nextStartMode.coinsToOpenNextMode = 1000;
		open(nextStartMode);
	}

	private bool gameIsOnHardMode() {
		const string START_MODE_NAME = "Hard";
		return chosenStartModeIs(START_MODE_NAME);
	}

	private void openFreeMode() {
		var nextStartMode = new StartMode();
		nextStartMode.name = "Free";
		nextStartMode.speed = 1;
		nextStartMode.coinsToOpenNextMode = 0;
		open(nextStartMode);
	}			
}