﻿using UnityEngine;

public class MenuButton : MonoBehaviour {
	public Sprite switchIsOn;
	public Sprite switchIsOff;

	private void OnMouseDown() {
		highlightButton();
	}

	private void highlightButton() {
		var sr = GetComponent<SpriteRenderer>();
		sr.sprite = switchIsOn;
	}

	private void OnMouseUp() {
		unhighlightButton();
		pauseGame();
		disableMenuButton();
		disableSwitchButton();
		disablePauseButton();
		hidePausePopUpWindow();
		showMenuPopUpWindow();
		disableLeftPlayField();
		disableRightPlayField();
	}

	private void unhighlightButton() {
		var sr = GetComponent<SpriteRenderer>();
		sr.sprite = switchIsOff;
	}

	private void pauseGame() {
		Time.timeScale = 0;
	}

	private void disableMenuButton() {
		const string NAME = "Footer/Menu";
		disable(NAME);
	}

	private void disable(string name) {
		var child = getTransformFor(name);
		var collider = child.GetComponentInChildren<Collider2D>();
		collider.enabled = false;
	}

	private Transform getTransformFor(string name) {
		var playScreen = GetComponentInParent<PlayScreen>();
		var gameWindow = playScreen.transform;
		return gameWindow.Find(name);
	}

	private void disableSwitchButton() {
		const string NAME = "Footer/Switch";
		disable(NAME);
	}

	private void disablePauseButton() {
		const string NAME = "Footer/Pause";
		disable(NAME);
	}

	private void hidePausePopUpWindow() {
		const string NAME = "Pause Pop-Up Window";
		const bool VALUE = false;
		var popUpWindow = get(NAME);
		popUpWindow.SetActive(VALUE);
	}

	private GameObject get(string name) {
		var child = getTransformFor(name);
		return child.gameObject;
	}

	private void showMenuPopUpWindow() {
		const string NAME = "Menu Pop-Up Window";
		const bool VALUE = true;
		var popUpWindow = get(NAME);
		popUpWindow.SetActive(VALUE);
	}

	private void disableLeftPlayField() {
		const string NAME = "Play Fields/Left Play Field/Play Field/Background";
		disable(NAME);
	}

	private void disableRightPlayField() {
		const string NAME = "Play Fields/Right Play Field/Play Field/Background";
		disable(NAME);
	}
}