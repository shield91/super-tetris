﻿using UnityEngine;
using AssemblyCSharp;
using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

public class CampaignLevelScreen : MonoBehaviour {
	private void Start() {
		setPreviousScreenName();
		loadCampaignBricks();
		launchFirstFallingBricks();
	}

	private void setPreviousScreenName() {
		const string KEY = "Previous Screen Name";
		const string VALUE = "Levels Screen";
		PlayerPrefs.SetString(KEY, VALUE);
	}

	private void loadCampaignBricks() {
		var suppliant = transform;
		var loader = new CampaignBricksLoader(suppliant);
		loader.load();
	}

	private void launchFirstFallingBricks() {
		var playWindow = transform;
		var launcher = new FirstFallingBricksLauncher(playWindow);
		launcher.launchBricks();
	}

	private void Update() {
		if (allCampaignBricksAreDestroyed()) {
			processLevelFinish();
		}
	}

	private bool allCampaignBricksAreDestroyed() {
		var bricks = GetComponentsInChildren<CampaignBrick>();
		var n = bricks.Length;
		return n == 0;
	}

	private void processLevelFinish() {
		pauseGame();
		markThatGameIsCompleted();
		updatePlayerProgress();
		showLevelCompletePopUpWindow();
		setUpNextLevelButton();
		disableMenuButton();
		disableSwitchButton();
		disablePauseButton();
		disableLeftPlayField();
		disableRightPlayField();
		disableThisScript();
	}

	private void pauseGame() {
		Time.timeScale = 0;
	}

	private void markThatGameIsCompleted() {
		const bool VALUE = true;
		var ps = GetComponent<PlayScreen>();
		ps.setGameComplete(VALUE);
	}

	private void updatePlayerProgress() {
		updateHighScoreForChosenLevel();
		updateBalance();
		updateGameTimeInSeconds();
		updatePointsInCampaign();
	}

	private void updateHighScoreForChosenLevel() {
		var o = getPlayerProgress();
		var serializer = getSerializerForPlayerProgress();
		var campaigns = o.campaigns;
		var chosenCampaignIndex = getChosenCampaignIndex();
		var chosenCampaign = campaigns[chosenCampaignIndex];
		var chosenCampaignLevels = chosenCampaign.levels;
		var chosenLevelIndex = getChosenLevelIndex();
		var chosenLevel = chosenCampaignLevels[chosenLevelIndex];
		var highScoreInCoins = getHighScoreInCoins();
		var stream = getStreamForPlayerProgress();
		chosenLevel.highScoreInCoins = highScoreInCoins;
		serializer.Serialize(stream, o);
		stream.Close();
	}

	private PlayerProgress getPlayerProgress() {
		var serializer = getSerializerForPlayerProgress();
		var stream = getStreamForPlayerProgress();
		var playerProgress = serializer.Deserialize(stream) as PlayerProgress;
		stream.Close();
		return playerProgress;
	}

	private XmlSerializer getSerializerForPlayerProgress() {
		var type = typeof(PlayerProgress);
		return new XmlSerializer(type);
	}

	private FileStream getStreamForPlayerProgress() {
		var path = getPathToPlayerProgress();
		var mode = FileMode.OpenOrCreate;
		return new FileStream(path, mode);
	}
	
	private string getPathToPlayerProgress() {
		const string KEY = "Path To Player Progress";
		return PlayerPrefs.GetString(KEY);
	}

	private int getChosenCampaignIndex() {
		var engine = new PlayerProgressCampaignsSearchEngine();
		return engine.getChosenCampaignIndex();
	}

	private int getChosenLevelIndex() {
		var xmlForSelectedLevel = getXmlForChosenLevel();
		var engine = new PlayerProgressChosenCampaignLevelsSearchEngine(xmlForSelectedLevel);
		return engine.getChosenLevelIndex();
	}

	private string getXmlForChosenLevel() {
		const string KEY = "Chosen Level";
		return PlayerPrefs.GetString(KEY);
	}

	private int getHighScoreInCoins() {
		var playerProgress = getPlayerProgress();
		var campaigns = playerProgress.campaigns;
		var chosenCampaignIndex = getChosenCampaignIndex();
		var chosenCampaign = campaigns[chosenCampaignIndex];
		var chosenCampaignLevels = chosenCampaign.levels;
		var chosenLevelIndex = getChosenLevelIndex();
		var chosenLevel = chosenCampaignLevels[chosenLevelIndex];
		var a = chosenLevel.highScoreInCoins;
		var b = getEarnedCoins();
		return Mathf.Max(a, b);
	}

	private int getEarnedCoins() {
		const string NAME = "Header/Coins/Value";
		var coins = transform.Find(NAME);
		var tm = coins.GetComponent<TextMesh>();
		var s = tm.text;
		return int.Parse(s);
	}

	private void updateBalance() {
		var serializer = getSerializerForPlayerProgress();
		var o = getPlayerProgress();
		var stream = getStreamForPlayerProgress();
		o.balanceInCoins += getEarnedCoins();
		serializer.Serialize(stream, o);
		stream.Close();
	}

	private void updateGameTimeInSeconds() {
		var serializer = getSerializerForPlayerProgress();
		var o = getPlayerProgress();
		var stream = getStreamForPlayerProgress();
		var f = Time.timeSinceLevelLoad;
		o.gameTimeInSeconds += Mathf.RoundToInt(f);
		serializer.Serialize(stream, o);
		stream.Close();
	}

	private void updatePointsInCampaign() {
		var serializer = getSerializerForPlayerProgress();
		var o = getPlayerProgress();
		var stream = getStreamForPlayerProgress();
		o.pointsInCampaign += getEarnedCoins();
		serializer.Serialize(stream, o);
		stream.Close();
	}

	private void showLevelCompletePopUpWindow() {
		const string NAME = "Level Complete Pop-Up Window";
		const bool VALUE = true;
		var go = getGameObjectFor(NAME);
		go.SetActive(VALUE);
	}

	private GameObject getGameObjectFor(string name) {
		var playScreen = GetComponentInParent<PlayScreen>();
		var gameWindow = playScreen.transform;
		var child = gameWindow.Find(name);
		return child.gameObject;
	}
	
	private void setUpNextLevelButton() {
		if (nextLevelButtonShouldBeDisabled()) {
			hideNextLevelButton();
		}
	}

	private bool nextLevelButtonShouldBeDisabled() {
		bool ba = itIsTheLastLevelInCampaign();
		bool bb = playerHasInsufficientFundsToPurchaseNextLevel();
		return ba || bb;
	}

	private bool itIsTheLastLevelInCampaign() {
		var nextItem = getNextLevel();
		return nextItem == null;
	}

	public XmlNode getNextLevel() {
		var chosenLevel = getChosenLevel();
		return chosenLevel.NextSibling;
	}

	private XmlNode getChosenLevel() {
		const string NAME = "Number";
		var document = getDocumentForChosenLevel();
		var documentElement = document.DocumentElement;
		var attributes = documentElement.Attributes;
		var attribute = attributes[NAME];
		var name = attribute.Value;
		var engine = new CampaignLevelsSearchEngine();
		engine.get(name);
		return engine.getCurrentItem();
	}

	private XmlDocument getDocumentForChosenLevel() {
		var document = new XmlDocument();
		var xml = getXmlForChosenLevel();
		document.LoadXml(xml);
		return document;
	}

	private bool playerHasInsufficientFundsToPurchaseNextLevel() {
		var playerProgress = getPlayerProgress();
		var balance = playerProgress.balanceInCoins;
		var nextLevelCost = getNextLevelCost();
		return balance < nextLevelCost;
	}

	public int getNextLevelCost() {
		try {
			return getCostForNextLevel();
		} catch (Exception e) {
			return getDefaultValueForCostBecauseOf(e);
		}
	}

	private int getCostForNextLevel() {
		const string NAME = "Cost";
		var nextLevel = getNextLevel();
		var attributes = nextLevel.Attributes;
		var attribute = attributes[NAME];
		var s = attribute.Value;
		return int.Parse(s);
	}

	private int getDefaultValueForCostBecauseOf(Exception e) {
		handle(e);
		return getDefaultValueForCost();
	}

	private void handle(Exception e) {
		e.GetType();
	}

	private int getDefaultValueForCost() {
		return 0;
	}

	private void hideNextLevelButton() {
		const string NAME = "Level Complete Pop-Up Window/Next Level";
		const bool VALUE = false;
		var go = getGameObjectFor(NAME);
		go.SetActive(VALUE);
	}

	private void disableMenuButton() {
		const string NAME = "Footer/Menu";
		disable(NAME);
	}

	private void disable(string name) {
		var playScreen = GetComponentInParent<PlayScreen>();
		var gameWindow = playScreen.transform;
		var child = gameWindow.Find(name);
		var collider = child.GetComponentInChildren<Collider2D>();
		collider.enabled = false;
	}

	private void disableSwitchButton() {
		const string NAME = "Footer/Switch";
		disable(NAME);
	}

	private void disablePauseButton() {
		const string NAME = "Footer/Pause";
		disable(NAME);
	}

	private void disableLeftPlayField() {
		const string NAME = "Play Fields/Left Play Field/Play Field/Background";
		disable(NAME);
	}

	private void disableRightPlayField() {
		const string NAME = "Play Fields/Right Play Field/Play Field/Background";
		disable(NAME);
	}

	private void disableThisScript() {
		enabled = false;
	}
}