using UnityEngine;

namespace AssemblyCSharp
{
	public class CampaignBricksLoader : ItemsLoader {
		private Transform requester;

		public CampaignBricksLoader(Transform suppiant) {
			var value = suppiant;
			setRequester(value);
		}

		private void setRequester(Transform value) {
			requester = value;
		}

		protected override string getXpathForItems() {
			return "Level/PlayField";
		}

		protected override string getXmlForItems() {
			const string KEY = "Chosen Level";
			return PlayerPrefs.GetString(KEY);
		}

		protected override string getPathForCurrentItem() {
			return "Campaign Brick";
		}

		protected override Transform getParentForItems() {
			var name = getNameForItemsParent();
			var suppliant = getRequester();
			return suppliant.Find(name);
		}

		private string getNameForItemsParent() {
			const string SA = "Play Fields/";
			const string SB = "/Play Field";
			string name = getValueForName();
			return SA + name + SB;
		}

		private string getValueForName() {
			const string NAME = "Name";
			return getValueFor(NAME);
		}

		protected Transform getRequester() {
			return requester;
		}

	 	protected override string getNameForCurrentItem() {
			return "Campaign Brick";
		}

		protected override void setUpSpecificParametersForCurrentItem() {
			loadBlocks();
			setUpBrickTransformComponent();
			sealBrickPositionInGrid();
			makeSureThatFirstFallingBrickWillBeControllable();
		}

		private void loadBlocks() {
			packDataForBlocksLoading();
			loadMinos();
		}

		private void packDataForBlocksLoading() {
			const string KEY = "Current Brick";
			var item = getCurrentItem();
			var value = item.OuterXml;
			PlayerPrefs.SetString(KEY, value);
		}

		private void loadMinos() {
			var suppliant = getInstantiatedItem();
			var loader = new CampaignBrickBlocksLoader(suppliant);
			loader.load();
		}

		private void setUpBrickTransformComponent() {
			var item = getInstantiatedItem();
			item.localPosition = Vector3.zero;
			item.localScale = Vector3.one;
		}

		private void sealBrickPositionInGrid() {
			var minosset = getInstantiatedItem();
			var updater = new GridUpdater(minosset);
			updater.updateGrid();
		}

		private void makeSureThatFirstFallingBrickWillBeControllable() {
			var item = getInstantiatedItem();
			item.SetAsFirstSibling();
		}
	}
}