using UnityEngine;

namespace AssemblyCSharp
{
	public class CampaignBrickBlocksLoader : CampaignBricksLoader {
		public CampaignBrickBlocksLoader(Transform suppiant) : base(suppiant) {}

		protected override string getXpathForItems() {
			return "PlayField/Block";
		}

		protected override string getXmlForItems() {
			const string KEY = "Current Brick";
			return PlayerPrefs.GetString(KEY);
		}

		protected override GameObject getOriginalForCurrentItem() {
			var brick = getParentForItems();
			var playField = brick.parent;
			var name = getNameForCurrentBlock();
			var child = playField.Find(name);
			return child.gameObject;
		}

		protected override string getPathForCurrentItem() {
			return getEmptyStringBecauseOriginalWillBeInstantiatedDifferently();
		}

		private string getEmptyStringBecauseOriginalWillBeInstantiatedDifferently() {
			return "";
		}

		protected override Transform getParentForItems() {
			return getRequester();
		}

		private string getNameForCurrentBlock() {
			const string PATH = "Level Editor Panel/";
			string blockName = getValueForBlockName();
			return PATH + blockName;
		}
		
		private string getValueForBlockName() {
			const string NAME = "Name";
			return getValueFor(NAME);
		}

		protected override string getNameForCurrentItem() {
			return getValueForBlockName();
		}

		protected override float getMultiplierForItemScale() {
			return 0.9f;
		}

		protected override void setUpSpecificParametersForCurrentItem() {
			setUpBlockTransformComponent();
			setUpBlockTransparency();
		}

		private void setUpBlockTransformComponent() {
			var item = getInstantiatedItem();
			item.localPosition = getPositionForBlock();
			item.localScale = Vector3.one;
		}

		private Vector3 getPositionForBlock() {
			float x = getPositionForBlockOnX();
			float y = getPositionForBlockOnY();
			return new Vector3(x, y);
		}

		private float getPositionForBlockOnX() {
			const string NAME = "X";
			return getPositionForBlockOn(NAME);
		}

		private float getPositionForBlockOn(string name) {
			var s = getValueFor(name);
			return float.Parse(s);
		}

		private float getPositionForBlockOnY() {
			const string NAME = "Y";
			return getPositionForBlockOn(NAME);
		}

		private void setUpBlockTransparency() {
			const float R = 255f / 255f;
			const float G = R;
			const float B = G;
			const float A = 128 / 255f;
			var item = getInstantiatedItem();
			var sr = item.GetComponent<SpriteRenderer>();
			sr.color = new Color(R, G, B, A);
		}
	}
}