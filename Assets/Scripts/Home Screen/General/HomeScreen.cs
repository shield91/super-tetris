﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Xml;

public class HomeScreen : MonoBehaviour {
	private void Start() {
		deleteDataAboutCampaigns();
		setUpPathToPlayerProgressIfItIsNecessary();
	}

	private void deleteDataAboutCampaigns() {
		deleteDataAboutOriginalCampaigns();
		deleteDataAboutModifiedCampaigns();
	}

	private void deleteDataAboutOriginalCampaigns() {
		const string NAME = "Campaigns";
		deleteDataAbout(NAME);
	}

	private void deleteDataAbout(string name) {
		var key = name;
		PlayerPrefs.DeleteKey(key);
	}

	private void deleteDataAboutModifiedCampaigns() {
		const string NAME = "Modified Campaigns";
		deleteDataAbout(NAME);
	}

	private void setUpPathToPlayerProgressIfItIsNecessary() {
		if (playModeIsEnabled()) {
			setUpPathToPlayerProgress();
		}
	}

	private bool playModeIsEnabled() {
		return !levelEditorIsEnabled();
	}

	private bool levelEditorIsEnabled() {
		const string KEY = "Level Editor Mode Is Enabled";
		const string VALUE = "";
		string value = PlayerPrefs.GetString(KEY);
		return !value.Equals(VALUE);
	}

	private void setUpPathToPlayerProgress() {
		const string KEY = "Path To Player Progress";
		string value = getPathToPlayerProgress();
		PlayerPrefs.SetString(KEY,value);
	}

	private string getPathToPlayerProgress() {
		const string NAME = "/Player Progress.xml";
		string header = Application.persistentDataPath;
		return header + NAME;
	}

	public void goToStartScreen() {
		const string SCENE_NAME = "Start Screen";
		SceneManager.LoadScene(SCENE_NAME);
	}

	public void processGoingToCampaignScreenInLevelEditorMode() {
		enableLevelEditorMode();
		showPopUpWindow();
	}

	private void enableLevelEditorMode() {
		const string KEY = "Level Editor Mode Is Enabled";
		const string VALUE = "Yes";
		PlayerPrefs.SetString(KEY, VALUE);
	}

	private void showPopUpWindow() {
		const string NAME = "Pop-Up Window";
		const bool VALUE = true;
		var t = transform.Find(NAME);
		var go = t.gameObject;
		go.SetActive(VALUE);
	}
	
	public void goToCampaignScreen() {
		packDataAboutCampaigns();
		loadCampaignScreen();
	}

	public void packDataAboutCampaigns() {
		const string KEY = "Campaigns";
		string value = getDataAboutCampaigns();
		PlayerPrefs.SetString(KEY, value);
	}

	private string getDataAboutCampaigns() {
		if (levelEditorIsEnabled()) {
			return getDataAboutCampaignsInLevelEditorMode();
		} else {
			return getDataAboutCampaignsInPlayMode();
		}
	}

	private string getDataAboutCampaignsInLevelEditorMode() {
		const string KEY = "Path To Campaigns";
		const string SA = "\\Campaigns.xml";
		var path = PlayerPrefs.GetString(KEY);
		var filename = path + SA;
		var document = new XmlDocument();
		document.Load(filename);
		return document.OuterXml;
	}

	private string getDataAboutCampaignsInPlayMode() {
		const string PATH = "General/Campaigns";
		var systemTypeInstance = typeof(TextAsset);
		var o = Resources.Load(PATH, systemTypeInstance);
		var ta = (TextAsset) o;
		return ta.text;
	}

	public void loadCampaignScreen() {
		const string SCENE_NAME = "Campaign Screen";
		SceneManager.LoadScene(SCENE_NAME);
	}

	public void goToStatusScreen() {
		packDataAboutCampaigns();
		loadStatusScreen();
	}

	private void loadStatusScreen() {
		const string SCENE_NAME = "Status Screen";
		SceneManager.LoadScene(SCENE_NAME);
	}

	public void goToShopScreen() {
		const string SCENE_NAME = "Shop Screen";
		SceneManager.LoadScene(SCENE_NAME);
	}

	public void quitTheGame() {
		Application.Quit();
	}
}