﻿using UnityEngine;
using UnityEngine.UI;

public class HomeScreenPopUpWindow : MonoBehaviour {
	private void Start() {
		loadPathToCampaigns();
	}

	private void loadPathToCampaigns() {
		const string KEY = "Path To Campaigns";
		const string DEFAULT_VALUE = "C:";
		var field = getInputField();
		field.text = PlayerPrefs.GetString(KEY, DEFAULT_VALUE);
	}

	private InputField getInputField() {
		const string NAME = "Input Field";
		var field = transform.Find(NAME);
		return field.GetComponent<InputField>();
	}

	public void processPopUpWindowClosure() {
		confirmPathToCampaigns();
		packDataAboutCampaignsInLevelEditorMode();
		loadCampaignScreen();
	}

	private void confirmPathToCampaigns() {
		const string KEY = "Path To Campaigns";
		var f = getInputField();
		var value = f.text;
		PlayerPrefs.SetString(KEY, value);
	}

	private void packDataAboutCampaignsInLevelEditorMode() {
		var hs = GetComponentInParent<HomeScreen>();
		hs.packDataAboutCampaigns();
	}

	private void loadCampaignScreen() {
		var hs = GetComponentInParent<HomeScreen>();
		hs.loadCampaignScreen();
	}
}