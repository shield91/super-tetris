﻿using UnityEngine;
using System.Collections;
using Facebook.Unity;
using UnityEngine.UI;

public class FacebookManager : MonoBehaviour
{
	public Button facebook_share_btn;
	public GameObject no_add_button;

	void Awake()
	{
		if (!FB.IsInitialized)
		{
			FB.Init();
		}
		else
		{
			FB.ActivateApp();
		}
	}

	// Use this for initialization
	void Start () {
		if(PlayerPrefs.HasKey("no_add"))
		{
			no_add_button.SetActive(false);
		}
	}

	public void LogIn()
	{
		FB.LogInWithReadPermissions(callback: OnLogIn);
	}

	public void OnLogIn(ILoginResult result)
	{
		if (FB.IsLoggedIn)
		{
			AccessToken token = AccessToken.CurrentAccessToken;
		}
		else
		{
			Debug.Log("cannot access");
		}
	}

	public void Share()
	{
		FB.ShareLink(
			//TODO
			contentTitle: "Super Tetris game! Fun games for all. Enjoy :)",
//			contentURL: new System.Uri("link to game"),
			contentURL: new System.Uri("https://play.google.com/store/apps/details?id=com.BurningDaylight.PipesPuzzle"),
			contentDescription: "New aproach to classic tetris game",
			callback: OnShare,
//			photoURL: new System.Uri("link to image")
			photoURL: new System.Uri("http://burningdaylight.esy.es/bd_images/Android_512.png")
		);
	}
	public void OnShare(IShareResult result)
	{
		if (result.Cancelled || !string.IsNullOrEmpty(result.Error))
		{
			Debug.Log("Sharelink error" + result.Error);
		}
		else if (!string.IsNullOrEmpty(result.PostId))
		{
			Debug.Log(result.PostId);
		}
		else
		{
			Debug.Log("Share succeed");

			PlayerPrefs.SetInt("Share_clicked_date", System.DateTime.Today.Day);
			//TODO add 500 coins to player progress xml
			// += 500 coins
			facebook_share_btn.interactable = false;
		}
	}

}
